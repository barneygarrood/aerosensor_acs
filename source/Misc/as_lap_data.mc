using Toybox.System as Sys;

class as_lap_data_array
{
	const LAP_VALID = 0;
	const LAP_NO = 1;
	const LAP_TIME = 2;
	const LAP_DIST = 3;
	const LAP_CDA = 4;
	const LAP_HEAD = 5;
	const LAP_CHEST = 6;

	const LAP_DATA_COUNT = 7;
}

class as_lap_data
{		
	const AEROSENSOR_LAP_DATA_LAPTIME = 0;
	const AEROSENSOR_LAP_DATA_CDA = 1;
	const AEROSENSOR_LAP_DATA_PCOR = 2;
	const AEROSENSOR_LAP_DATA_DISTANCE = 3;
	const AEROSENSOR_LAP_DATA_USER_PWR = 4;
	const AEROSENSOR_LAP_DATA_GRADE_PWR = 5;
	const AEROSENSOR_LAP_DATA_FRICTION_PWR = 6;
	const AEROSENSOR_LAP_DATA_WIND_PWR = 7;		
	const AEROSENSOR_LAP_DATA_KIN_PWR = 8;		
	const AEROSENSOR_LAP_DATA_PDYN_PWR = 9;		
	const AEROSENSOR_LAP_DATA_YAW = 10;												
	var count = 11;
	
	var deviceLapNumber = -1;
	var lapNumber = 0;	
	var lapOffset = 0;	
	var valid = false;	
	var scaling= [	0.001, 0.0001, 0.0001, 0.001, 0.01, 0.01, 0.01, 0.01, 0.01 , 0.01, 0.01];						
	var offset = [0, 0, 0, 0, 2<<22, 2<<22, 2<<22, 2<<22, 2<<22, 2<<22, 2<<22]; 		
	var vals = new [count];		
	hidden var body_position_accum = new[2]; 
	hidden var body_position_count = 0;
	var body_position_avg = new[2];
	
	function initialize()
	{
		for (var i=0;i<count;i++)
		{
			vals[i]=0.0;
		}
		body_position_accum[0] = 0;
		body_position_accum[1] = 0;
		body_position_avg[0] = 0;
		body_position_avg[1] = 0;
		body_position_count = 0;
	}
			
	function resetLapNumber()
	{
		lapOffset = lapNumber;
	}
	
	function isLapNew(data)
	{
		
		return ((data[1] & 0x0F) != deviceLapNumber);
	}
	
	function offsetLapNo()
	{
		return lapNumber - lapOffset;
	}
	function getFromBurst(data)
	{
		var newLapNumber = data[1] & 0x0F;
		var newValid = (data[1]>>4) & 0x01;
		Sys.println("getFromBurst, new lap number = " + newLapNumber + ", deviceLapNumber = "+deviceLapNumber+", valid = "+newValid);
		if (isLapNew(data))
		{
			valid = newValid;
			Sys.println("Valid = " + valid);
			Sys.println("PRE Get from burst: new lap # = "+ newLapNumber +", deviceLapNumber = "+deviceLapNumber+", lapNumber = "+lapNumber);
			if (deviceLapNumber == -1)
			{
				lapNumber = newLapNumber;
			}
			else
			{
				if (((newLapNumber - lapNumber) & 0x0F) < 2)	// Make sure we are roughly aligned on lap number.
				{
					lapNumber += ((newLapNumber - deviceLapNumber) & 0x0F);
				}
				else
				{
					lapNumber = newLapNumber;
				}
			}
			deviceLapNumber = newLapNumber;
			Sys.println("POST Get from burst: new lap # = "+ newLapNumber +", deviceLapNumber = "+ deviceLapNumber +", lapNumber = " + lapNumber);
			// Put data into lap data array:
			for (var i=0;i<count;i++)
			{
				vals[i]=0;
				if (data.size() >(i*3+4))
				{
					// Get binary value from the two bytes:
					vals[i] = data[i*3+2] + (data[i*3+3] << 8) + (data[i*3+4] << 16);
					vals[i]-=offset[i];
					// Scale to engineering units.
					vals[i] *= scaling[i];
				}
			}
			// Set CdA to zero if not valid:
			if (valid == 0) 
			{
				vals[AEROSENSOR_LAP_DATA_CDA] = 0;
			}
			// Update body position averages:
			update_body_pos_avg();
			// Update fields in FIT file:
			if (velo_view_fit_file.isLogging())
			{
				velo_view_fit_file.fields[CDA_LAP_FIELD].setData(vals[AEROSENSOR_LAP_DATA_CDA]);
				velo_view_fit_file.fields[HEAD_AVG_FIELD].setData(body_position_avg[0]);
				velo_view_fit_file.fields[CHEST_AVG_FIELD].setData(body_position_avg[1]);
				velo_view_fit_file.fields[LAP_TIME_FIELD].setData(vals[AEROSENSOR_LAP_DATA_LAPTIME]);
				velo_view_fit_file.fields[AERO_CAL_FIELD].setData(vals[AEROSENSOR_LAP_DATA_PCOR]);
				velo_view_fit_file.fields[LAP_RIDER_POWER_FIELD].setData(vals[AEROSENSOR_LAP_DATA_USER_PWR]);
				velo_view_fit_file.fields[LAP_AERO_POWER_FIELD].setData(vals[AEROSENSOR_LAP_DATA_WIND_PWR]);
				velo_view_fit_file.fields[LAP_GRAV_POWER_FIELD].setData(vals[AEROSENSOR_LAP_DATA_GRADE_PWR]);
				velo_view_fit_file.fields[LAP_FRIC_POWER_FIELD].setData(vals[AEROSENSOR_LAP_DATA_FRICTION_PWR]);
				velo_view_fit_file.fields[LAP_KIN_POWER_FIELD].setData(vals[AEROSENSOR_LAP_DATA_KIN_PWR]);
				velo_view_fit_file.fields[LAP_PDYN_POWER_FIELD].setData(vals[AEROSENSOR_LAP_DATA_PDYN_PWR]);
				velo_view_fit_file.fields[LAP_DISTANCE_FIELD].setData(vals[AEROSENSOR_LAP_DATA_DISTANCE]);
				velo_view_fit_file.fields[YAW_AVG_FIELD].setData(vals[AEROSENSOR_LAP_DATA_YAW]);
				
			}
			// Increment lap:
			velo_view_fit_file.addLap();
			vv_sensors.as_lap_transmitter.setData(lapNumber,
										valid,
										vals[AEROSENSOR_LAP_DATA_CDA],
										vals[AEROSENSOR_LAP_DATA_PCOR],
										vals[AEROSENSOR_LAP_DATA_LAPTIME]);
			
			// Reset all lap data:
			if (velo_view_fit_file.isLogging())
			{
				velo_view_fit_file.fields[CDA_LAP_FIELD].setData(0.0f);
				velo_view_fit_file.fields[HEAD_AVG_FIELD].setData(0);
				velo_view_fit_file.fields[CHEST_AVG_FIELD].setData(0);
				velo_view_fit_file.fields[LAP_TIME_FIELD].setData(0);
				velo_view_fit_file.fields[AERO_CAL_FIELD].setData(0);
				velo_view_fit_file.fields[LAP_RIDER_POWER_FIELD].setData(0);
				velo_view_fit_file.fields[LAP_AERO_POWER_FIELD].setData(0);
				velo_view_fit_file.fields[LAP_GRAV_POWER_FIELD].setData(0);
				velo_view_fit_file.fields[LAP_FRIC_POWER_FIELD].setData(0);
				velo_view_fit_file.fields[LAP_KIN_POWER_FIELD].setData(0);
				velo_view_fit_file.fields[LAP_PDYN_POWER_FIELD].setData(0);
				velo_view_fit_file.fields[LAP_DISTANCE_FIELD].setData(0);
				velo_view_fit_file.fields[YAW_AVG_FIELD].setData(0);
			}
		}
	}
	
	function add_body_pos(head,chest)
	{
		if ((head!= null) & (chest != null))
		{
			body_position_accum[0]+=head;
			body_position_accum[1]+=chest;
			body_position_count += 1.0f;
		}
	}
	
	function update_body_pos_avg()
	{
		if (body_position_count>0)
		{
			body_position_avg[0] = body_position_accum[0]/body_position_count;	
			body_position_avg[1] = body_position_accum[1]/body_position_count;	
		}
		else
		{
			body_position_avg[0]=0.0f;
			body_position_avg[1]=0.0f;
		}
		body_position_accum[0] = 0.0f;
		body_position_accum[1] = 0.0f;
		body_position_count = 0.0f;
	}
	
	function copy()
	{
		var ret = new as_lap_data();
		ret.valid = valid;
		ret.lapNumber = lapNumber;
		ret.lapOffset = lapOffset;
		for (var i=0;i<3;i++)
		{
			ret.body_position_avg[0] = body_position_avg[0];
		}
		for (var i=0;i<count;i++)
		{
			ret.vals[i]=vals[i];
		}
		return ret;
	}

	function getLapDataArray()
	{
		var ret = new[0];
		ret.add(valid);
		ret.add(lapNumber);
		ret.add(vals[AEROSENSOR_LAP_DATA_LAPTIME]);
		ret.add(vals[AEROSENSOR_LAP_DATA_DISTANCE]);
		ret.add(vals[AEROSENSOR_LAP_DATA_CDA]);
		ret.add(body_position_avg[0]);
		ret.add(body_position_avg[1]);
		return ret;
	}
}