using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.WatchUi as Ui;
using Toybox.AntPlus as AntPlus;

class vv_icons extends Ui.Drawable
{
	hidden var icon_font = Gfx.FONT_XTINY;
	hidden var batOutline = [[0,0],[16,0],[16,2],[18,2],[18,5],[16,5],[16,7],[0,7]];
	hidden var triOutline = [[0,0],[8,4],[0,8]];
	var height = 12;
	hidden var xFit;
	hidden var xAerosensor;
	hidden var xAerobody;
	
	function initialize(params)
	{
		Drawable.initialize(params);
		xFit = params[:xFit];
		xAerosensor = params[:xAerosensor];
		xAerobody = params[:xAerobody];
		Sys.println("xFit = " + xFit);
	}
	
	function draw(dc)
	{
		drawRecording(dc,velo_view_fit_file.isLogging(), xFit);
		drawConnection(dc, !vv_sensors.aerobody.searching, xAerobody, "BODY");
		drawBattery(dc,vv_sensors.aerobody.data.batLevel,xAerobody+10);
		if (vv_settings.values[vv_settings.AEROSENSOR_ACTIVE])
		{
			drawConnection(dc, !vv_sensors.aerosensor.searching, xAerosensor, "AERO");
			drawSubConnection(dc,"PWR",vv_sensors.aerosensor.data.pairedDevices[0].channelState,xAerosensor+10);
			var state;
			if (vv_sensors.aerosensor.data.pairedDevices[1].channelState > vv_sensors.aerosensor.data.pairedDevices[2].channelState)
			{
				state = vv_sensors.aerosensor.data.pairedDevices[1].channelState;
			}
			else
			{
				state = vv_sensors.aerosensor.data.pairedDevices[2].channelState;
			}
			drawSubConnection(dc,"SPD",state,xAerosensor+35);
			drawBattery(dc,vv_sensors.aerosensor.data.batLevel,xAerosensor+60);
		}
		height = dc.getFontHeight(icon_font);
	}
	
	function drawSubConnection(dc,text, state, xStart)
	{
		// Firstly draw letter
		var yStart = 0;	
		// Draw rectangle based on state:
		switch (state)
		{
			case AntPlus.DEVICE_STATE_DEAD:
			case AntPlus.DEVICE_STATE_CLOSED:
			case AntPlus.DEVICE_STATE_CNT:
				dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
				break;
			case AntPlus.DEVICE_STATE_SEARCHING:
				dc.setColor(Gfx.COLOR_YELLOW, Gfx.COLOR_TRANSPARENT);
				break;
			case AntPlus.DEVICE_STATE_TRACKING:
				dc.setColor(Gfx.COLOR_DK_GREEN, Gfx.COLOR_TRANSPARENT);
				break;
		}
    	var fHeight = dc.getFontHeight(icon_font);
    	var width = dc.getTextWidthInPixels(text, icon_font)+2;
		dc.fillRectangle(xStart,0,width,fHeight);
		
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
    	dc.drawText(xStart+1, yStart, icon_font, text, Gfx.TEXT_JUSTIFY_LEFT);
    	// Then draw connection indicator:
    	
	}
	
	// Battery level = integer, 1 (full) to 5 (critical)
	function drawBattery(dc, batLevel, xStart)
	{	
		var yStart = 0;
		var barWidth = 3;
		var barHeight = 6;
		var ptCount = batOutline.size();
				
		for (var i=0;i<ptCount-1;i++)
		{
			dc.drawLine( xStart + batOutline[i][0] , yStart + batOutline[i][1] , xStart + batOutline[i+1][0] , yStart + batOutline[i+1][1] );
		}
		dc.drawLine( xStart + batOutline[ptCount-1][0] , yStart + batOutline[ptCount-1][1] , xStart + batOutline[0][0] , yStart + batOutline[0][1] );
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
		dc.setPenWidth(1);
		for (var i=0;i<ptCount-1;i++)
		{
			dc.drawLine( xStart + batOutline[i][0] , yStart + batOutline[i][1] , xStart + batOutline[i+1][0] , yStart + batOutline[i+1][1] );
		}
		dc.drawLine( xStart + batOutline[ptCount-1][0] , yStart + batOutline[ptCount-1][1] , xStart + batOutline[0][0] , yStart + batOutline[0][1] );
		
		// Draw battery bars:
		if (batLevel > 0)
		{
			for (var i=0;i<6-batLevel;i++)
			{
				dc.fillRectangle(xStart+1+i*(barWidth), yStart + 1,barWidth,barHeight);
			}
		}
		else
		{
			// Bat not yet initialised - draw cross.
			dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
			dc.drawLine(xStart+5, yStart, xStart + 5 + 8, yStart + 8);
			dc.drawLine(xStart+5, yStart + 8, xStart + 5 + 8, yStart);
		}
		
	}
	
	function drawConnection(dc,connected, xStart, text)
	{
		var yStart = 0;
		
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
    	var fWidth = dc.getTextWidthInPixels(text,icon_font);
    	var fHeight = dc.getFontHeight(icon_font);
    	dc.drawText(xStart-fWidth-2, yStart, icon_font, text, Gfx.TEXT_JUSTIFY_LEFT);
		
		dc.setPenWidth(1);
		if (connected)
		{
			dc.setColor(Gfx.COLOR_DK_GREEN, Gfx.COLOR_TRANSPARENT);
			dc.fillCircle(xStart + 1, yStart + 8 -1,1);
			dc.drawArc(xStart + 1, yStart + 8 -1, 3, Gfx.ARC_COUNTER_CLOCKWISE,0,90);
			dc.drawArc(xStart + 1, yStart + 8 -1, 5, Gfx.ARC_COUNTER_CLOCKWISE,0,90);
			dc.drawArc(xStart + 1, yStart + 8 -1, 7, Gfx.ARC_COUNTER_CLOCKWISE,0,90);
		}
		else
		{
			dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
			// Oddly, the last point of a line is not drawn, so draw these points separately, just to make a point (Ha!)
			dc.drawLine(xStart, yStart + 8, xStart + 8, yStart );
			dc.drawLine(xStart + 8, yStart + 8, xStart, yStart);
			dc.drawPoint(xStart + 8, yStart );
			dc.drawPoint(xStart, yStart);
		}
	}
	
	// Not recording - just a simple red filled square:
	function drawRecording(dc, isRecording, xStart)
	{
		var yStart = 0;
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
    	var text = "FIT";
    	var fWidth = dc.getTextWidthInPixels(text,icon_font);
    	var fHeight = dc.getFontHeight(icon_font);
    	dc.drawText(xStart-fWidth-2, yStart, icon_font, text, Gfx.TEXT_JUSTIFY_LEFT);
    	
		dc.setPenWidth(1);
		if (isRecording)
		{
			dc.setColor(Gfx.COLOR_DK_GREEN, Gfx.COLOR_TRANSPARENT);
			var tri = new [triOutline.size()];
			for (var i=0;i<triOutline.size();i++)
			{
				tri[i] = new [2];
				tri[i][0] = triOutline[i][0] + xStart;
				tri[i][1] = triOutline[i][1] + yStart;
			}
			dc.fillPolygon(tri);
		}
		else
		{
			dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
			dc.fillRectangle(xStart,yStart,8,8);
		}
	}
}