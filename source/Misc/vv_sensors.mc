using Toybox.Graphics as Gfx;

class vv_status
{
	var color;
	var text;
	function initialize()
	{
		color = Gfx.COLOR_BLACK;
		text = "";
	}
}

class vv_sensors
{
	static var as_lap_transmitter;
	static var aerosensor;
	static var aerobody;
	static var bpwr;
	static var bsc;
	static var bspd;
	static var hr;
	static var lapTime;
	
	static function getStatus()
	{
		var ret = new vv_status();
       	if (aerosensor.data.validCda)
    	{
    		ret.text = "Valid";
    		ret.color = as_colors.foreground;
    	}
    	else if (aerosensor.searching)
    	{
    		ret.color = Gfx.COLOR_RED;
    		ret.text = "Searching...";
    	}
    	else
    	{
    		ret.color = Gfx.COLOR_RED;
    		if (aerosensor.data.cdaError == 1)
    		{
    			ret.text = "Braking";
    		}
    		else if (aerosensor.data.cdaError == 2)
    		{
    			ret.text = "Too slow";
    		}
    		else if (aerosensor.data.cdaError == 3)
    		{
    			ret.text = "Initialising..";
    		}
    		else if (aerosensor.data.cdaError > 10)
    		{
    			var countdown = 0;
	    		if ((aerosensor.data.cdaError > 10) && (aerosensor.data.cdaError <=40))
	    		{
	    			countdown = aerosensor.data.cdaError - 10;
	    		}
	    		else if ((aerosensor.data.cdaError > 40) && (aerosensor.data.cdaError <=60))
	    		{
	    			countdown = (aerosensor.data.cdaError/5) + 34;
	    		}
	    		else if ((aerosensor.data.cdaError > 60))
	    		{
	    			countdown = (aerosensor.data.cdaError/10) + 47;
	    		}
	    		ret.text = "Valid in " + countdown.format("%u") + "s";
    		}
    		else
    		{
    			// Should never get here..
    			ret.text = "Unknown condition.";
    		}
    	}
		return ret;
	}
	
	static function getStatusShortAv()
	{
		var ret = new vv_status();
       	if (aerosensor.data.validShort)
    	{
    		ret.text = "Valid";
    		ret.color = as_colors.foreground;
    	}
    	else if (aerosensor.searching)
    	{
    		ret.color = Gfx.COLOR_RED;
    		ret.text = "Searching...";
    	}
    	else
    	{
    		ret.color = Gfx.COLOR_RED;
    		if (aerosensor.data.cdaError == 1)
    		{
    			ret.text = "Braking";
    		}
    		else if (aerosensor.data.cdaError == 2)
    		{
    			ret.text = "Too slow";
    		}
    		else if (aerosensor.data.cdaError == 3)
    		{
    			ret.text = "Initialising..";
    		}
    	}
		return ret;
	}
	
	static function getLapStatus()
	{
		var ret = new vv_status();
		if (aerosensor.data.activeLap & aerosensor.data.validLap)
		{
			ret.text = "Active";
			ret.color = Gfx.COLOR_DK_GREEN;
		}
		else if (vv_sensors.aerosensor.data.validLap)
		{
			ret.text = "Complete";
			ret.color = as_colors.foreground;
		}
		else
		{
			ret.text = "Initialising.";
			ret.color = Gfx.COLOR_RED;
		}
		return ret;
	}
}