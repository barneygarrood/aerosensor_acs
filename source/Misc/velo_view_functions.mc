using Toybox.Graphics as Gfx;

class velo_view_functions
{
	function getValidText(dc, valid, searching, error, countdown)
	{
		var text = "";
		
		if (valid)
    	{
    		text = "Valid";
    	}
    	else if (searching)
    	{
    		text = "Searching...";
    	}
    	else
    	{
    		dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
    		if (error == 1)
    		{
    			text = "Braking";
    		}
    		else if (error == 2)
    		{
    			text = "Too slow";
    		}
    		else if (error == 3)
    		{
    			text = "Initialising..";
    		}
    		else if ((error > 10) && countdown)
    		{
    			countdown = 0;
	    		if ((error > 10) && (error <=40))
	    		{
	    			countdown = error - 10;
	    		}
	    		else if ((error > 40) && (error <=60))
	    		{
	    			countdown = (error/5) + 34;
	    		}
	    		else if ((error > 60))
	    		{
	    			countdown = (error/10) + 47;
	    		}
	    		text = "Valid in " + countdown.format("%u") + "s";
    		}
    		else
    		{
    			// Should never get here..
    			text = "Initialising";
    		}
    	}
    	return text;
	}
}