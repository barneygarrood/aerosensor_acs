using Toybox.Application as App;
using Toybox.System as Sys;
using Toybox.Graphics as Gfx;

class vv_settings
{
	static const SETTINGS_COUNT = 9;
	static const HEADREF = 0;
	static const CHESTREF = 1;
	static const WAISTREF = 2;
	static const NULLZONE = 3;
	static const AEROSENSOR_DEVICE_NUMBER = 4;
	static const AEROBODY_DEVICE_NUMBER = 5;
	static const OVERWRITE_AEROBODY_REF = 6;
	static const AEROSENSOR_ACTIVE = 7;
	static const BLACK_BACKGROUND = 8;
		
	static var propertyTags = ["HeadRef", "ChestRef", "WaistRef", "NullZone", "AerosensorDeviceNumber", "AerobodyDeviceNumber", "OverwriteRef", "AerosensorActive", "BlackBackground"];	
	static var values = new[SETTINGS_COUNT];
	static var foreground = 0;
	
	static function load()
	{
		foreground = Gfx.COLOR_BLACK;
		var app = App.getApp();
		for (var i=0;i<SETTINGS_COUNT;i++)
		{
			values[i] = app.getProperty(propertyTags[i]);
		}
	}

	static function save()
	{
		var app = App.getApp();
		for (var i=0;i<SETTINGS_COUNT;i++)
		{
			logging.log("Setting save " + propertyTags[i] + " = " + values[i]);
			app.Properties.setValue(propertyTags[i],values[i]);
			logging.log("done");
		}
	}
	
	static function toggleColorMode()
	{	
		values[BLACK_BACKGROUND] = !values[BLACK_BACKGROUND];
		as_colors.setBackground();
		save();
	}
}
