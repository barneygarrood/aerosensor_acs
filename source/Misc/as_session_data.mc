using Toybox.System as Sys;

class as_session_data
{
    // Hard code for now, but these will be user defineable.
    static private var _dist_tol = 100; // units?
    static private var _time_tol = 5;  // units?
    static private var _head_tol = 5; // units?
    static private var _chest_tol = 5; // units?

    static var sessionData = new [0];
    static var sessionLapData = new [0];
    static var sessionLapValid = new[0];
    static var allSessionData = new [0];

    static function reset()
    {
        sessionData = new[as_lap_data_array.LAP_DATA_COUNT];
        for (var i=0;i<as_lap_data_array.LAP_DATA_COUNT;i++)
        {
            sessionData[i]=0;
        }
        sessionLapData = new[0];
    }

    static function startSession()
    {
        reset();
    }

    static function endSession()
    {
        allSessionData.add(sessionData);
    }

    static function addLap(lapData)
    {
        // Only do if we are logging:
        if (velo_view_fit_file.isLogging())
        {
            sessionLapData.add(lapData);
            sessionLapValid.add(true);
        }
    }

    static function calculateSessionData()
    {
        // Initialise session data:
        for (var i=0; i<as_lap_data_array.LAP_DATA_COUNT;i++)
        {
            sessionData.add(0);
        }
        // If no laps, can't return any session data.
        var lapCount = sessionLapData.size();
        Sys.println("Lap count = " + lapCount);
        if (lapCount == 0) {return;}
        // If there are just two laps then no way of telling which is valid, so average them:
        // Mark all as valid.
        for (var i=0; i<lapCount;i++)
        {
            sessionLapData[i][as_lap_data_array.LAP_VALID] = true;
        }
        // If more than 3 then we try to find valid conditions.  With two laps we can't say which are most common.
        if (lapCount>=3)
        {
            // Need to mark as valid.
            // We tests validity by marking points within tolerance of most populous band.  Sort of the mode.
            // Go by distance, time, head, chest.
            var res = new[lapCount];
            var tol = [_dist_tol, _time_tol, _head_tol, _chest_tol];
            var tol_count = 4;
            var chanIndex = [as_lap_data_array.LAP_DIST,
                            as_lap_data_array.LAP_TIME,
                            as_lap_data_array.LAP_HEAD,
                            as_lap_data_array.LAP_CHEST];
            for (var i=0;i<tol_count;i++)
            {
                var maxIndex = -1;   // Index of lap at start of most populous band.
                for (var j=0;j<lapCount;j++)
                {
                    // If already marked as invalid then skip.
                    if (!sessionLapData[j][as_lap_data_array.LAP_VALID]){continue;}
                    if (maxIndex == -1)
                    {   
                        maxIndex = j;
                    }
                    var sisterLaps = new[0];
                    sisterLaps.add(j);
                    for (var k=0;k<lapCount;k++)
                    {
                        if ((!(k==j)) & sessionLapData[k][as_lap_data_array.LAP_VALID])
                        {
                            if ((sessionLapData[k][chanIndex[i]] >= sessionLapData[j][chanIndex[i]]) & 
                                (sessionLapData[k][chanIndex[i]] <= (sessionLapData[j][chanIndex[i]]+tol[i])))
                            {
                                sisterLaps.add(k);
                            }
                        }
                    }
                    res[j] = sisterLaps;
                    if (sisterLaps.size() > res[maxIndex].size())
                    {
                        maxIndex = j;
                    }
                }
                // Reset valids to false:
                for (var j=0; j<lapCount;j++)
                {
                    sessionLapData[j][as_lap_data_array.LAP_VALID] = false;
                }
                // Then set trues:
                for (var j=0; j<res[maxIndex].size();j++)
                {
                    sessionLapData[res[maxIndex][j]][as_lap_data_array.LAP_VALID] = true;
                }
            }
        }
        // Average data:
        var count=0;
        for (var i=2;i<as_lap_data_array.LAP_DATA_COUNT;i++)
        {
            sessionData[i]=0;
        }
        for (var i = 0;i<lapCount;i++)
        {
            if (sessionLapData[i][as_lap_data_array.LAP_VALID])
            {
                for (var j=2;j<as_lap_data_array.LAP_DATA_COUNT;j++)
                {
                    sessionData[j]+=sessionLapData[i][j];
                }
                count++;
            }
        }
        sessionData[1] = count;
        for (var i=2;i<as_lap_data_array.LAP_DATA_COUNT;i++)
        {
            sessionData[i]/=count;
        }
    }
}