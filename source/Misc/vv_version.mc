using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.WatchUi as Ui;
using Toybox.AntPlus as AntPlus;

class vv_version extends Ui.Drawable
{
	hidden var icon_font = Gfx.FONT_XTINY;
	hidden var xDrawVersion;
	
	function initialize(params)
	{
		Drawable.initialize(params);
		if (params[:xDrawVersion] != null)
		{
			xDrawVersion = params[:xDrawVersion];
		}
		else
		{
			// Default to true.
			xDrawVersion = true;
		}
	}
	
	function draw(dc)
	{
		if (xDrawVersion)
		{
			drawVersion(dc,0);
		}
	}
	
	function drawVersion(dc,xStart)
	{
		var text = Ui.loadResource(Rez.Strings.Version)	;	
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
    	var fHeight = dc.getFontHeight(icon_font);
		var yStart = dc.getHeight() - fHeight;
    	dc.drawText(xStart, yStart, icon_font, text, Gfx.TEXT_JUSTIFY_LEFT);
    	var fWidth = dc.getTextWidthInPixels(text,icon_font);
    	// Print page number
    	text = "p" + velo_view_delegate.getPageNumber().toString();
    	dc.drawText(xStart+fWidth+5, yStart , icon_font, text, Gfx.TEXT_JUSTIFY_LEFT);
	}
}