using Toybox.FitContributor as Fit;
using Toybox.ActivityRecording as Record;
using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Attention;

var session = null;
var FIELD_COUNT = 40;

var WIND_YAW_FIELD = 0;
var WIND_SPEED_FIELD = 1;
var BAROMETRIC_PRESSURE_FIELD = 2;
var AIR_DENSITY_FIELD = 3;
var RIDER_POWER_FIELD = 4;
var AERO_POWER_FIELD = 5;
var GRAV_POWER_FIELD = 6;
var FRIC_POWER_FIELD = 7;
var AERO_POS_HEAD_FIELD = 8;
var AERO_POS_CHEST_FIELD = 9;
var CDA_FIELD = 10;
var LAP_FIELD = 11;
var CDA_LAP_RECORD_FIELD = 12;
var ROAD_SPEED_FIELD = 13;
var CDA_STATUS_FIELD = 14;

var CDA_LAP_FIELD = 15;
var HEAD_AVG_FIELD = 16;
var CHEST_AVG_FIELD = 17;
var LAP_TIME_FIELD = 18;
var AERO_CAL_FIELD = 19;
var LAP_RIDER_POWER_FIELD = 20;
var LAP_AERO_POWER_FIELD = 21;
var LAP_GRAV_POWER_FIELD = 22;
var LAP_FRIC_POWER_FIELD = 23;
var LAP_KIN_POWER_FIELD = 24;
var LAP_PDYN_POWER_FIELD = 25;
var LAP_DISTANCE_FIELD = 26;
var YAW_AVG_FIELD = 27;

var TOTAL_MASS_SETTING_FIELD = 28;
var AERO_CAL_SETTING_FIELD = 29;
var WHEEL_CIRC_SETTING_FIELD = 30;
var PM_SCALING_SETTING_FIELD = 31;
var CRR_SETTING_FIELD = 32;
var LAP_COUNT_FIELD = 33;
var CDA_SESSION_FIELD = 34;
var LAPTIME_SESSION_FIELD = 35;
var HEAD_AVG_SESSION_FIELD = 36;
var CHEST_AVG_SESSION_FIELD = 37;
var HEAD_REF_SESSION_FIELD = 38;
var CHEST_REF_SESSION_FIELD = 39;

var BIKE_SPEED_FIELD = 14;

class velo_view_fit_file
{
	// FIT fields:
	static var fields = new [FIELD_COUNT];
	//static var lap_number = 0;
	static var lap_start_time = 0;
	
	static function addLap()	// time is system clock time in milliseconds of lap trigger.
	{
		Sys.println("Adding lap.");
		if( ( session == null ) || !session.isRecording() ) 
		{
			var toneProfile =
		    	[
				new Attention.ToneProfile(2000,250),
				new Attention.ToneProfile(3000,250),
				new Attention.ToneProfile(4000,250)										
		    	];
		    	
		    Attention.playTone({:toneProfile=>toneProfile});
		}
		if( ( session != null ) && session.isRecording() ) 
		{
			session.addLap();
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	
	static function startLog()
	{
		// Create session:
        session = Record.createSession({:name=>"AeroSession", :sport=>Record.SPORT_CYCLING});

        // Create record fields - these are written once per second.
		fields[WIND_YAW_FIELD] = session.createField("wind_yaw",WIND_YAW_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"°"});
		fields[WIND_SPEED_FIELD] = session.createField("wind_speed",WIND_SPEED_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"kph"});
		fields[BAROMETRIC_PRESSURE_FIELD] = session.createField("barometric_pressure",BAROMETRIC_PRESSURE_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"Pa"});
		fields[AIR_DENSITY_FIELD] = session.createField("air_density",AIR_DENSITY_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"kg/m3"});
		fields[RIDER_POWER_FIELD] = session.createField("rider_power",RIDER_POWER_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"W"});
		fields[AERO_POWER_FIELD] = session.createField("aero_power",AERO_POWER_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"W"});
		fields[GRAV_POWER_FIELD] = session.createField("grav_power",GRAV_POWER_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"W"});
		fields[FRIC_POWER_FIELD] = session.createField("fric_power",FRIC_POWER_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"W"});
		fields[AERO_POS_HEAD_FIELD] = session.createField("aero_pos_head",AERO_POS_HEAD_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"cm"});
		fields[AERO_POS_CHEST_FIELD] = session.createField("aero_pos_chest",AERO_POS_CHEST_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"cm"});
		fields[CDA_FIELD] = session.createField("cda",CDA_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"m2"});
		fields[LAP_FIELD] = session.createField("lap",LAP_FIELD,Fit.DATA_TYPE_SINT16,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>""});

		fields[CDA_LAP_RECORD_FIELD] = session.createField("cda_lap_record",CDA_LAP_RECORD_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"m2"});
		fields[ROAD_SPEED_FIELD] = session.createField("road_speed",ROAD_SPEED_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>"kph"});
		fields[CDA_STATUS_FIELD] = session.createField("cda_status",CDA_STATUS_FIELD,Fit.DATA_TYPE_UINT16,{ :mesgType=>Fit.MESG_TYPE_RECORD, :units=>""});
		fields[CDA_LAP_FIELD] = session.createField("cda_lap",CDA_LAP_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"m2"});
		fields[HEAD_AVG_FIELD] = session.createField("head_avg",HEAD_AVG_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"cm"});
		fields[CHEST_AVG_FIELD] = session.createField("chest_avg",CHEST_AVG_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"cm"});
		fields[LAP_TIME_FIELD] = session.createField("lap_time",LAP_TIME_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"s"});
		fields[AERO_CAL_FIELD] = session.createField("aero_cal",AERO_CAL_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>""});
		fields[LAP_RIDER_POWER_FIELD] = session.createField("lap_rider_power",LAP_RIDER_POWER_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"W"});
		fields[LAP_AERO_POWER_FIELD] = session.createField("lap_aero_power",LAP_AERO_POWER_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"W"});
		fields[LAP_GRAV_POWER_FIELD] = session.createField("lap_grav_power",LAP_GRAV_POWER_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"W"});
		fields[LAP_FRIC_POWER_FIELD] = session.createField("lap_fric_power",LAP_FRIC_POWER_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"W"});
		fields[LAP_KIN_POWER_FIELD] = session.createField("lap_kin_power",LAP_KIN_POWER_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"W"});
		fields[LAP_PDYN_POWER_FIELD] = session.createField("lap_pdyn_power",LAP_PDYN_POWER_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"W/m2"});
		fields[LAP_DISTANCE_FIELD] = session.createField("lap_distance",LAP_DISTANCE_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"revs"});
		fields[YAW_AVG_FIELD] = session.createField("yaw_avg",YAW_AVG_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_LAP, :units=>"°"});

		fields[TOTAL_MASS_SETTING_FIELD] = session.createField("total_mass_setting",TOTAL_MASS_SETTING_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>"kg"});
		fields[AERO_CAL_SETTING_FIELD] = session.createField("aero_cal_setting",AERO_CAL_SETTING_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>""});
		fields[WHEEL_CIRC_SETTING_FIELD] = session.createField("wheel_circ_setting",WHEEL_CIRC_SETTING_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>"mm"});
		fields[PM_SCALING_SETTING_FIELD] = session.createField("pm_scaling_setting",PM_SCALING_SETTING_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>""});
		fields[CRR_SETTING_FIELD] = session.createField("crr_setting",CRR_SETTING_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>""});
		fields[LAP_COUNT_FIELD] = session.createField("lap_count",LAP_COUNT_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>""});
		fields[CDA_SESSION_FIELD] = session.createField("cda_session",CDA_SESSION_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>"m2"});
		fields[LAPTIME_SESSION_FIELD] = session.createField("laptime_session",LAPTIME_SESSION_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>"s"});
		fields[HEAD_AVG_SESSION_FIELD] = session.createField("head_avg_session",HEAD_AVG_SESSION_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>"cm"});
		fields[CHEST_AVG_SESSION_FIELD] = session.createField("chest_avg_session",CHEST_AVG_SESSION_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>"cm"});
		fields[HEAD_REF_SESSION_FIELD] = session.createField("head_ref_session",HEAD_REF_SESSION_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>"cm"});
		fields[CHEST_REF_SESSION_FIELD] = session.createField("chest_ref_session",CHEST_REF_SESSION_FIELD,Fit.DATA_TYPE_FLOAT,{ :mesgType=>Fit.MESG_TYPE_SESSION, :units=>"cm"});


        for (var i=0;i<FIELD_COUNT;i++)
        {
        	fields[i].setData(0);
        }
        
        // Set other settings:
        if (vv_sensors.aerosensor.settings.loaded)
        {
        	fields[TOTAL_MASS_SETTING_FIELD].setData(vv_sensors.aerosensor.settings.vals[vv_sensors.aerosensor.settings.SETTING_TOTAL_MASS]);
        	fields[AERO_CAL_SETTING_FIELD].setData(vv_sensors.aerosensor.settings.vals[vv_sensors.aerosensor.settings.SETTING_AERO_CAL]);
        	fields[WHEEL_CIRC_SETTING_FIELD].setData(vv_sensors.aerosensor.settings.vals[vv_sensors.aerosensor.settings.SETTING_WHEEL_CIRC]);
        	fields[PM_SCALING_SETTING_FIELD].setData(vv_sensors.aerosensor.settings.vals[vv_sensors.aerosensor.settings.SETTING_PM_SCALING]);
        	fields[CRR_SETTING_FIELD].setData(vv_sensors.aerosensor.settings.vals[vv_sensors.aerosensor.settings.SETTING_CRR]);
			fields[HEAD_REF_SESSION_FIELD].setData(vv_sensors.aerobody.getHeadRef());
			fields[CHEST_REF_SESSION_FIELD].setData(vv_sensors.aerobody.getChestRef());
        }
        // Send command to aerosensor to start session.
		vv_sensors.aerosensor.sendStartSessionCommand();
		vv_sensors.aerosensor.lap_data.resetLapNumber();
		// Reset lap number:
		vv_sensors.aerosensor.resetLapNumber();
        session.start();
		// Reset session:
		as_session_data.startSession();
        lap_start_time = Sys.getTimer();
        Ui.requestUpdate();
	}
	
	static function stopLog()
	{
		if( ( session != null ) && session.isRecording() ) 
		{
			//addLap();
    		// Send command to aerosensor to end session.
			vv_sensors.aerosensor.sendEndSessionCommand();
			// Write session values:
			fields[LAP_COUNT_FIELD].setData(as_session_data.sessionData[as_lap_data_array.LAP_NO]);
			fields[CDA_SESSION_FIELD].setData(as_session_data.sessionData[as_lap_data_array.LAP_CDA]);
			fields[LAPTIME_SESSION_FIELD].setData(as_session_data.sessionData[as_lap_data_array.LAP_TIME]);
			fields[HEAD_AVG_SESSION_FIELD].setData(as_session_data.sessionData[as_lap_data_array.LAP_HEAD]);
			fields[CHEST_AVG_SESSION_FIELD].setData(as_session_data.sessionData[as_lap_data_array.LAP_CHEST]);

	        session.stop();
	        session.save();
	        session = null;
			as_session_data.endSession();
	        Ui.requestUpdate();
        }
	}
	
	static function startStopLog()
	{
		if( Toybox has :ActivityRecording ) 
		{
			if (( session == null ) || ( session.isRecording() == false ) )
			{
				me.startLog();
			}
			else if( ( session != null ) && session.isRecording() ) 
			{
				me.stopLog();
			}
		}
	}
	
	static function isLogging()
	{
		if( ( session != null ) && session.isRecording())
		{
			return true;
		} 
		else 
		{
			return false;
		}
		
	}
}