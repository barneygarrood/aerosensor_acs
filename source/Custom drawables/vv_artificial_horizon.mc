using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Math as Math;
using Toybox.System as Sys;

class vv_artificial_horizon extends Ui.Drawable
{
	hidden var mAngle;
	hidden var mValueText;
	hidden var mHeader;
	
	function setAngle(value){if (value != null){mAngle = value;}}
	function setHeader(text){mHeader = text;}
	
	function initialize(params) 
	{
		Drawable.initialize(params);
		mHeader = "";
		mValueText = "";
		mAngle = 0;
	}
	
	function drawText(dc,x,y,font,color,text)
	{
    	dc.setColor(color,Gfx.COLOR_TRANSPARENT);
    	dc.drawText(x,y,font,text,Gfx.TEXT_JUSTIFY_LEFT);
	}
	
	function draw(dc) 
	{
		var font = Gfx.FONT_MEDIUM;
		// Draw header:
		drawText(dc,locX + 1, locY+1, font, as_colors.foreground, mHeader);
		// First draw horizontal line
		dc.setPenWidth(1);
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
		dc.drawLine(locX+width/10,locY+height/2,locX+width*9/10, locY+height/2);
		// Then draw angled line
		dc.setPenWidth(3);
		dc.setColor(Gfx.COLOR_BLUE, Gfx.COLOR_TRANSPARENT);
		var len = width*2/5;
		var x = len * Math.cos(mAngle*Math.PI/180);
		var y = len * Math.sin(mAngle*Math.PI/180);
		var xctr = locX + width/2;
		var yctr = locY + height/2;
		dc.drawLine(xctr-x, yctr-y, xctr+x, yctr+y);
		// Then draw central circle
		dc.setColor(Gfx.COLOR_BLUE, Gfx.COLOR_TRANSPARENT);
		dc.fillCircle(xctr,yctr,width/6);
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
		dc.drawCircle(xctr,yctr,width/6);
		
		
		// Then draw value
		font = Gfx.FONT_NUMBER_MILD;
		var text = mAngle.format("%.1f");
		var text_width = dc.getTextWidthInPixels(text, font);
		var font_height = dc.getFontHeight(font);
		drawText(dc,locX + width/2-text_width/2, locY+height/2-font_height/2, font, Gfx.COLOR_WHITE, mAngle.format("%.1f"));
		
	}
}