using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_button extends Ui.Drawable
{
	hidden var mColor = Gfx.COLOR_WHITE;
	hidden var mTextColor = Gfx.COLOR_BLACK;
	hidden var mMainText; // or icon stuff
	hidden var mSubText; // or icon stuff
	hidden var mEnabled;
	hidden var mActive;
	
	function initialize(params) 
	{
		Drawable.initialize(params);
		mColor = params[:color];
		if (params[:textColor] != null)
		{
			mTextColor = params[:textColor];
		}else {mTextColor = as_colors.foreground;}
		if (params[:mainText]!=null)
		{
			mMainText = params[:mainText];
		} else {mMainText = "";}
		if (params[:subText]!=null)
		{
			mSubText = params[:subText];
		} else {mSubText = "";}
		mEnabled = params[:enabled];
		if (params[:active]!=null)
		{
			mActive = params[:active];
		}
		else
		{
			mActive = true;
		}
	}
	
	function setColor(color) 
	{
		mColor = color;
	}
	
	function setTextColor(textColor) 
	{
		mTextColor = textColor;
	}
	
	function setMainText(text) 
	{
		mMainText = text;
	}
	
	function setSubText(text) 
	{
		mSubText = text;
	}
		
	function disable()
	{
		mEnabled = false;
	}
	function enable()
	{
		mEnabled = true;
	}
	
	function activate()
	{
		mActive = true;
	}
	
	function deactivate()
	{
		mActive = false;
	}
	
	function isEnabled()
	{
		return mEnabled;
	}
	
	function setWidth(newWidth)
	{
		width = newWidth;
	}
	
	function setHeight(newHeight)
	{
		width = newHeight;
	}
	
	function draw(dc) 
	{
		if (mEnabled)
		{
			if (mActive)
			{
				dc.setColor(mColor, mColor);	
			}
			else
			{
				dc.setColor(Gfx.COLOR_LT_GRAY,Gfx.COLOR_LT_GRAY);
			}		
			dc.fillRectangle(locX, locY, width, height);
			dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
			dc.drawRectangle(locX, locY, width, height);
			
			dc.setColor(mTextColor, Gfx.COLOR_TRANSPARENT);
			if (mSubText.length() > 0)
			{
				var font = Gfx.FONT_SMALL;
				dc.drawText(locX + width / 2, locY + height / 4, font, mMainText, Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
				font = Gfx.FONT_TINY;
				dc.drawText(locX + width / 2, locY + height / 4 * 3, font, mSubText, Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
			}
			else
			{
				var font = Gfx.FONT_MEDIUM;
				dc.drawText(locX + width / 2, locY + height / 2, font, mMainText, Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
			}
		}
	}
	
	function isPointInside(coord) 
	{
		if (!mActive){return false;}
		if (coord[0] < locX) 
		{
			return false; // too far left
		}
		else if (coord[1] < locY) 
		{
			return false; // too far above
		}
		else if ((locX + width) < coord[0]) 
		{
			return false; // too far right
		}
		else if ((locY + height) < coord[1]) 
		{
			return false; // too far below
		}
		
		return true;
	}
		
	hidden var mTapCallback;
		
	function setTapCallback(callback) 
	{
		mTapCallback = callback;
	}
		
	function onTap() 
	{
		if (mTapCallback != null) 
		{
			return mTapCallback.invoke(self);
		}
		return false;
	}
}
