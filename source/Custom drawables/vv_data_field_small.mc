using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_data_field_small extends Ui.Drawable
{
	hidden var mHeadingText;
	hidden var mValueText;
	hidden var mValueColor;
	hidden var mUnitColor;
	hidden var mUnitText;
	hidden var mBackgroundColor;
	hidden var name;
	hidden var mDefaultBackgroundColor;
	hidden var mDefaultValueColor;
	hidden var mDefaultUnitColor;
	
	function setHeadingText(text){mHeadingText = text;}
	function setValueText(text){mValueText = text;}
	function setUnitText(text){mUnitText = text;}
	function setValueColor(color)
	{
		mValueColor = color;
		mDefaultValueColor = false;
	}
	function setUnitColor(color)
	{
		mUnitColor = color;
		mDefaultUnitColor = false;
	}
	function setBackgroundColor(color)
	{
		mBackgroundColor = color;
		mDefaultBackgroundColor = false;
	}
	
	function initialize(params) 
	{
		Drawable.initialize(params);
		mHeadingText = params[:headingText];
		mValueText = params[:valueText];
		
		if (params[:valueColor] == null)
		{
			mValueColor = as_colors.foreground;
			mDefaultValueColor = true;
		}
		else
		{
			mValueColor = params[:valueColor];
			mDefaultValueColor = false;
		}
		mUnitText = params[:unitText];
		if (params[:valueBackgroundColor] == null)
		{
			mBackgroundColor = as_colors.background;
			mDefaultBackgroundColor = true;
		}
		else
		{
			mBackgroundColor = params[:valueBackgroundColor];
			mDefaultBackgroundColor = false;
		}
		if (params[:unitColor] == null)
		{
			mUnitColor = as_colors.foreground;
			mDefaultUnitColor = true;
		}
		else
		{
			mUnitColor = params[:unitColor];
			mDefaultUnitColor = false;
		}
		
		
		name = params[:name];
	}

	
	function drawText(dc,x,y,font,color,text)
	{
    	dc.setColor(color,Gfx.COLOR_TRANSPARENT);
    	dc.drawText(x,y,font,text,Gfx.TEXT_JUSTIFY_LEFT);
	}

	function draw(dc)
	{
		// draw Block:
		if (mDefaultBackgroundColor)
		{
			mBackgroundColor = as_colors.background;
		}
    	dc.setColor(mBackgroundColor,Gfx.COLOR_TRANSPARENT);
		dc.fillRectangle(locX,locY,width,height);
		
		// Header:
		var font = Gfx.FONT_SMALL;
		drawText(dc,locX + 1, locY+1, font, as_colors.foreground, mHeadingText);
		
		// Value:
		if (mDefaultValueColor)
		{
			mValueColor = as_colors.foreground;
		}
		font = Graphics.FONT_LARGE;
		var textHeight = dc.getFontHeight(font);
		var textWidth = dc.getTextWidthInPixels(mValueText,font);
		drawText(dc, locX + width/2 - textWidth/2, locY + height/2-textHeight/2, font, mValueColor,mValueText);
				
		// Units:
		if (mDefaultUnitColor)
		{
			mUnitColor = as_colors.foreground;
		}
		font = Gfx.FONT_TINY;
		textHeight = dc.getFontHeight(font);
		textWidth = dc.getTextWidthInPixels(mUnitText,font);
		drawText(dc,locX + width - textWidth-1, locY + height-textHeight, font, mUnitColor,mUnitText);
		
	}
}