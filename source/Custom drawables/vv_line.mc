using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class vv_line extends Ui.Drawable
{
	hidden var mY1;
	hidden var mY2;
	hidden var mX1;
	hidden var mX2;
	
	function initialize(params) 
	{
		Drawable.initialize(params);
		mX1 = params[:x1];
		mX2 = params[:x2];
		mY1 = params[:y1];
		mY2 = params[:y2];
	}

	function draw(dc)
	{
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
		dc.drawLine(mX1,mY1,mX2,mY2);
	}
}