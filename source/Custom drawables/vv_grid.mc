using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class vv_grid extends Ui.Drawable
{
	hidden var mSize;
	hidden var mY1;
	hidden var mY2;
	
	function initialize(params) 
	{
		Drawable.initialize(params);
		mSize = params[:size];
		mY1 = params[:y1];
		mY2 = params[:y2];
	}

	function draw(dc)
	{
		var w=dc.getWidth();
		var h=dc.getHeight();
		var ih = vv_icons.height;
		if (mY1==null){mY1 = ih + (h-ih)/3;}
		if (mY2==null){mY2 = ih + (h-ih)*2/3;}
		var rowHeight = (h-ih)/3;
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
		if (mSize == 4)
		{
			dc.drawLine(w/2,ih,w/2,mY1);
		}
		else if(mSize == 5)
		{
			dc.drawLine(w/2,mY1,w/2,h);
		} 
		else if (mSize == 6)
		{
			dc.drawLine(w/2,ih,w/2,h);
		}
		else if (mSize == 7)
		{
			dc.drawLine(w/2,ih,w/2,mY2);
			dc.drawLine(w/3,mY2,w/3,h);
			dc.drawLine(2*w/3,mY2,2*w/3,h);
		}
		dc.drawLine(0,mY1,w, mY1);
		if (mSize > 2){dc.drawLine(0,mY2,w,mY2);}
		
	}
}