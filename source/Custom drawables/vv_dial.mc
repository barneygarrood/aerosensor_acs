using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Math as Math;

class vv_dial extends Ui.Drawable
{
	hidden var mMin;
	hidden var mMax;
	hidden var mValue;
	hidden var mTitle;
	hidden var mUnits;
	
	function setMin(value){mMin = value;}
	function setMax(value){mMax = value;}
	function setValue(value){mValue = value;}
	function setTitle(text){mTitle = text;}
	function setUnits(text){mUnits = text;}
	
	function initialize(params)
	{
		Drawable.initialize(params);
		mMin = params[:min];
		mMax = params[:max];
		mValue = params[:value];
		mTitle = params[:title];
		mUnits = params[:units];
	}

	function draw(dc)
	{	
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
		dc.setPenWidth(1);
		var margin = 5;
		var xctr = width/2.0f+width/12;
		var yctr = locY + height/2.0f + height/8;
		var radius = height/2.0f - margin + width/12;
		var radiusOffset = 10;
		var ptrWidth = 10;
		var ptrTipOffset = 20;
		dc.fillCircle(xctr, yctr, radius);
		
		dc.setColor(as_colors.background, Gfx.COLOR_TRANSPARENT);
		dc.fillCircle(xctr-radiusOffset+1, yctr, radius-radiusOffset);
		dc.fillRectangle(0, yctr, width, height);
		dc.fillPolygon([[xctr,yctr],[width,yctr-width/2],[width,yctr]]);
		
		// create pointer coordinates and rotate:
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
		dc.fillCircle(xctr,yctr,ptrWidth/2);
		
		var pointer = [[xctr,yctr + ptrWidth/2],[margin+ptrTipOffset,yctr],[xctr,yctr - ptrWidth/2]];
		var angle = (mValue - mMin) * 0.75f * Math.PI / (mMax - mMin) ;
		
		for (var i=0;i<3;i++)
		{
			var x = pointer[i][0] - xctr;
			var y = pointer[i][1] - yctr;
			pointer[i][0] = x * Math.cos(angle) - y * Math.sin(angle) + xctr;
			pointer[i][1] = x * Math.sin(angle) + y * Math.cos(angle) + yctr;
		}
		dc.setPenWidth(2);
		dc.fillPolygon(pointer);
		
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
		for (var i=0;i<2;i++)
		{
			dc.drawLine(pointer[i][0],pointer[i][1],pointer[i+1][0], pointer[i+1][1]);
		}
		dc.drawLine(pointer[2][0],pointer[2][1],pointer[0][0],pointer[0][1]);
		
		var text = mTitle;
    	var font = Gfx.FONT_SMALL;
    	dc.drawText(margin, locY + height/2 + height/8,font,text,Gfx.TEXT_JUSTIFY_LEFT);
    	
    	font = Gfx.FONT_LARGE;
    	text = mValue.format("%0.1f") + " " + mUnits;
    	var fWidth = dc.getTextWidthInPixels(text,font);
    	var fHeight = dc.getFontHeight(font);
    	dc.drawText(width/2 - fWidth/2, locY + height-fHeight, font,text,Gfx.TEXT_JUSTIFY_LEFT);
    	
	}
}