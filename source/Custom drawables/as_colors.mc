
using Toybox.System as Sys;
using Toybox.Graphics as Gfx;

class as_colors
{
	static var foreground;
	static var background;
		
		
	static function setBackground()
	{
		if (vv_settings.values[vv_settings.BLACK_BACKGROUND])
		{
			foreground = Gfx.COLOR_WHITE;
			background = Gfx.COLOR_BLACK;
		}
		else
		{
			foreground = Gfx.COLOR_BLACK;
			background = Gfx.COLOR_WHITE;
		}
	}
	
	static function init()
	{
		setBackground();
	}
	
}