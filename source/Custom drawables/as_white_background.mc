using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class as_white_background extends Ui.Drawable
{

	function draw(dc)
	{
		// draw Block:
    	dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_TRANSPARENT);
		dc.fillRectangle(0,0,dc.getWidth(),dc.getHeight());
	}
}