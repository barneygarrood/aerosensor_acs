using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class vv_button_down extends Ui.Drawable
{
	hidden var mEnabled;
	
	function initialize(params) 
	{
		Drawable.initialize(params);
	}
	function disable()
	{
		mEnabled = false;
	}
	function enable()
	{
		mEnabled = true;
	}
	
	function isEnabled()
	{
		return mEnabled;
	}
	
	function setWidth(newWidth)
	{
		width = newWidth;
	}
	
	
	function draw(dc) 
	{
		if (mEnabled)
		{
			dc.setColor(as_colors.background, Gfx.COLOR_TRANSPARENT);
			dc.fillRectangle(locX, locY, width, height);
			dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
			dc.drawRectangle(locX, locY, width, height);
			dc.fillPolygon([[locX+width/2-10, locY+height/2-10],[ locX+width/2, locY+height/2+10],
							[locX+width/2+10, locY+height/2-10]]);
		}
	}
	
	function isPointInside(coord) 
	{
		if (coord[0] < locX) 
		{
			return false; // too far left
		}
		else if (coord[1] < locY) 
		{
			return false; // too far above
		}
		else if ((locX + width) < coord[0]) 
		{
			return false; // too far right
		}
		else if ((locY + height) < coord[1]) 
		{
			return false; // too far below
		}
		
		return true;
	}
		
	hidden var mTapCallback;
		
	function setTapCallback(callback) 
	{
		mTapCallback = callback;
	}
		
	function onTap() 
	{
		if (mTapCallback != null) 
		{
			return mTapCallback.invoke(self);
		}
		return false;
	}
}
