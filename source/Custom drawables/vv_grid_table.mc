using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_grid_table extends Ui.Drawable
{
	
	function initialize(params) 
	{
		Drawable.initialize(params);
	}

	function draw(dc)
	{
		var w=dc.getWidth();
		var h=dc.getHeight();
		var ih = vv_icons.height;
		var font  = Gfx.FONT_SMALL;
		var textHeight = dc.getFontHeight(font);
		
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);
		// Header line:
		dc.drawLine(0,ih+textHeight,w,ih+textHeight);
		dc.drawLine(0,ih+textHeight*2,w,ih+textHeight*2);
		// Vertical lines
		var w1 = dc.getTextWidthInPixels("X", font)*1.5;
		dc.drawLine(w1,ih,w1,h);
		var w2 = (w-w1)/5;
		for (var i=0;i<4;i++)
		{
			dc.drawLine(w1+w2*(i+1),ih,w1+w2*(i+1),h);
		}
	}
}