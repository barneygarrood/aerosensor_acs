using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class vv_radar extends Ui.Drawable
{
	hidden var mEnabled;
	hidden var mArcAngle;
	
	function initialize(params) 
	{
		Drawable.initialize(params);
		mEnabled = false;
		mArcAngle = 0;
	}
	
	function disable()
	{
		mEnabled = false;
	}
	
	function enable()
	{
		mEnabled = true;
	}
	
	function isEnabled()
	{
		return mEnabled;
	}
	
	function setAngle(arcAngle)
	{
		mArcAngle = arcAngle;
	}
	
	function draw(dc) 
	{
		if (mEnabled)
		{
			dc.setPenWidth(3);
			dc.setColor(Gfx.COLOR_DK_GREEN, Gfx.COLOR_TRANSPARENT);
			dc.setPenWidth(16);
			dc.drawCircle(locX, locY,8);
			dc.setColor(Gfx.COLOR_GREEN, Gfx.COLOR_TRANSPARENT);
			dc.drawArc(locX, locY, 8, Gfx.ARC_CLOCKWISE, mArcAngle+60,mArcAngle);
			dc.setColor(Gfx.COLOR_DK_BLUE, Gfx.COLOR_TRANSPARENT);
			dc.setPenWidth(1);
			dc.fillCircle(locX, locY,2);
		}
	}
}