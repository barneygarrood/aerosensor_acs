using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class vv_data_field_large extends Ui.Drawable
{
	hidden var mHeadingText;
	hidden var mValueText;
	hidden var mValueColor;
	hidden var mMsgText;
	hidden var mMsgColor;
	hidden var mUnitText;
	hidden var mValueFont;
	
	function setHeadingText(text){mHeadingText = text;}
	function setValueText(text){mValueText = text;}
	function setMsgText(text){mMsgText = text;}
	function setUnitText(text){mUnitText = text;}
	function setValueColor(color){mValueColor = color;}
	function setMsgColor(color){mMsgColor = color;}
	function setValueFont(font){mValueFont = font;}
	
	function initialize(params) 
	{
		Drawable.initialize(params);
		mHeadingText = params[:headingText];
		mValueText = params[:valueText];
		mValueColor = params[:valueColor];
		mMsgText = params[:messageText];
		mMsgColor = params[:messageColor];
		mUnitText = params[:unitText];
	}

	function drawText(dc,x,y,font,color,text)
	{
    	dc.setColor(color,Gfx.COLOR_TRANSPARENT);
    	dc.drawText(x,y,font,text,Gfx.TEXT_JUSTIFY_LEFT);
	}

	function draw(dc)
	{
		var w=dc.getWidth();
		// Header:
		var font = Gfx.FONT_SMALL;
		drawText(dc,1, locY+1, font, as_colors.foreground, mHeadingText);
		
		// Value:
		if (mValueFont == null)
		{
			font = Gfx.FONT_NUMBER_THAI_HOT;
		}
		else
		{
			font = mValueFont;
		}
		var textHeight = dc.getFontHeight(font);
		var textWidth = dc.getTextWidthInPixels(mValueText,font);
		drawText(dc,w/2 - textWidth/2, locY + height/2-textHeight*4/10, font, mValueColor,mValueText);
		
		// Message:
		if (mMsgText!=null)
		{
			font = Gfx.Graphics.FONT_SMALL;
			textHeight = dc.getFontHeight(font);
			textWidth = dc.getTextWidthInPixels(mMsgText,font);
			drawText(dc,w/2 - textWidth/2, locY + height-textHeight-1, font, mMsgColor,mMsgText);
		}
		
		// Units:
		font = Gfx.FONT_TINY;
		textHeight = dc.getFontHeight(font);
		textWidth = dc.getTextWidthInPixels(mUnitText,font);
		drawText(dc,w - textWidth-1, locY + height-textHeight-1, font, as_colors.foreground,mUnitText);
		
	}
}