using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.AntPlus;	

class vv_passpair_menu extends Ui.View
{
	hidden var m_selectedItem = 0;
	
	function selectedItem()
	{
		return m_selectedItem;
	}
	
	function initialize()
	{
		Ui.View.initialize();
	}
	
	function drawText(dc,x,y,font,color,text,centrex, centrey)
	{
    	dc.setColor(color,Gfx.COLOR_TRANSPARENT);
    	if (centrex)
    	{
    		x -= dc.getTextWidthInPixels(text,font)/2;
    	}
    	if (centrey)
    	{
    		y -= dc.getFontHeight(font)/2;
    	}
    	
    	dc.drawText(x,y,font,text,Gfx.TEXT_JUSTIFY_LEFT);
	}
	
	// Returns device state.
	function getDeviceStateTextFromEnum(state)
	{
		var text = "";
		switch (state)
		{
			case AntPlus.DEVICE_STATE_DEAD:
				text = "Dead";
				break;
			case AntPlus.DEVICE_STATE_CLOSED:
				text = "Closed";
				break;
			case AntPlus.DEVICE_STATE_SEARCHING:
				text = "Searching";
				break;
			case AntPlus.DEVICE_STATE_TRACKING:
				text = "Tracking";
				break;
			case AntPlus.DEVICE_STATE_CNT:
				text = "Cnt (not sure..)";
				break;
		}
		return text;
	}
	
	function areEnoughSensorsPairedOnHead()
	{
		if (!vv_sensors.bpwr.isTracking()){return false;}
		else if (vv_sensors.bsc.isTracking()){return true;}
		else if (vv_sensors.bspd.isTracking()){return true;}
		return false;
	}
	
	function doHeadAndAerosensorMatch()
	{
		if(vv_sensors.bpwr.getDeviceNumber() != vv_sensors.aerosensor.data.pairedDevices[0].deviceNumber)
		{
			return false;
		}
		else if (vv_sensors.bspd.isTracking())
		{
			if (vv_sensors.bspd.getDeviceNumber() != vv_sensors.aerosensor.data.pairedDevices[1].deviceNumber)
			{
				return false;
			}
			return true;
		}
		else if (vv_sensors.bsc.isTracking())
		{
			if (vv_sensors.bsc.getDeviceNumber() != vv_sensors.aerosensor.data.pairedDevices[2].deviceNumber)
			{
				return false;
			}
			return true;
		}
		return false;			
	}
	
	function onLayout(dc)
	{
        setLayout(Rez.Layouts.PassPairLayout(dc));
        // Rejig heights - easier to do in code:
        var row_height = (dc.getHeight()-vv_icons.height)/6;
        var w = dc.getWidth();
        var drw;
        
        drw = self.findDrawableById("header");
        drw.setMainText("Pass-Pairing");
        drw.setSubText("");
        drw.locX = 0;
        drw.locY = vv_icons.height;
        drw.height = row_height;
        drw.width = w;
		
        drw = self.findDrawableById("edgeHeader");
        drw.setMainText("Edge");
        drw.setSubText("");
        drw.locX = 0;
        drw.locY = vv_icons.height+row_height;
        drw.height = row_height;
        drw.width = w/2;
        
        drw = self.findDrawableById("aerosensorHeader");
        drw.setMainText("Aerosensor");
        drw.setSubText("");
        drw.locX = w/2;
        drw.locY = vv_icons.height+row_height;
        drw.height = row_height;
        drw.width = w/2;
        
        drw = self.findDrawableById("edgePwr");
        drw.locX = 0;
        drw.locY = vv_icons.height+row_height*2;
        drw.height = row_height;
        drw.width = w/2;
        
        drw = self.findDrawableById("aerosensorPwr");
        drw.locX = w/2;
        drw.locY = vv_icons.height+row_height*2;
        drw.height = row_height;
        drw.width = w/2;
        
        drw = self.findDrawableById("edgeSpd");
        drw.locX = 0;
        drw.locY = vv_icons.height+row_height*3;
        drw.height = row_height;
        drw.width = w/2;
        
        drw = self.findDrawableById("aerosensorSpd");
        drw.locX = w/2;
        drw.locY = vv_icons.height+row_height*3;
        drw.height = row_height;
        drw.width = w/2;
        
        drw = self.findDrawableById("edgeBsc");
        drw.locX = 0;
        drw.locY = vv_icons.height+row_height*4;
        drw.height = row_height;
        drw.width = w/2;
        
        drw = self.findDrawableById("aerosensorBsc");
        drw.locX = w/2;
        drw.locY = vv_icons.height+row_height*4;
        drw.height = row_height;
        drw.width = w/2;
        
        drw = self.findDrawableById("status");
        drw.locX = 0;
        drw.locY = vv_icons.height+row_height*5;
        drw.height = row_height;
        drw.width = w;
	}
	
	function onUpdate(dc)
	{
		reDrawAll(dc);
        View.onUpdate(dc);
	}
	
	function setButtonTextAndColor(mainText, subText, color, butName)
	{		
		var drw = self.findDrawableById(butName);
		if (drw!=null)
		{
			drw.setMainText(mainText);
			drw.setSubText(subText);
			drw.setColor(color);
		}
	}
	
	function reDrawAll(dc)
	{
		var color;
		var text;
		
		// PWR devices:
		if (m_selectedItem == 0){color = Gfx.COLOR_BLUE;}else{color = Gfx.COLOR_DK_BLUE;}
		// Edge:
		var mainText =  "PWR #" + vv_sensors.bpwr.getDeviceNumber();
		var subText =  vv_sensors.bpwr.getDeviceStateText();
		setButtonTextAndColor(mainText,subText,color, "edgePwr");
		// Aerosensor:
		mainText =  "PWR #" + vv_sensors.aerosensor.data.pairedDevices[0].deviceNumber;
		subText =  getDeviceStateTextFromEnum(vv_sensors.aerosensor.data.pairedDevices[0].channelState);
		setButtonTextAndColor(mainText,subText,color, "aerosensorPwr");
		
		// SPD devices:
		if (m_selectedItem == 1){color = Gfx.COLOR_BLUE;}else{color = Gfx.COLOR_DK_BLUE;}
		// Edge:
		mainText =  "SPD #" + vv_sensors.bspd.getDeviceNumber();
		subText =  vv_sensors.bspd.getDeviceStateText();
		setButtonTextAndColor(mainText,subText,color, "edgeSpd");
		// Aerosensor:
		mainText =  "SPD #" + vv_sensors.aerosensor.data.pairedDevices[1].deviceNumber;
		subText =  getDeviceStateTextFromEnum(vv_sensors.aerosensor.data.pairedDevices[1].channelState);
		setButtonTextAndColor(mainText,subText,color, "aerosensorSpd");
		
		// BSC devices:
		if (m_selectedItem == 2){color = Gfx.COLOR_BLUE;}else{color = Gfx.COLOR_DK_BLUE;}
		// Edge:
		mainText =  "BSC #" + vv_sensors.bsc.getDeviceNumber();
		subText =  vv_sensors.bsc.getDeviceStateText();
		setButtonTextAndColor(mainText,subText,color, "edgeBsc");
		// Aerosensor:
		mainText =  "BSC #" + vv_sensors.aerosensor.data.pairedDevices[2].deviceNumber;
		subText =  getDeviceStateTextFromEnum(vv_sensors.aerosensor.data.pairedDevices[2].channelState);
		setButtonTextAndColor(mainText,subText,color, "aerosensorBsc");
		
		//Status panel:
		if (areEnoughSensorsPairedOnHead())
		{
			if (doHeadAndAerosensorMatch())
			{
				if (m_selectedItem == 3){color = Gfx.COLOR_DK_GREEN;}else{color = Gfx.COLOR_DK_GREEN;}
				mainText = "Aerosensor matches head unit.";
				subText = "Already tracking.";
			}
			else
			{
				if (m_selectedItem == 3){color = Gfx.COLOR_DK_BLUE;}else{color = Gfx.COLOR_PURPLE;}
				mainText = "Aerosensor devices don't match.";
				subText = "Enter to sync.";
			}
		} 
		else
		{
			if (m_selectedItem == 3){color = Gfx.COLOR_RED;}else{color = Gfx.COLOR_DK_RED;}
			mainText = "Head unit not tracking.";
			subText = "Setup head unit first.";
		}
		setButtonTextAndColor(mainText,subText,color,"status");
	}
	
	function incrementSelectedItem()
	{
		m_selectedItem++;
		if (m_selectedItem>3)
		{
			m_selectedItem=0;
		}
		Ui.requestUpdate();
	}
	
	function decrementSelectedItem()
	{
		m_selectedItem--;
		if (m_selectedItem<0)
		{
			m_selectedItem=3;
		}
		Ui.requestUpdate();
	}
	
	function setSelectedItem(index)
	{
		m_selectedItem = index;
	}
}
