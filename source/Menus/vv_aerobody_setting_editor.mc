using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_aerobody_setting_editor extends Ui.View
{
	var selection;
	var value;
	const butCount = 6;
	const butNames = ["AddMaj","SubMaj","AddMin","SubMin", "Save", "Cancel"];
	
	function initialize()
	{
		selection = 0;
		value = vv_settings.values[vv_settings.NULLZONE];
		Ui.View.initialize();
	}
	
	function onLayout(dc)
	{
        setLayout(Rez.Layouts.SettingEditLayout(dc));
        // Rejig heights - easier to do in code:
        var row_height = (dc.getHeight()-vv_icons.height)/5;
        var drw;
        drw = self.findDrawableById("Header");
        drw.locY = vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("AddMaj");
        drw.locY = row_height+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("AddMin");
        drw.locY = row_height+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("SubMaj");
        drw.locY = row_height*3+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("Value");
        drw.locY = row_height*2+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("SubMin");
        drw.locY = row_height*3+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("Save");
        drw.locY = row_height*4+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("Cancel");
        drw.locY = row_height*4+vv_icons.height;
        drw.height = row_height;
        
		// Set title:
		var text = "Aerobody Null Zone";
		setButtonText(text,"Header");
		
		// Set button texts:
		text = "+ 1.0";
		setButtonText(text,"AddMaj");
		
		text = "- 1.0";
		setButtonText(text,"SubMaj");
		
		text = "+ 0.1";
		setButtonText(text,"AddMin");
		
		text = "- 0.1";
		setButtonText(text,"SubMin");
		
		text = "Save";
		setButtonText(text,"Save");
		
		text = "Cancel";
		setButtonText(text,"Cancel");
        
		reDrawAll(dc);
	}
	
	function onUpdate(dc)
	{
		reDrawAll(dc);
        View.onUpdate(dc);
	}
	
	function setButtonText(text, butName)
	{		
		var drw = self.findDrawableById(butName);
		if (drw!=null)
		{
			drw.setMainText(text);
			drw.setSubText("");
		}
	}
	
	function reDrawAll(dc)
	{
		// Set value:
		var text = (value/10.0f).format("%.1f") + " cm";
		setButtonText(text,"Value");
		// Select correct button:
		for (var i=0;i<butCount;i++)
		{
			var drw = self.findDrawableById(butNames[i]);
			if (i == selection)
			{
				drw.setColor(Gfx.COLOR_BLUE);
			}
			else
			{
				drw.setColor(Gfx.COLOR_DK_BLUE);
			}
		}
	}
		
	function incrementSelectedItem()
	{
		selection++;
		if (selection>=6)
		{
			selection=0;
		}
		Ui.requestUpdate();
	}
	
	function decrementSelectedItem()
	{
		selection--;
		if (selection<0)
		{
			selection=6-1;
		}
		Ui.requestUpdate();
	}
	
	
}