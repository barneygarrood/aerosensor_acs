using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using aerosensor_module as Aerosensor;

class vv_aerosensor_demo_mode_menu_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden const BUTTON_COUNT = 6;
	hidden var _changingMode = 0;
	
    function initialize(view) 
    {
        InputDelegate.initialize();
    	m_view = view;
        // Reset modes:
        vv_sensors.aerosensor.clearModes();
        // Request mode page:
        vv_sensors.aerosensor.sendRequestPageCommand(224);
    	m_view.preUpdateCallback = method(:updateMenu);
		_changingMode = 0;
        
		view.title = "Aerosensor Demo Mode";
        // Reset menu:
        resetMenu();
    }
    
    function updateMenu()
    {
    	if (vv_sensors.aerosensor.searching)
    	{
    		m_view.subtitle = "Not connected.";
    	}
    	else if (vv_sensors.aerosensor.data.demoMode == -1)
    	{	
    		if (_changingMode)
    		{
    			m_view.subtitle = "Changing mode";
    		}
    		else
    		{
    			m_view.subtitle = "Requesting mode";
    		}
    	}
    	else
    	{
    		m_view.subtitle = "Mode = " + vv_sensors.aerosensor.demoModeText();
    	}
    }
    
    
    function resetMenu()
    {
    	// Clear menu:
    	m_view.resetMenu();
    	m_view.addItem("Default Mode", "Default");
    	m_view.addItem("Demo Mode 1", "Constant all");
    	m_view.addItem("Demo Mode 2", "True baro");
    	m_view.addItem("Demo Mode 3", "Sine wave baro");
    	m_view.addItem("Demo Mode 4", "Match airspeed");
    	m_view.addItem("Demo Mode 5", "Match airspeed for calc");
    }
	    
	function actionButton(index)
    {
    	var view;
    	if (index!=null)
    	{
    		m_view.setSelectedItem(index);
    	}
    	// Clear mode:
        vv_sensors.aerosensor.clearModes();
		_changingMode = 1;
    	vv_sensors.aerosensor.setDemoMode(m_view.selectedItem());
    	// Request mode page:
        vv_sensors.aerosensor.sendRequestPageCommand(224);
    }
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}

    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	// check arrow buttons:
		if (isPressed("button_up",coords))
		{
			m_view.decrementSelectedItem();
			Ui.requestUpdate();
		}
		else if (isPressed("button_down",coords))
		{
			m_view.incrementSelectedItem();
			Ui.requestUpdate();
		}
    	for (var i= 0;i<BUTTON_COUNT;i++)
    	{
    		if (isPressed("button" + i,coords))
    		{
    			actionButton(i);
    			break;
    		}
    	}
    }
    
    
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    	case Ui.KEY_MENU:
	    		var view;
	    		actionButton(m_view.selectedItem());
	    		break;
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
}	