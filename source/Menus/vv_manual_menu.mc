using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_manual_menu extends Ui.View
{
	hidden var m_items;
	hidden var m_item_count;
	hidden var m_selectedItem;
	hidden var m_rows = 5;
	hidden var m_titles;
	hidden var m_subtitles;
	hidden const MAX_ITEMS = 32;
	hidden var m_start;
	hidden var enableUpdate;
	
	var preUpdateCallback = null;
	var postUpdateCallback = null;
	var onShowUpdateCallback = null;
	var specialItems;
	
	var title = "";
	var subtitle = "";
	hidden var mRadarEnabled = false;
	
	function enableRadar()
	{
		mRadarEnabled = true;
	}
	
	function firstButton()
	{
		return m_start;
	}
	
	function selectedItem()
	{
		return m_selectedItem;
	}

	function initialize()
	{
		Ui.View.initialize();
		enableUpdate = false;
	}
	
	function resetMenu()
	{
		m_titles = new[MAX_ITEMS];
		m_subtitles = new[MAX_ITEMS];
		m_item_count = 0;
		m_selectedItem = 0;
		m_start = 0;
	}
	
	function drawText(dc,x,y,font,color,text)
	{
    	dc.setColor(color,Gfx.COLOR_TRANSPARENT);
    	dc.drawText(x,y,font,text,Gfx.TEXT_JUSTIFY_LEFT);
	}
	function onLayout(dc)
	{
		enableUpdate = false;
        setLayout(Rez.Layouts.ManualMenuLayout(dc));
    	if (mRadarEnabled)
    	{
    		self.findDrawableById("radar").enable();
    	}
		reDrawAll(dc);
		enableUpdate = true;
	}
	
	function onShow()
	{
		if (onShowUpdateCallback != null)
		{
			onShowUpdateCallback.invoke();
		}
	}
	
	function onUpdate(dc)
	{
		if (enableUpdate)
		{
			reDrawAll(dc);
		}
        View.onUpdate(dc);
	}
	
	function reDrawAll(dc)
	{
    	// Pre update callback, to update menu items if necessary.
    	if (preUpdateCallback!=null)
    	{
    		preUpdateCallback.invoke();
    	}
    	var arrowsEnabled = false;
    	if (m_item_count > m_rows)
    	{
    		self.findDrawableById("button_up").enable();
    		self.findDrawableById("button_down").enable();
    		arrowsEnabled = true;
    	}
    	else
    	{
    		self.findDrawableById("button_up").disable();
    		self.findDrawableById("button_down").disable();
    		arrowsEnabled = false;
    	}
    	
    	var butWidth;
    	if (!arrowsEnabled)
    	{
    		butWidth = dc.getWidth();
    		for (var i=0;i<m_rows;i++)
    		{
    			self.findDrawableById("button"+i).setWidth(butWidth);
    		}
    	}
    	
    	if ((m_selectedItem-m_start+1)>m_rows)
    	{
    		m_start = m_selectedItem - m_rows+1;
    	}
    	
    	else if (m_selectedItem<m_start)
    	{
    		m_start = m_selectedItem;
    	}
    	var end=m_start+m_rows-1;
    	if (end>=m_item_count)
    	{
    		end = m_item_count-1;
    	}
		// Set title:
		var drw = self.findDrawableById("header");
		drw.setMainText(title);
		drw.setSubText(subtitle);     	
    	// Draw items:
    	for (var i=m_start;i<=end;i++)
    	{
			drw = self.findDrawableById("button"+(i-m_start));
	    	drw.setMainText(m_titles[i]);
	    	drw.setSubText(m_subtitles[i]);
	    	if (i == m_selectedItem)
	    	{
	    		if (specialItems != null)
	    		{
	    			if (specialItems[i])
	    			{
	    				drw.setColor(Gfx.COLOR_RED);
	    			}
	    			else
	    			{
	    			drw.setColor(Gfx.COLOR_BLUE);
	    			}
	    		}
	    		else
	    		{
	    			drw.setColor(Gfx.COLOR_BLUE);
	    		}
	    	}
	    	else
	    	{
	    		if (specialItems != null)
	    		{
	    			if (specialItems[i])
	    			{
	    				drw.setColor(Gfx.COLOR_DK_RED);
	    			}
	    			else
	    			{
	    			drw.setColor(Gfx.COLOR_DK_BLUE);
	    			}
	    		}
	    		else
	    		{
	    			drw.setColor(Gfx.COLOR_DK_BLUE);
	    		}
	    	}
	    	drw.enable();
    	}
    	for (var i = end+1; i<m_rows; i++)
    	{
			drw = self.findDrawableById("button"+(i-m_start));
    		drw.disable();
    	}
    	
    	// Post update callback to draw anything else on screen:
    	if (postUpdateCallback!=null)
    	{
    		postUpdateCallback.invoke(dc);
    	}
	}
	
	function addItem(title,subtitle)
	{
		m_titles[m_item_count] = title;
		m_subtitles[m_item_count] = subtitle;
		m_item_count++;
	}	
	
	function modItem(index, title,subtitle)
	{
		m_titles[index] = title;
		m_subtitles[index] = subtitle;
	}
	
	function incrementSelectedItem()
	{
		m_selectedItem++;
		if (m_selectedItem>=m_item_count)
		{
			m_selectedItem=0;
		}
		Ui.requestUpdate();
	}
	
	function decrementSelectedItem()
	{
		m_selectedItem--;
		if (m_selectedItem<0)
		{
			m_selectedItem=m_item_count-1;
		}
		Ui.requestUpdate();
	}
	
	function setSelectedItem(index)
	{
		m_selectedItem = m_start+index;
	}
	
	
}