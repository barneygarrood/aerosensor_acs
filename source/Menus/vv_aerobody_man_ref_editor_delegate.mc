using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_aerobody_man_ref_editor_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden var m_timer;
	hidden var m_start_time;
	
    function initialize(view)
    {
        InputDelegate.initialize();
    	m_view = view;
        vv_sensors.aerobody.ref_status = REF_STATUS_NONE;
    }
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if (button.isPointInside(coords))
				{
					ret = true;
				}
			}
		}
		return ret;
	}
	
    function actionButton(index)
    {
    	switch (index)
    	{
    		case 0:
    			m_view.start_meas();
    			break;
    		case 1:
    			// Save values:
    			vv_sensors.aerobody.setHeadRef(m_view.headRef);
    			vv_sensors.aerobody.setChestRef(m_view.chestRef);
    			vv_sensors.aerobody.store_datums();
				m_view.selection = 10;
    			break;
    		case 2:
				Ui.popView(SLIDE_LEFT);
				Ui.requestUpdate();
    			break;
    	}
    }
    
    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	for (var i= 0;i<m_view.butCount;i++)
    	{
    		if (isPressed(m_view.butNames[i],coords))
    		{
    			actionButton(i);
    			break;
    		}
    	}
    }
    
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    		var view;
	    		actionButton(m_view.selection);
	    		break;
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
}	
