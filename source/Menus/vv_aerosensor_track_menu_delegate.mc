using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using aerosensor_module as Aerosensor;

class vv_aerosensor_track_menu_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden const BUTTON_COUNT = 3;
	hidden var specialItems = new[BUTTON_COUNT];	// Set to true to colour items red.
	
    function initialize(view) 
    {
        InputDelegate.initialize();
    	m_view = view;
    	m_view.preUpdateCallback = method(:updateMenu);
        
		view.title = "Aerosensor Track Layout";
		for (var i=0;i<3;i++)
		{
			specialItems[i]=false;
		}
		// Reset flags
		vv_sensors.aerosensor.trackReset();
		// Request settings
		vv_sensors.aerosensor.sendRequestTrackCommand();
        // Reset menu:
        resetMenu();    	
    }
    
    function resetMenu()
    {
    	// Clear menu:
    	m_view.resetMenu();
    	m_view.addItem("Get Layout", vv_sensors.aerosensor.getPullStatusText(vv_sensors.aerosensor.track.status));
    	m_view.addItem("Edit Layout","View and edit layout");
    	m_view.addItem("Push Layout", vv_sensors.aerosensor.getPushStatusText(vv_sensors.aerosensor.track.status));
    	updateMenu();
    }
    
    function updateMenu()
    {
    	// Clear menu:
    	m_view.modItem(0,"Get Layout", vv_sensors.aerosensor.getPullStatusText(vv_sensors.aerosensor.track.status));
    	m_view.modItem(2,"Push layout", vv_sensors.aerosensor.getPushStatusText(vv_sensors.aerosensor.track.status));    	
    	specialItems[2] = vv_sensors.aerosensor.track.status.isDirty;
    	m_view.specialItems = specialItems;
    }
	    
	function actionButton(index)
    {
    	var view;
    	if (index!=null)
    	{
    		m_view.setSelectedItem(index);
    	}
    	switch (index)
    	{
			case 0:
	        	vv_sensors.aerosensor.sendRequestTrackCommand();//m_view);
				break;
			case 1:
				if ((vv_sensors.aerosensor.track.status.settingsPull == Aerosensor.SETTINGS_COMPLETE) & 
						(vv_sensors.aerosensor.track.count > 0))
				{
		    		view = new vv_manual_menu();
					WatchUi.pushView(view, new vv_aerosensor_track_edit_menu_delegate(view), Ui.SLIDE_LEFT);
				}
				break;
			case 2:
				if (vv_sensors.aerosensor.track.status.isDirty)
				{
					vv_sensors.aerosensor.sendTrack();
				}
				break;
    	}
    }
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}

    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	// check arrow buttons:
		if (isPressed("button_up",coords))
		{
			m_view.decrementSelectedItem();
			Ui.requestUpdate();
		}
		else if (isPressed("button_down",coords))
		{
			m_view.incrementSelectedItem();
			Ui.requestUpdate();
		}
    	for (var i= 0;i<BUTTON_COUNT;i++)
    	{
    		if (isPressed("button" + i,coords))
    		{
    			actionButton(i);
    			break;
    		}
    	}
    }
    
    
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    	case Ui.KEY_MENU:
	    		var view;
	    		switch (m_view.selectedItem())
	    		{
	    			case 0:
    					actionButton(0);
	    				break;
	    			case 1:
    					actionButton(1);
	    				break;
	    			case 2:
    					actionButton(2);
	    				break;
	    			case 2:
    					actionButton(3);
	    				break;
	    		}
	    		break;
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
}	