using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_connection_menu_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden var m_button_count;
	
    function initialize(view) 
    {
        InputDelegate.initialize();
    	m_view = view;
        
		view.title = "Connections menu";
    	if (vv_settings.values[vv_settings.AEROSENSOR_ACTIVE])
    	{
    		m_button_count = 3;
    	}
    	else
    	{
    		m_button_count = 1;
    	}
        // Reset menu:
        logging.log("Button count = " + m_button_count);
        resetMenu();
    }
    
    function resetMenu()
    {
    	// Clear menu:
    	m_view.resetMenu();
    	if (vv_settings.values[vv_settings.AEROSENSOR_ACTIVE])
    	{
	    	m_view.addItem("Aerosensor", "Aero Sensor");
	    	m_view.addItem("Aerobody", "Body Position Sensor");
	    	m_view.addItem("Pass-pair",	"Pair Aerosensor to bike sensors.");
    	}
    	else
    	{
    		m_view.addItem("Aerobody", "Body Position Sensor");
    	}
    }
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}
	
    function actionButton(index)
    {
		var view = new vv_manual_menu();
		var delegate;
		
    	if (vv_settings.values[vv_settings.AEROSENSOR_ACTIVE])
    	{
			switch (index)
			{
				case 0:
					delegate = new vv_con_aerosensor_menu_delegate(view);
					break;
				case 1:
					delegate = new vv_con_aerobody_menu_delegate(view);
					break;
				case 2:
		    		view = new vv_passpair_menu();
					delegate =  new vv_passpair_menu_delegate(view);
					break;
			}
		}
		else
		{
			delegate = new vv_con_aerobody_menu_delegate(view);
		}
		Ui.pushView(view, delegate, Ui.SLIDE_LEFT);
    }
    
    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	
    	if (isPressed("button_up",coords))
		{
			m_view.decrementSelectedItem();
			Ui.requestUpdate();
		}
		else if (isPressed("button_down",coords))
		{
			m_view.incrementSelectedItem();
			Ui.requestUpdate();
		}
		
    	for (var i= 0;i<m_button_count;i++)
    	{
    		{
	    		if (isPressed("button" + i,coords))
	    		{
	    			actionButton(i);
	    			break;
    			}
    		}
    	}
    }
	
	
    
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    		actionButton(m_view.selectedItem());
	    		break;
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
}	