using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using aerosensor_module as aerosensor;
using Toybox.Graphics as Gfx;
using Toybox.Timer;

class vv_passpair_menu_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden var m_timer;
	hidden var butNames = {0=>"edgePwr", 1=>"aerosensorPwr", 2=>"edgeSpd", 3=>"aerosensorSpd", 4=>"edgeBsc", 5=>"aerosensorBsc", 6=>"status"};
	hidden const BUTTON_COUNT = 7;
    function initialize(view) {
        InputDelegate.initialize();
    	m_view = view;
        m_timer = new Timer.Timer();
        m_timer.start(method(:timerCallback),500,true);
    }
    
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}
    
    function actionButton(index)
    {
    	switch (index)
    	{
			case 0:
				vv_sensors.aerosensor.sendOpenChannelCommand(11,vv_sensors.bpwr.getDeviceNumber());
				break;
			case 1:
				vv_sensors.aerosensor.sendOpenChannelCommand(123,vv_sensors.bspd.getDeviceNumber());
				break;
			case 2:
				vv_sensors.aerosensor.sendOpenChannelCommand(121,vv_sensors.bsc.getDeviceNumber());
				break;
			case 3:
				vv_sensors.aerosensor.sendOpenChannelCommand(11,vv_sensors.bpwr.getDeviceNumber());
				// Need to decide which of spd and spdcad to send:
				if (vv_sensors.bspd.isTracking())
				{
					vv_sensors.aerosensor.sendOpenChannelCommand(123,vv_sensors.bspd.getDeviceNumber());
				}
				else if (vv_sensors.bsc.isTracking())
				{
					vv_sensors.aerosensor.sendOpenChannelCommand(121,vv_sensors.bsc.getDeviceNumber());
				}
			default:
				break;
    	}
    }
    	
    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	for (var i= 0;i<BUTTON_COUNT;i++)
    	{
    		if (isPressed(butNames[i],coords))
    		{
    			var index=0;
    			if (i<2){index = 0;}
    			else if (i<4){index = 1;}
    			else if (i<6){index = 2;}
    			else {index = 3;}
				m_view.setSelectedItem(index);
				actionButton(index);
    			break;
    		}
    	}
    }
    	
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    		actionButton(m_view.selectedItem());
	    		break;
	    	case Ui.KEY_ESC:
	   			m_timer.stop();
	   			m_timer = null;
	    		break;
		}
	}
	
	function timerCallback()
	{
		Ui.requestUpdate();
	}
}