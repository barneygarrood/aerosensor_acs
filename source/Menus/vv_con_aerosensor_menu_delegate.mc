using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using aerosensor_module as aerosensor;
using Toybox.Timer;
using Toybox.Graphics as Gfx;

class vv_con_aerosensor_menu_delegate extends Ui.InputDelegate {
	
	hidden var m_view;
	hidden var m_scanner;
	hidden var m_timer;
	hidden var m_arcAngle=0;
	hidden const BUTTON_COUNT = 5;
	
    function initialize(view) {
        InputDelegate.initialize();
    	m_view = view;
        
		view.title = "aerosensor search";
		view.subtitle = "0 devices found";
        // Reset menu:
        resetMenu();
        // Start scanner;
        var callback = self.method(:update);
        m_scanner = new aerosensor.aerosensor_scanner(callback,vv_sensors.aerosensor.deviceCfg.deviceNumber);
        m_timer = new Timer.Timer();
        m_timer.start(method(:timerCallback),100,true);
        m_view.enableRadar();
    }
    
    function resetMenu()
    {
    	// Clear menu:
    	m_view.resetMenu();
    	// Add starting sensor.
    	var title = "Current ID = " + vv_sensors.aerosensor.deviceCfg.deviceNumber;
    	var subtitle = "Connected";
    	
    	if (vv_sensors.aerosensor.searching == true)
    	{
    		subtitle = "Searching...";
    	}
    	m_view.addItem(title,subtitle);
    }
	
	function update(devices,deviceID, rssi, addNew)
	{
		if (addNew)
		{
			m_view.subtitle = deviceID + " devices found.";
			m_view.addItem ("Device Id = " + devices[deviceID],
							"Signal strength = " + rssi);
		}
		else
		{
			m_view.modItem(deviceID+1,"Device Id = " + devices[deviceID],
							"Signal strength = " + rssi);
		}
	}	
	    
	function actionButton(index)
    {
    	if (index!=null)
    	{
    		m_view.setSelectedItem(index);
    	}
	    changeDevice();
    }
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}

    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	// check arrow buttons:
		if (isPressed("button_up",coords))
		{
			m_view.decrementSelectedItem();
			Ui.requestUpdate();
		}
		else if (isPressed("button_down",coords))
		{
			m_view.incrementSelectedItem();
			Ui.requestUpdate();
		}
    	for (var i= 0;i<BUTTON_COUNT;i++)
    	{
    		if (isPressed("button" + i,coords))
    		{
    			actionButton(i);
    			break;
    		}
    	}
    }
	
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    		changeDevice();
	    		break;
	    	case Ui.KEY_ESC:
	   			m_scanner.stopSearch();
	   			m_scanner.release();
	   			m_scanner = null;
	   			m_timer.stop();
	   			m_timer = null;
	    		break;
		}
	}
	
	function changeDevice()
	{
		if (m_view.selectedItem() > 0)
		{
			vv_sensors.aerosensor.changeDeviceID(m_scanner.devices[m_view.selectedItem()-1]);
			m_scanner.defaultDevice = vv_sensors.aerosensor.deviceCfg.deviceNumber;
			m_scanner.reset();
			resetMenu();
			Ui.requestUpdate();
		}
	}
	
	function timerCallback()
	{
		if (m_arcAngle == 0)
		{
			m_arcAngle = 360;
		}
		m_arcAngle-=15;
		m_view.findDrawableById("radar").setAngle(m_arcAngle);
    	var title = "Current ID = " + vv_sensors.aerosensor.deviceCfg.deviceNumber;
    	var subtitle = "Connected";
		if (vv_sensors.aerosensor.searching)
		{
			subtitle = "Searching...";
		}
		
		m_view.modItem(0,title,subtitle);
		Ui.requestUpdate();
	}
}