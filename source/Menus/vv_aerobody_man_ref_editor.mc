using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Timer;

class vv_aerobody_man_ref_editor extends Ui.View
{
	var index;
	var selection;
	var value;
	const butCount = 3;
	const butNames = ["Measure", "Save", "Cancel"];
	var headRef = null;
	var chestRef = null;
	var measuring_datum = false;
	var m_start_time;
	var m_timer;
		
	
	function initialize()
	{
		selection = 1;
		headRef = vv_sensors.aerobody.getHeadRef();
		chestRef = vv_sensors.aerobody.getChestRef();
		Ui.View.initialize();
	}
	
	function drawText(dc,x,y,font,color,text)
	{
    	dc.setColor(color,Gfx.COLOR_TRANSPARENT);
    	dc.drawText(x,y,font,text,Gfx.TEXT_JUSTIFY_LEFT);
	}
	
	function onLayout(dc)
	{
        setLayout(Rez.Layouts.AerobodyRefEditLayout(dc));
        
		// Set title:
		var text = "Aerobody Reference";
		setButtonText(text,"Header");
		
		text = "Save";
		setButtonText(text,"Save");
		
		text = "Close";
		setButtonText(text,"Cancel");
        
		reDrawAll(dc);
	}
	
	function onUpdate(dc)
	{
		reDrawAll(dc);
        View.onUpdate(dc);
	}
	
	function setButtonColor(color, butName)
	{		
		var drw = self.findDrawableById(butName);
		if (drw!=null)
		{
			drw.setColor(color);
		}
	}
	function setFieldBkgColor(color, butName)
	{		
		var drw = self.findDrawableById(butName);
		if (drw!=null)
		{
			drw.setBackgroundColor(color);
		}
	}
	function setButtonText(text, butName)
	{		
		var drw = self.findDrawableById(butName);
		if (drw!=null)
		{
			drw.setMainText(text);
		}
	}
	
	function setButtonSubText(text, butName)
	{		
		var drw = self.findDrawableById(butName);
		if (drw!=null)
		{
			drw.setSubText(text);
		}
	}
	
	function activateButton(butName)
	{
		var drw = self.findDrawableById(butName);
		if (drw!=null){drw.activate();}	
	}
	
	function deactivateButton(butName)
	{
		var drw = self.findDrawableById(butName);
		if (drw!=null){drw.deactivate();}		
	}
	
	function setField(header,valueText,units,fieldName)
	{	
		var drw = self.findDrawableById(fieldName);
		if (drw!=null)
		{
			drw.setHeadingText(header);
			drw.setValueText(valueText);
			drw.setUnitText(units);
		}
	}
	
	function reDrawAll(dc)
	{
		var text = "";
		if (measuring_datum)
		{
			headRef = vv_sensors.aerobody.getHeadPos();
			chestRef = vv_sensors.aerobody.getChestPos();
		}
		if (headRef != null)
		{
			text = (headRef/10.0f).format("%.1f");	
		} else {text = "---";}
		setField("Head",text,"cm","Value1");
		
		if (chestRef != null)
		{
			text = (chestRef/10.0f).format("%.1f");	
		} else {text = "---";}
		setField("Chest",text,"cm","Value2");
		
		// Activate/deactivate:
		if (vv_sensors.aerobody.searching)
		{
			deactivateButton("Measure");
		}
		else
		{
			activateButton("Measure");
		}
		var dirty1 = false;
		var dirty2 = false;
		if (headRef != vv_settings.values[vv_settings.HEADREF])
		{
			dirty1 = true;
		} 
		if (chestRef != vv_settings.values[vv_settings.CHESTREF])
		{
			dirty2 = true;
		}
		if (dirty1 | dirty2)
		{
			setButtonColor(Gfx.COLOR_DK_BLUE,"Save");
			if (dirty1)	{setFieldBkgColor(Gfx.COLOR_RED,"Value1");}
			else {setFieldBkgColor(Gfx.COLOR_DK_GREEN,"Value1");}
			if (dirty2)	{setFieldBkgColor(Gfx.COLOR_RED,"Value2");}
			else {setFieldBkgColor(Gfx.COLOR_DK_GREEN,"Value2");}
			activateButton("Save");
			setButtonText("Save", "Save");
		}
		else
		{
			deactivateButton("Save");
			setFieldBkgColor(Gfx.COLOR_DK_GREEN,"Value1");
			setFieldBkgColor(Gfx.COLOR_DK_GREEN,"Value2");
			setButtonText("Saved", "Save");
		}
		
		for (var i=0;i<butCount;i++)
		{
			var drw = self.findDrawableById(butNames[i]);
			if (i == selection)
			{
				drw.setColor(Gfx.COLOR_BLUE);
			}
			else
			{
				drw.setColor(Gfx.COLOR_DK_BLUE);
			}
		}
		setButtonText(getMeasButText(),"Measure");
	}
		
	function incrementSelectedItem()
	{
		selection++;
		if (selection>=butCount)
		{
			selection=0;
		}
		Ui.requestUpdate();
	}
	
	function decrementSelectedItem()
	{
		selection--;
		if (selection<0)
		{
			selection=butCount-1;
		}
		Ui.requestUpdate();
	}
    
    function start_meas()
    {
    	if (measuring_datum){return;}
		m_start_time = Sys.getTimer();
		m_timer = new Timer.Timer();
		m_timer.start(method(:finish_meas), vv_sensors.aerobody.DATUM_SET_TIME + vv_sensors.aerobody.AVG_TIME,false);
		measuring_datum = true;
    }
    
    hidden function getMeasButText()
    {
    	var text = "Measure";
    	if (measuring_datum)
    	{
			var dt = Sys.getTimer() - m_start_time;
			if (dt < vv_sensors.aerobody.DATUM_SET_TIME)
			{
				text = "Stby  " + ((vv_sensors.aerobody.DATUM_SET_TIME - dt)/1000.0f).format("%.1f") + "s";
			}
			else
			{
				text = "Avg " + ((vv_sensors.aerobody.DATUM_SET_TIME + vv_sensors.aerobody.AVG_TIME - dt)/1000.0f).format("%.1f") + "s";
			}
    	}
    	return text;
    }
    
    function finish_meas()
    {
    	measuring_datum = false;
    	chestRef = vv_sensors.aerobody.getChestPos();
    	headRef = vv_sensors.aerobody.getHeadPos();
    	selection = 1;
    }
	
	
}