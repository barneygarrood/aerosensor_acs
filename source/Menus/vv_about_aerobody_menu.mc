using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_about_aerobody_menu extends Ui.View
{
	var mHeader;
	var mVersion;
	var mSerialNumber;
	var mVoltage;
	var mSoc;
	
	function setHeader(header){mHeader = header;}
	function setVersion(version){mVersion = version;}
	function setSerialNumber(serialNumber){mSerialNumber = serialNumber;}
	function setVoltage(voltage){mVoltage = voltage;}
	function setSoc(soc){mSoc = soc;}
	
	function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    // Load your resources here
    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.AboutPage(dc));
	    self.findDrawableById("header").setMainText(mHeader);	
    }
    
    // Update the view 
    function onUpdate(dc) 
    {
		reDrawAll(dc);
        View.onUpdate(dc);
    }

    // Update the view 
    function reDrawAll(dc) 
    {  
    	if (vv_sensors.aerobody.searching)
    	{
    		mVersion = "Device not connected.";
    		mSerialNumber = "";
    		mVoltage = "";
    		mSoc = "";
    	}
    	else
    	{
	    	if(vv_sensors.aerobody.data.swVersionSet)
	    	{
	    		mVersion = "Firmaware version: " + vv_sensors.aerobody.data.majSwVersion + "." + vv_sensors.aerobody.data.minSwVersion;
    			mSerialNumber = "Serial number: " + vv_sensors.aerobody.data.serialNumber.format("%x");
	    	}
	    	else
	    	{
	    		mVersion = "Firmaware version: Requesting..";
    			mSerialNumber = "Serial number: Requesting..";
	    	}
	    	if (vv_sensors.aerobody.data.batLevel == 0)
	    	{
		    	mVoltage = "Battery voltage: Waiting..";
		    	mSoc = "Battery level: Waiting..";
	    	}
	    	else
	    	{
		    	mVoltage = "Battery voltage: " + vv_sensors.aerobody.data.batVoltage.format("%.3f") + "V";
		    	mSoc = "Battery level: " + ((6-vv_sensors.aerobody.data.batLevel)*20).format("%u") + "%";
		    }
	    }
    	
	    self.findDrawableById("version").setText(mVersion);	
	    self.findDrawableById("serialNumber").setText(mSerialNumber);
	    self.findDrawableById("voltage").setText(mVoltage);
	    self.findDrawableById("soc").setText(mSoc);	
    }
}