using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_top_menu_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden var m_button_count;
	
    function initialize(view) 
    {
        InputDelegate.initialize();
    	m_view = view;
    	//m_view.updateCallback = method(:reDrawCallback);
        
		view.title = "Main menu";
    	if (vv_settings.values[vv_settings.AEROSENSOR_ACTIVE])
    	{
    		m_button_count = 5;
    	}
    	else
    	{
    		m_button_count = 3;
    	}
        // Reset menu:
        resetMenu();
    }
    
    function resetMenu()
    {
    	// Clear menu:
    	m_view.resetMenu();
    	m_view.addItem("Aerosensor Settings","Aerosensor device settings");
    	m_view.addItem("Aerobody Settings","Aerobody device settings");
    	m_view.addItem("Connections","Device connections");
    	m_view.addItem("Settings","App settings");
    	
    }
    
    function actionButton(index)
    {
    	var view = new vv_manual_menu();
    	var delegate;
    	if (vv_settings.values[vv_settings.AEROSENSOR_ACTIVE])
    	{
	    	switch (index)
	    	{
				case 0:
					delegate = new vv_aerosensor_setting_menu_delegate(view); 
					break;
				case 1:
					delegate = new vv_aerobody_setting_menu_delegate(view);
					break;
				case 2:
					delegate = new vv_connection_menu_delegate(view);
					break;
				case 3:
					delegate = new as_app_settings_menu_delegate(view);
					break;
	    	}
	    }
	    else	// Aerobody only.
	    {
	    	switch (index)
	    	{
				case 0:
					delegate = new vv_aerobody_setting_menu_delegate(view);
					break;
				case 1:
					delegate = new vv_connection_menu_delegate(view);
					break;
	    	}
	    }
		WatchUi.pushView(view, delegate, Ui.SLIDE_LEFT);
    }
    
    function onHold(evt)
    {
    	var coords = evt.getCoordinates();
    }
	
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}

    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	// check arrow buttons:
		if (isPressed("button_up",coords))
		{
			m_view.decrementSelectedItem();
			Ui.requestUpdate();
		}
		else if (isPressed("button_down",coords))
		{
			m_view.incrementSelectedItem();
			Ui.requestUpdate();
		}
    	for (var i= 0;i<m_button_count;i++)
    	{
    		if (isPressed("button" + i,coords))
    		{
    			actionButton(i);
    			break;
    		}
    	}
    }
    
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    	case Ui.KEY_MENU:
	    		actionButton (m_view.selectedItem());
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
	
}