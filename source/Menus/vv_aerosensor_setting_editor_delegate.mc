using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using aerosensor_module as Aerosensor;

class vv_aerosensor_setting_editor_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden var butNames = {	0=>"AddMaj", 1=>"SubMaj", 2=>"AddMin", 3=>"SubMin", 4=>"Save", 5=>"Cancel"};
	hidden const BUTTON_COUNT = 6;
	
    function initialize(view)
    {
        InputDelegate.initialize();
    	m_view = view;
    	if (m_view.type == Aerosensor.AEROSENSOR_SETTING_TYPE_PARAMETER)
    	{
    		m_view.minor = vv_sensors.aerosensor.settings.minor[m_view.index];
    		m_view.major = vv_sensors.aerosensor.settings.major[m_view.index];
    		m_view.min = vv_sensors.aerosensor.settings.min[m_view.index];
    		m_view.max = vv_sensors.aerosensor.settings.max[m_view.index];
    	}
    	if (m_view.type == Aerosensor.AEROSENSOR_SETTING_TYPE_TRACK)
    	{
    		m_view.minor = vv_sensors.aerosensor.track.minor[m_view.index];
    		m_view.major = vv_sensors.aerosensor.track.major[m_view.index];
    		m_view.min = vv_sensors.aerosensor.track.min[m_view.index];
    		m_view.max = vv_sensors.aerosensor.track.max[m_view.index];
    	}
    }
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}
	
    function actionButton(index)
    {
    	switch (index)
    	{
    		case 0:
    			if ((m_view.value + m_view.major) <= m_view.max)
    			{
					m_view.value += m_view.major;
				}
				Ui.requestUpdate;
    			break;
    		case 1:
    			if ((m_view.value - m_view.major) >= m_view.min)
    			{
					m_view.value -= m_view.major;
				}
				Ui.requestUpdate;
    			break;
    		case 2:
    			if ((m_view.value + m_view.minor) <= m_view.max)
    			{
					m_view.value += m_view.minor;
				}
				Ui.requestUpdate;
    			break;
    		case 3:
    			if ((m_view.value - m_view.minor) >= m_view.min)
    			{
					m_view.value -= m_view.minor;
				}
				Ui.requestUpdate;
    			break;
    		case 4:
		    	if (m_view.type == Aerosensor.AEROSENSOR_SETTING_TYPE_PARAMETER)
		    	{
					vv_sensors.aerosensor.settings.vals[m_view.index] = m_view.value;
		    	}
		    	if (m_view.type == Aerosensor.AEROSENSOR_SETTING_TYPE_TRACK)
		    	{
					vv_sensors.aerosensor.track.vals[m_view.index] = m_view.value;
		    	}
				Ui.popView(SLIDE_LEFT);
				Ui.requestUpdate();
    			break;
    		case 5:
				Ui.popView(SLIDE_LEFT);
				Ui.requestUpdate();
    			break;
    	}
    }
    
    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	for (var i= 0;i<BUTTON_COUNT;i++)
    	{
    		if (isPressed(butNames[i],coords))
    		{
    			actionButton(i);
    			break;
    		}
    	}
    }
    
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    		var view;
	    		actionButton(m_view.selection);
	    		break;
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
}	
