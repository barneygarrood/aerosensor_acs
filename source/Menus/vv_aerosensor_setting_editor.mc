using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using aerosensor_module as Aerosensor;

class vv_aerosensor_setting_editor extends Ui.View
{
	var index;
	var type;
	var selection;
	var value;
	var major;
	var minor;
	var min;
	var max;
	
	const butCount = 6;
	const butNames = ["AddMaj","SubMaj","AddMin","SubMin", "Save", "Cancel"];
	function initialize(_index,_type)
	{
		index = _index;
		type = _type;
		selection = 0;
		if (type == Aerosensor.AEROSENSOR_SETTING_TYPE_PARAMETER)
		{
			value = vv_sensors.aerosensor.settings.vals[index];
		}
		else if (type == Aerosensor.AEROSENSOR_SETTING_TYPE_TRACK)
		{
			value = vv_sensors.aerosensor.track.vals[index];
		}
		
		
		Ui.View.initialize();
	}
	
	function drawText(dc,x,y,font,color,text)
	{
    	dc.setColor(color,Gfx.COLOR_TRANSPARENT);
    	dc.drawText(x,y,font,text,Gfx.TEXT_JUSTIFY_LEFT);
	}
	
	function onLayout(dc)
	{
        setLayout(Rez.Layouts.SettingEditLayout(dc));
        // Rejig heights - easier to do in code:
        var row_height = (dc.getHeight()-vv_icons.height)/5;
        var drw;
        drw = self.findDrawableById("Header");
        drw.locY = vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("AddMaj");
        drw.locY = row_height+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("AddMin");
        drw.locY = row_height+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("SubMaj");
        drw.locY = row_height*3+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("Value");
        drw.locY = row_height*2+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("SubMin");
        drw.locY = row_height*3+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("Save");
        drw.locY = row_height*4+vv_icons.height;
        drw.height = row_height;
        
        drw = self.findDrawableById("Cancel");
        drw.locY = row_height*4+vv_icons.height;
        drw.height = row_height;
        
		// Set title:
		var text=""; 
		if (type == Aerosensor.AEROSENSOR_SETTING_TYPE_PARAMETER)
		{
			text = vv_sensors.aerosensor.settings.defs[index];
		}
		else if (type == Aerosensor.AEROSENSOR_SETTING_TYPE_TRACK)
		{
			text = vv_sensors.aerosensor.track.defs[index];
		}
		
		
		setButtonText(text,"Header");
		var maj=0;
		var min=0;
		if (type == Aerosensor.AEROSENSOR_SETTING_TYPE_PARAMETER)
		{
			maj = vv_sensors.aerosensor.settings.major[index].format(vv_sensors.aerosensor.settings.format[index]);
			min = vv_sensors.aerosensor.settings.minor[index].format(vv_sensors.aerosensor.settings.format[index]);
		}
		else if (type == Aerosensor.AEROSENSOR_SETTING_TYPE_TRACK)
		{
			maj = vv_sensors.aerosensor.track.major[index].format(vv_sensors.aerosensor.track.format[index]);
			min = vv_sensors.aerosensor.track.minor[index].format(vv_sensors.aerosensor.track.format[index]);
		}
		
		// Set button texts:
		text = "+ " + maj;
		setButtonText(text,"AddMaj");
		
		text = "- " + maj;
		setButtonText(text,"SubMaj");
		
		text = "+ " + min;
		setButtonText(text,"AddMin");
		
		text = "- " + min;
		setButtonText(text,"SubMin");
		
		text = "Save";
		setButtonText(text,"Save");
		
		text = "Cancel";
		setButtonText(text,"Cancel");
        
		reDrawAll(dc);
	}
	
	function onUpdate(dc)
	{
		reDrawAll(dc);
        View.onUpdate(dc);
	}
	
	function setButtonText(text, butName)
	{		
		var drw = self.findDrawableById(butName);
		if (drw!=null)
		{
			drw.setMainText(text);
			drw.setSubText("");
		}
	}
	
	function reDrawAll(dc)
	{
		// Set value:
		var text = "";
		if (type == Aerosensor.AEROSENSOR_SETTING_TYPE_PARAMETER)
		{
			text = value.format(vv_sensors.aerosensor.settings.format[index]) + 
						" " + vv_sensors.aerosensor.settings.units[index];
		}
		else if (type == Aerosensor.AEROSENSOR_SETTING_TYPE_TRACK)
		{
			text = value.format(vv_sensors.aerosensor.track.format[index]) + 
						" " + vv_sensors.aerosensor.track.units[index];
		}
		setButtonText(text,"Value");
		// Select correct button:
		for (var i=0;i<butCount;i++)
		{
			var drw = self.findDrawableById(butNames[i]);
			var limit = false;
			switch (i)
			{
				case 0:
					limit = ((value + major) > max);
					break;
				case 1:
					limit = ((value - major) < min);
					break;
				case 2:
					limit = ((value + minor) > max);
					break;
				case 3:
					limit = ((value - minor) < min);
					break;
			}
			if (i == selection)
			{
				if (limit) {drw.setColor(Gfx.COLOR_RED);} else {drw.setColor(Gfx.COLOR_BLUE);}
			}
			else
			{
				if (limit) {drw.setColor(Gfx.COLOR_DK_RED);} else {drw.setColor(Gfx.COLOR_DK_BLUE);}
			}
		}
		
	}
		
	function incrementSelectedItem()
	{
		selection++;
		if (selection>=6)
		{
			selection=0;
		}
		Ui.requestUpdate();
	}
	
	function decrementSelectedItem()
	{
		selection--;
		if (selection<0)
		{
			selection=6-1;
		}
		Ui.requestUpdate();
	}
	
	
}