using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using aerosensor_module as Aerosensor;

class vv_aerosensor_track_edit_menu_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden const BUTTON_COUNT = 5;
	
    function initialize(view) 
    {
        InputDelegate.initialize();
    	m_view = view;
    	m_view.onShowUpdateCallback = method(:resetMenu);
        
		view.title = "Track Edit Menu";
        // Reset menu:
        resetMenu();
    }
    
    function resetMenu()
    {
    	// Clear menu:
    	m_view.resetMenu();
    	updateMenu();
    }
    
    function updateMenu()
    {
		for (var i=0;i<vv_sensors.aerosensor.track.count;i++)
		{
			var str1 = vv_sensors.aerosensor.track.defs[i];
			var str2 = vv_sensors.aerosensor.track.vals[i].format(vv_sensors.aerosensor.track.format[i]) + 
						" " + vv_sensors.aerosensor.track.units[i];
			m_view.addItem(str1,str2);
			// Is dirty?
			if (vv_sensors.aerosensor.track.originalVals[i]!=vv_sensors.aerosensor.track.vals[i])
			{
				vv_sensors.aerosensor.track.status.isDirty = true;
				vv_sensors.aerosensor.track.isValDirty[i] = true;
			}
			else
			{
				vv_sensors.aerosensor.track.isValDirty[i] = false;
			}
		}
		m_view.specialItems = vv_sensors.aerosensor.track.isValDirty;
    }
	    
	function actionButton(index)
    {
    	if (index!=null)
    	{
    		m_view.setSelectedItem(index);
    	}
		var view;
		var selectedItem = m_view.selectedItem();
		view = new vv_aerosensor_setting_editor(selectedItem, Aerosensor.AEROSENSOR_SETTING_TYPE_TRACK);
		WatchUi.pushView(view, new vv_aerosensor_setting_editor_delegate(view), Ui.SLIDE_LEFT);
    }
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}

    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	// check arrow buttons:
		if (isPressed("button_up",coords))
		{
			m_view.decrementSelectedItem();
			Ui.requestUpdate();
		}
		else if (isPressed("button_down",coords))
		{
			m_view.incrementSelectedItem();
			Ui.requestUpdate();
		}
    	for (var i= 0;i<BUTTON_COUNT;i++)
    	{
    		if (isPressed("button" + i,coords))
    		{
    			actionButton(i);
    			break;
    		}
    	}
    }
    
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    		actionButton(null);
	    		break;
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
}	