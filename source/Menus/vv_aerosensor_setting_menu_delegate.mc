using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using aerosensor_module as Aerosensor;

class vv_aerosensor_setting_menu_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden const BUTTON_COUNT = 5;
	
    function initialize(view) 
    {
        InputDelegate.initialize();
    	m_view = view;
    	m_view.preUpdateCallback = method(:updateMenu);
        
		view.title = "Aerosensor Settings";
        // Reset menu:
        resetMenu();
    }
    
    function resetMenu()
    {
    	// Clear menu:
    	m_view.resetMenu();
    	m_view.addItem("Parameters", "Parameters menu");
    	m_view.addItem("Track", "Track layout menu");
    	m_view.addItem("Calculation mode", "Change calculation mode.");
    	m_view.addItem("Demo mode", "Change demonstration mode.");
    	m_view.addItem("About", "Aerosensor device information.");
    }
    
    function updateMenu()
    {
    }
	    
	function actionButton(index)
    {
    	var view;
    	if (index!=null)
    	{
    		m_view.setSelectedItem(index);
    	}
    	switch (index)
    	{
			case 0:
		    	view = new vv_manual_menu();
				WatchUi.pushView(view, new vv_aerosensor_parameters_menu_delegate(view), Ui.SLIDE_LEFT);
				break;
			case 1:
		    	view = new vv_manual_menu();
				WatchUi.pushView(view, new vv_aerosensor_track_menu_delegate(view), Ui.SLIDE_LEFT);
				break;
			case 2:
		    	view = new vv_manual_menu();
				WatchUi.pushView(view, new vv_aerosensor_calc_mode_menu_delegate(view), Ui.SLIDE_LEFT);
				break;
			case 3:
		    	view = new vv_manual_menu();
				WatchUi.pushView(view, new vv_aerosensor_demo_mode_menu_delegate(view), Ui.SLIDE_LEFT);
				break;
			case 4:
	    		view = new vv_about_aerosensor_menu();
				WatchUi.pushView(view, new vv_about_aerosensor_menu_delegate(view), Ui.SLIDE_LEFT);
				break;
    	}
    }
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}

    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	// check arrow buttons:
		if (isPressed("button_up",coords))
		{
			m_view.decrementSelectedItem();
			Ui.requestUpdate();
		}
		else if (isPressed("button_down",coords))
		{
			m_view.incrementSelectedItem();
			Ui.requestUpdate();
		}
    	for (var i= 0;i<BUTTON_COUNT;i++)
    	{
    		if (isPressed("button" + i,coords))
    		{
    			actionButton(i);
    			break;
    		}
    	}
    }
    
    
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    	case Ui.KEY_MENU:
	    		var view;
	    		switch (m_view.selectedItem())
	    		{
	    			case 0:
    					actionButton(0);
	    				break;
	    			case 1:
    					actionButton(1);
	    				break;
	    			case 2:
    					actionButton(2);
	    				break;
	    			case 2:
    					actionButton(3);
	    				break;
	    		}
	    		break;
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
}	