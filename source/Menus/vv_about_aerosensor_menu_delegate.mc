using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Timer;
using Toybox.Graphics as Gfx;

class vv_about_aerosensor_menu_delegate extends Ui.InputDelegate {
	
	var m_view;
	
    function initialize(view) {
    	m_view = view;
    	m_view.setHeader("Aerosensor Info");
        InputDelegate.initialize();
    }
	
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		break;
	    	case Ui.KEY_UP:
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    		break;
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
}