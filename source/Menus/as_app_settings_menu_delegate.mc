using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using aerosensor_module as Aerosensor;

class as_app_settings_menu_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden const BUTTON_COUNT = 5;
	
    function initialize(view) 
    {
        InputDelegate.initialize();
    	m_view = view;
    	//m_view.preUpdateCallback = method(:updateMenu);
        
		view.title = "App settings";
        // Reset menu:
        resetMenu();
    }
    
    function resetMenu()
    {
    	// Clear menu:
    	m_view.resetMenu();
    	if (vv_settings.values[vv_settings.BLACK_BACKGROUND])
    	{
    		m_view.addItem("Toggle color mode", "Black background");
    	}
    	else
    	{
    		m_view.addItem("Toggle color mode", "White background");
    	}
    	
    }
	    
	function actionButton(index)
    {
    	var view;
    	if (index!=null)
    	{
    		m_view.setSelectedItem(index);
    	}
    	vv_settings.toggleColorMode();
    	resetMenu();
    }
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}

    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	// check arrow buttons:
		if (isPressed("button_up",coords))
		{
			m_view.decrementSelectedItem();
			Ui.requestUpdate();
		}
		else if (isPressed("button_down",coords))
		{
			m_view.incrementSelectedItem();
			Ui.requestUpdate();
		}
    	for (var i= 0;i<BUTTON_COUNT;i++)
    	{
    		if (isPressed("button" + i,coords))
    		{
    			actionButton(i);
    			break;
    		}
    	}
    }
    
    
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    	case Ui.KEY_MENU:
	    		var view;
	    		actionButton(m_view.selectedItem());
	    		break;
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
}	