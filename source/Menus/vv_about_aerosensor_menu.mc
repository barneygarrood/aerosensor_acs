using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_about_aerosensor_menu extends Ui.View
{
	var mHeader;
	var mVersion;
	var mSerialNumber;
	var mVoltage;
	var mSoc;
	var mCalcMode;
	var mDemoMode;
	
	function setHeader(header){mHeader = header;}
//	function setVersion(version){mVersion = version;}
//	function setSerialNumber(serialNumber){mSerialNumber = serialNumber;}
//	function setVoltage(voltage){mVoltage = voltage;}
//	function setSoc(soc){mSoc = soc;}
//	function setCalcMode(calcMode){mCalcMode = calcMode;}
//	function setDemoMode(demoMode){mDemoMode = demoMode;}
	
	function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    // Load your resources here
    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.AboutPage(dc));
	    self.findDrawableById("header").setMainText(mHeader);	
    }
    
    // Update the view 
    function onUpdate(dc) 
    {
		reDrawAll(dc);
        View.onUpdate(dc);
    }

    // Update the view 
    function reDrawAll(dc) 
    {  
    	if (vv_sensors.aerosensor.searching)
    	{
    		mVersion = "Device not connected.";
    		mSerialNumber = "";
    		mVoltage = "";
    		mSoc = "";
    		mCalcMode = "";
    		mDemoMode = "";
    	}
    	else
    	{
	    	if(vv_sensors.aerosensor.data.swVersionSet)
	    	{
	    		mVersion = "Firmaware version: " + vv_sensors.aerosensor.data.majSwVersion + "." + vv_sensors.aerosensor.data.minSwVersion;
    			mSerialNumber = "Serial number: " + vv_sensors.aerosensor.data.serialNumber.format("%x");
	    	}
	    	else
	    	{
	    		mVersion = "Firmaware version: Requesting..";
    			mSerialNumber = "Serial number: Requesting..";
	    	}
	    	if (vv_sensors.aerosensor.data.batLevel == 0)
	    	{
		    	mVoltage = "Battery voltage: Waiting..";
		    	mSoc = "Battery level: Waiting..";
	    	}
	    	else
	    	{
		    	mVoltage = "Battery voltage: " + vv_sensors.aerosensor.data.batVoltage.format("%.3f") + "V";
		    	mSoc = "Battery level: " + ((6-vv_sensors.aerosensor.data.batLevel)*20).format("%u") + "%";
		    }
		    mCalcMode = "Calc Mode: " + vv_sensors.aerosensor.data.calcMode.format("%.0f");
		    mDemoMode = "Demo Mode: " + vv_sensors.aerosensor.data.demoMode.format("%.0f");
		    
	    }
    	
	    self.findDrawableById("version").setText(mVersion);	
	    self.findDrawableById("version").setColor(as_colors.foreground);	
	    self.findDrawableById("serialNumber").setText(mSerialNumber);
	    self.findDrawableById("serialNumber").setColor(as_colors.foreground);
	    self.findDrawableById("voltage").setText(mVoltage);
	    self.findDrawableById("voltage").setColor(as_colors.foreground);
	    self.findDrawableById("soc").setText(mSoc);	
	    self.findDrawableById("soc").setColor(as_colors.foreground);
	    self.findDrawableById("calcMode").setText(mCalcMode);	
	    self.findDrawableById("calcMode").setColor(as_colors.foreground);
	    self.findDrawableById("demoMode").setText(mDemoMode);	
	    self.findDrawableById("demoMode").setColor(as_colors.foreground);
    }
}