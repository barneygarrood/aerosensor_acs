using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;

class vv_aerobody_setting_editor_delegate extends Ui.InputDelegate
{
	hidden var m_view;
	hidden var butNames = {	0=>"AddMaj", 1=>"SubMaj", 2=>"AddMin", 3=>"SubMin", 4=>"Save", 5=>"Cancel"};
	hidden const BUTTON_COUNT = 6;
	
    function initialize(view)
    {
        InputDelegate.initialize();
    	m_view = view;
    }
    
	function isPressed(buttonName, coords)
	{
		var ret = false;
    	var button = m_view.findDrawableById(buttonName);
		if ((button!=null))
		{
			if (button.isEnabled())
			{
				if ((coords[0]>=button.locX) & (coords[0] <= (button.locX+button.width)) & (coords[1] >= button.locY) & (coords[1] <= (button.locY+button.height)))
				{
					ret = true;
				}
			}
		}
		return ret;
	}
	
    function actionButton(index)
    {
    	switch (index)
    	{
    		case 0:
				m_view.value += 10;
				Ui.requestUpdate;
    			break;
    		case 1:
				m_view.value -= 10;
				Ui.requestUpdate;
    			break;
    		case 2:
				m_view.value += 1;
				Ui.requestUpdate;
    			break;
    		case 3:
				m_view.value -= 1;
				Ui.requestUpdate;
    			break;
    		case 4:
				vv_settings.values[vv_settings.NULLZONE] = m_view.value;
				vv_settings.save();
				Ui.popView(SLIDE_LEFT);
				Ui.requestUpdate();
    			break;
    		case 5:
				Ui.popView(SLIDE_LEFT);
				Ui.requestUpdate();
    			break;
    	}
    }
    
    function onTap(evt)
   	{
    	var coords = evt.getCoordinates();
    	for (var i= 0;i<BUTTON_COUNT;i++)
    	{
    		if (isPressed(butNames[i],coords))
    		{
    			actionButton(i);
    			break;
    		}
    	}
    }
    
    function onKey(evt) 
    {
	    var key = evt.getKey();
	    switch (key)
	    {
	    	case Ui.KEY_DOWN:
	    		m_view.incrementSelectedItem();
	    		break;
	    	case Ui.KEY_UP:
	    		m_view.decrementSelectedItem();
	    		break;
	    	case Ui.KEY_START:
	    		break;
	    	case Ui.KEY_LAP:
	    		break;
	    	case Ui.KEY_ENTER:
	    		var view;
	    		actionButton(m_view.selection);
	    		break;
	    	case Ui.KEY_ESC:
	    		break;
		}
	}
}	
