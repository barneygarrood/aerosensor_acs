using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Timer;

class vv_page4 extends Ui.View 
{
	const YSTART = 10;
	
    function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    // Load your resources here
    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.DataGridx5(dc));
        var drw;
        
        drw = self.findDrawableById("data1");
        drw.setUnitText("");
        
        drw = self.findDrawableById("data2");
        drw.setHeadingText("Road Speed");
        drw.setUnitText("kph");
        //drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data3");
        drw.setHeadingText("CdA Change");
        drw.setUnitText("");
        
        drw = self.findDrawableById("data4");
        drw.setHeadingText("Head");
        drw.setUnitText("cm");
        //drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data5");
        drw.setHeadingText("Chest");
        drw.setUnitText("cm");
        //drw.setValueColor(as_colors.foreground);
    }
    
    // Update the view 
    function onUpdate(dc) 
    {
		reDrawAll(dc);
        View.onUpdate(dc);
    }

    // Update the view 
    function reDrawAll(dc) 
    {    	    	
    	var status = vv_sensors.getLapStatus();
        var drw;
    	// Lap CdA---------------------------------------------------------------------------------
    	drw=self.findDrawableById("data1");
		var lapNo = vv_sensors.aerosensor.offsetLapNo();
    	drw.setHeadingText("CdA lap " + lapNo.format("%.0f")) ;
    	drw.setMsgColor(status.color);
    	drw.setMsgText(status.text);
    	drw.setValueColor(status.color);
    	drw.setValueText(vv_sensors.aerosensor.data.cdaLap.format("%.4f"));
    	    	
    	// Road speed ---------------------------------------------------------------------------------
    	drw=self.findDrawableById("data2");
    	drw.setValueColor(as_colors.foreground);  
		var roadSpeed = antplus_speed.getSpeed(vv_sensors.bsc,vv_sensors.bspd);
		var text = "";
    	if (roadSpeed != null)
    	{
    		text = roadSpeed.format("%.1f");
    	}
    	else
    	{
    		text = "---";
    	}
    	drw.setValueText(text);
    	
    	// Delta to previous---------------------------------------------------------------------------------
    	drw=self.findDrawableById("data3");
    	var value = vv_sensors.aerosensor.data.cdaLap -  vv_sensors.aerosensor.prev_lap_data.vals[1];
    	var color;
    	if (value <= -0.0005) 
    	{
    		color = Gfx.COLOR_DK_GREEN;
    	}
    	else if (value >= 0.0005)
    	{
    		color = Gfx.COLOR_RED;
    	}
    	else 
    	{
    		color = as_colors.foreground;
    	}
    	drw.setValueColor(color);
    	drw.setValueText(value.format("%.4f"));
    	
    	
    	// Head position---------------------------------------------------------------------------------
    	drw=self.findDrawableById("data4");
    	drw.setValueColor(as_colors.foreground);
    	value = vv_sensors.aerobody.getHeadDeltaPos();
    	if (value == null)
    	{
    		drw.setValueText("---");
    	}
    	else
    	{
    		// Convert to cm.
    		value = value / 10.0f;
    		drw.setValueText(value.format("%.1f"));
    	}
    	drw.setBackgroundColor(vv_sensors.aerobody.getColour(vv_sensors.aerobody.HEAD_SENSOR));
    	
    	// Chest position---------------------------------------------------------------------------------
    	drw=self.findDrawableById("data5");
    	drw.setValueColor(as_colors.foreground);
    	value = vv_sensors.aerobody.getChestDeltaPos();
    	if (value == null)
    	{
    		drw.setValueText("---");
    	}
    	else
    	{
    		// Convert to cm.
    		value = value / 10.0f;
    		drw.setValueText(value.format("%.1f"));
    	}
    	drw.setBackgroundColor(vv_sensors.aerobody.getColour(vv_sensors.aerobody.CHEST_SENSOR));
    }
}
