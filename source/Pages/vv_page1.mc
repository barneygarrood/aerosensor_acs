using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Timer;
using Toybox.Activity;
//using Toybox.AntPlus;
using Toybox.Activity as Activity;

class vv_page1 extends Ui.View 
{
	hidden var mFontNeuropol;
	
    function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    // Load your resources here
    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.DataGridx5(dc));
        // Set headings etc:
        var drw;
        drw = self.findDrawableById("data1");
        drw.setHeadingText("CdA");
        drw.setUnitText("");
        
        drw = self.findDrawableById("data2");
        drw.setHeadingText("Road Speed");
        drw.setUnitText("kph");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data3");
        drw.setHeadingText("Headwind");
        drw.setUnitText("kph");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data4");
        drw.setHeadingText("Wind yaw");
        drw.setUnitText("deg.");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data5");
        drw.setHeadingText("Elevation");
        drw.setUnitText("m");
        drw.setValueColor(as_colors.foreground);
        
    }

    // Update the view 
    function onUpdate(dc) 
    {
		reDrawAll(dc);
        View.onUpdate(dc);
    }
    
    function reDrawAll(dc)
    {        
    	var text;
    	var color;
    	var drw;
		   	
    	var status = vv_sensors.getStatus();
    	    	
    	// Cda-----------------------------------------------------------------------------------------
    	drw = self.findDrawableById("data1");
    	drw.setMsgColor(status.color);
    	drw.setMsgText(status.text);
    	
    	text = vv_sensors.aerosensor.data.cda.format("%.3f");
    	drw.setValueColor(status.color);
    	drw.setValueText(text);
    	
    	// Road speed---------------------------------------------------------------------------------
    	color = as_colors.foreground;
    	var roadSpeed = antplus_speed.getSpeed(vv_sensors.bsc,vv_sensors.bspd);
    	if (roadSpeed != null)
    	{
    		text = roadSpeed.format("%.1f");
    	}
    	else
    	{
    		text = "---";
    	}
    	drw = self.findDrawableById("data2");
    	drw.setValueColor(color);
    	drw.setValueText(text);
    	
    	// Road speed---------------------------------------------------------------------------------
    	color = as_colors.foreground;  
    	var wind = vv_sensors.aerosensor.data.windSpeed; 
    	if ((wind != null) & (roadSpeed != null))
    	{
    		wind *= 3.6;  	
    		wind -= roadSpeed;
    		text = wind.format("%.1f");
    	}
    	else
    	{
    		text = "---";
    	}
    	drw = self.findDrawableById("data3");
    	drw.setValueColor(color);
    	drw.setValueText(text);
    	
    	// Wind yaw---------------------------------------------------------------------------------
    	color = as_colors.foreground;    	
    	var windYaw = vv_sensors.aerosensor.data.yaw;
    	if (windYaw != null)
    	{
    		text = windYaw.format("%.1f");
    	}
    	else
    	{
    		text = "---";
    	}
    	drw = self.findDrawableById("data4");
    	drw.setValueColor(color);
    	drw.setValueText(text);
    	
    	// Elevation---------------------------------------------------------------------------------
    	color = as_colors.foreground;  
    	var elevation = vv_sensors.aerosensor.data.elevation;
    	if (elevation!=null)
    	{  	
    		text = elevation.format("%.2f");
    	}
    	else
    	{
    		text = "---";
    	}
    	drw = self.findDrawableById("data5");
    	drw.setValueColor(color);
    	drw.setValueText(text);
    
    }
}
