using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Timer;
using Toybox.Activity as Activity;

class vv_page0 extends Ui.View 
{	
	
    function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    // Load your resources here
    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.DataGridx5(dc));
        var drw;
        
        drw = self.findDrawableById("data1");
        drw.setHeadingText("CdA");
        drw.setUnitText("");
        
        drw = self.findDrawableById("data2");
        drw.setHeadingText("Head");
        drw.setUnitText("cm");
        
        drw = self.findDrawableById("data3");
        drw.setHeadingText("Chest");
        drw.setUnitText("cm");
        
        drw = self.findDrawableById("data4");
        drw.setHeadingText("Road Speed");
        drw.setUnitText("kph");
        
        drw = self.findDrawableById("data5");
        drw.setHeadingText("Power (10s)");
        drw.setUnitText("W");
    }
    
    // Update the view 
    function onUpdate(dc) 
    {
		reDrawAll(dc);
        View.onUpdate(dc);
    }

    // Update the view 
    function reDrawAll(dc) 
    {
        var drw;
        
    	var status = vv_sensors.getStatus();
        // Cda-----------------------------------------------------------------------------------------
    	drw = self.findDrawableById("data1");
    	drw.setMsgColor(status.color);
    	drw.setMsgText(status.text);
    	
    	var text = vv_sensors.aerosensor.data.cda.format("%.3f");
    	drw.setValueColor(status.color);
    	drw.setValueText(text);
    	
    	// Head position---------------------------------------------------------------------------------
    	drw=self.findDrawableById("data2");
        drw.setValueColor(as_colors.foreground);
    	var value = vv_sensors.aerobody.getHeadDeltaPos();
    	if (value == null)
    	{
    		drw.setValueText("---");
    	}
    	else
    	{
    		// Convert to cm.
    		value = value / 10.0f;
    		drw.setValueText(value.format("%.1f"));
    	}
    	drw.setBackgroundColor(vv_sensors.aerobody.getColour(vv_sensors.aerobody.HEAD_SENSOR));
    	
    	// Chest position---------------------------------------------------------------------------------
    	drw=self.findDrawableById("data3");
        drw.setValueColor(as_colors.foreground);
    	value = vv_sensors.aerobody.getChestDeltaPos();
    	if (value == null)
    	{
    		drw.setValueText("---");
    	}
    	else
    	{
    		// Convert to cm.
    		value = value / 10.0f;
    		drw.setValueText(value.format("%.1f"));
    	}
    	drw.setBackgroundColor(vv_sensors.aerobody.getColour(vv_sensors.aerobody.CHEST_SENSOR));

    	// Road speed---------------------------------------------------------------------------------
    	var roadSpeed = antplus_speed.getSpeed(vv_sensors.bsc,vv_sensors.bspd);
    	if (roadSpeed != null)
    	{
    		text = roadSpeed.format("%.1f");
    	}
    	else
    	{
    		text = "---";
    	}
    	drw = self.findDrawableById("data4");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);
    	
    	// Power
    	drw = self.findDrawableById("data5");
        drw.setValueColor(as_colors.foreground);
    	drw.setValueText(vv_sensors.bpwr.getAvgPower().format("%.1f"));
    }
}
