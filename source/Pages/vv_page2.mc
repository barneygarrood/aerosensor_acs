using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Timer;
//using Toybox.AntPlus;
using Toybox.Activity as Activity;

class vv_page2 extends Ui.View 
{
	hidden var mFontNeuropol;
	
    function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    // Load your resources here
    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.DataGridx5(dc));
        // Set headings etc:
        var drw;
        drw = self.findDrawableById("data1");
        drw.setHeadingText("Wind pwr");
        drw.setUnitText("W");
        
        drw = self.findDrawableById("data2");
        drw.setHeadingText("User power");
        drw.setUnitText("W");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data3");
        drw.setHeadingText("Kinetic pwr");
        drw.setUnitText("W");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data4");
        drw.setHeadingText("Friction pwr");
        drw.setUnitText("W");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data5");
        drw.setHeadingText("Grade pwr");
        drw.setUnitText("W");
        drw.setValueColor(as_colors.foreground);
        
    }

    // Update the view 
    function onUpdate(dc) 
    {
		reDrawAll(dc);
        View.onUpdate(dc);
    }
    
    function reDrawAll(dc)
    {        
    	var text;
    	var font;
    	var color;
    	var drw; 	
    	// Populate values:
    	// Wind power---------------------------------------------------------------------------------
    	var status = vv_sensors.getStatusShortAv();
    	drw = self.findDrawableById("data1");
    	drw.setMsgColor(status.color);
    	drw.setMsgText(status.text);
    	text = (vv_sensors.aerosensor.data.windPwrPct * vv_sensors.aerosensor.data.pwrDenom / 100.0f).format("%.1f");
    	drw.setValueColor(status.color);
    	drw.setValueText(text);
    	
    	// Rider power---------------------------------------------------------------------------------
    	color = as_colors.foreground;    	
    	text = (vv_sensors.aerosensor.data.userPwrPct * vv_sensors.aerosensor.data.pwrDenom / 100.0f).format("%.1f");
    	drw = self.findDrawableById("data2");
    	drw.setValueColor(color);
    	drw.setValueText(text);
    	
    	
    	// Kinetic power---------------------------------------------------------------------------------
    	color = as_colors.foreground;    	
    	text = (vv_sensors.aerosensor.data.kineticPwrPct * vv_sensors.aerosensor.data.pwrDenom / 100.0f).format("%.1f");
    	drw = self.findDrawableById("data3");
    	drw.setValueColor(color);
    	drw.setValueText(text);
    	
    	// Friction power---------------------------------------------------------------------------------
    	color = as_colors.foreground;    	
    	text = (vv_sensors.aerosensor.data.fricPwrPct * vv_sensors.aerosensor.data.pwrDenom / 100.0f).format("%.1f");
    	drw = self.findDrawableById("data4");
    	drw.setValueColor(color);
    	drw.setValueText(text);
    	
    	// Grade power---------------------------------------------------------------------------------
    	color = as_colors.foreground;    	
    	text = (vv_sensors.aerosensor.data.gradePwrPct * vv_sensors.aerosensor.data.pwrDenom / 100.0f).format("%.1f");
    	drw = self.findDrawableById("data5");
    	drw.setValueColor(color);
    	drw.setValueText(text);
    
    }
}
