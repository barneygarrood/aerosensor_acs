using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Timer;
using Toybox.Activity as Activity;

class vv_page6 extends Ui.View 
{	
	
    function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    // Load your resources here
    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.DataGridx6(dc));
        var drw;
        
        drw = self.findDrawableById("data1");
        var lap = vv_sensors.aerosensor.lap_data.offsetLapNo();
        drw.setHeadingText("Lap " + lap.format("%.0f")+ " CdA");
        drw.setUnitText("");
        
        drw = self.findDrawableById("data2");
        drw.setHeadingText("Prev CdA");
        drw.setUnitText("");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data3");
        drw.setHeadingText("Cal factor");
        drw.setUnitText("");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data4");
        drw.setHeadingText("Prev cal factor");
        drw.setUnitText("");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data5");
        drw.setHeadingText("Distance");
        drw.setUnitText("revs");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data6");
        drw.setHeadingText("Prev distance");
        drw.setUnitText("revs");
        drw.setValueColor(as_colors.foreground);
    }
    
    // Update the view 
    function onUpdate(dc) 
    {
		reDrawAll(dc);
        View.onUpdate(dc);
    }

    // Update the view 
    function reDrawAll(dc) 
    {
        var drw;
        
    	var status = vv_sensors.getStatus();
    	
        // Cda-----------------------------------------------------------------------------------------
    	drw = self.findDrawableById("data1");
        var lap = vv_sensors.aerosensor.lap_data.offsetLapNo();
        drw.setHeadingText("Lap " + lap.format("%.0f") + " CdA");
    	var text = vv_sensors.aerosensor.lap_data.vals[1].format("%.3f");
    	drw.setValueColor(status.color);
    	drw.setValueText(text);
        
        // Prev lap Cda-----------------------------------------------------------------------------------------
    	drw = self.findDrawableById("data2");
    	text = vv_sensors.aerosensor.prev_lap_data.vals[1].format("%.3f");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);
    	
        // Dynamic correction factor-----------------------------------------------------------------------------------------
    	drw = self.findDrawableById("data3");
    	text = vv_sensors.aerosensor.lap_data.vals[2].format("%.3f");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);
    	
        // Dynamic correction factor-----------------------------------------------------------------------------------------
    	drw = self.findDrawableById("data4");
    	text = vv_sensors.aerosensor.prev_lap_data.vals[2].format("%.3f");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);
        
        // Lap Distance-----------------------------------------------------------------------------------------
    	drw = self.findDrawableById("data5");
    	text = vv_sensors.aerosensor.lap_data.vals[3].format("%.1f");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);
    	
        // Prev Lap Distance-----------------------------------------------------------------------------------------
    	drw = self.findDrawableById("data6");
    	text = vv_sensors.aerosensor.prev_lap_data.vals[3].format("%.1f");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);
    	

    }
}
