using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Timer;
using Toybox.Activity as Activity;

class vv_page5 extends Ui.View 
{	
	
    function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    // Load your resources here
    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.DataGridx6(dc));
        var drw;
        
        drw = self.findDrawableById("data1");
        drw.setHeadingText("Lap");
        drw.setUnitText("");
        
        drw = self.findDrawableById("data2");
        drw.setHeadingText("Road Speed");
        drw.setUnitText("kph");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data3");
        drw.setHeadingText("CdA");
        drw.setUnitText("");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data4");
        drw.setHeadingText("Delta CdA");
        drw.setUnitText("");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data5");
        drw.setHeadingText("Laptime");
        drw.setUnitText("s");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data6");
        drw.setHeadingText("Delta laptime");
        drw.setUnitText("s");
        drw.setValueColor(as_colors.foreground);
    }
    
    // Update the view 
    function onUpdate(dc) 
    {
		reDrawAll(dc);
        View.onUpdate(dc);
    }

    // Update the view 
    function reDrawAll(dc) 
    {
        var drw;
        
    	var status = vv_sensors.getStatus();
    	
    	// Lap number:
    	drw = self.findDrawableById("data1");
		var lapNo = vv_sensors.aerosensor.lap_data.offsetLapNo();
    	var text = lapNo.format("%.0f");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);
    	
    	// Road speed---------------------------------------------------------------------------------
    	var roadSpeed = antplus_speed.getSpeed(vv_sensors.bsc,vv_sensors.bspd);
    	if (roadSpeed != null)
    	{
    		text = roadSpeed.format("%.1f");
    	}
    	else
    	{
    		text = "---";
    	}
    	drw = self.findDrawableById("data2");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);
    	
    	
    	
        // Cda-----------------------------------------------------------------------------------------
        var curCdA = vv_sensors.aerosensor.lap_data.vals[1];
        var prevCdA = vv_sensors.aerosensor.prev_lap_data.vals[1];
        var deltaCdA = curCdA - prevCdA;
        
    	drw = self.findDrawableById("data3");
    	text = curCdA.format("%.3f");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);
        
        // Prev lap Cda-----------------------------------------------------------------------------------------
    	drw = self.findDrawableById("data4");
    	text = deltaCdA.format("%.3f");
    	if (deltaCdA>0.002)
    	{
    		drw.setValueColor(Gfx.COLOR_RED);
    	}
    	else if (deltaCdA<0.002)
    	{
    		drw.setValueColor(Gfx.COLOR_DK_GREEN);
    	}
    	else
    	{
    		drw.setValueColor(as_colors.foreground);
    	}
    	drw.setValueText(text);
        
        // Lap time-----------------------------------------------------------------------------------------
    	var current_laptime = vv_sensors.aerosensor.lap_data.vals[vv_sensors.aerosensor.lap_data.AEROSENSOR_LAP_DATA_LAPTIME];
    	var previous_laptime = vv_sensors.aerosensor.prev_lap_data.vals[vv_sensors.aerosensor.lap_data.AEROSENSOR_LAP_DATA_LAPTIME];
    	drw = self.findDrawableById("data5");
    	text = vv_sensors.aerosensor.lap_data.vals[vv_sensors.aerosensor.lap_data.AEROSENSOR_LAP_DATA_LAPTIME].format("%.3f");    	
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);
    	
        // Delta Lap time -----------------------------------------------------------------------------------------
    	drw = self.findDrawableById("data6");
    	var deltaLaptime = current_laptime - previous_laptime;
    	text = deltaLaptime.format("%.3f");
    	if (deltaLaptime>0.005)
    	{
    		drw.setValueColor(Gfx.COLOR_RED);
    	}
    	else if (deltaLaptime<0.005)
    	{
    		drw.setValueColor(Gfx.COLOR_DK_GREEN);
    	}
    	else
    	{
    		drw.setValueColor(as_colors.foreground);
    	}
    	drw.setValueText(text);
    	

    }
}
