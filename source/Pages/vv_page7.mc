using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Timer;
using Toybox.Activity as Activity;

class vv_page7 extends Ui.View 
{	
	
    function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    // Load your resources here
    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.DataGridx6(dc));
        var drw;
        
        drw = self.findDrawableById("data1");
        drw.setHeadingText("Lap Time");
        drw.setUnitText("s");
        
        drw = self.findDrawableById("data2");
        drw.setHeadingText("Lap");
        drw.setUnitText("");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data3");
        drw.setHeadingText("Av Speed");
        drw.setUnitText("kph");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data4");
        drw.setHeadingText("Power (10s)");
        drw.setUnitText("W");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data5");
        drw.setHeadingText("Head");
        drw.setUnitText("cm");
        drw.setValueColor(as_colors.foreground);
        
        drw = self.findDrawableById("data6");
        drw.setHeadingText("Chest");
        drw.setUnitText("cm");
        drw.setValueColor(as_colors.foreground);

    }
    
    // Update the view 
    function onUpdate(dc) 
    {
		reDrawAll(dc);
        View.onUpdate(dc);
    }

    // Update the view 
    function reDrawAll(dc) 
    {
        var drw;
    	var text;
        var value;
    	var status = vv_sensors.getStatus();

		// Lap time
    	drw = self.findDrawableById("data1");
    	text = vv_sensors.aerosensor.lap_data.vals[vv_sensors.aerosensor.lap_data.AEROSENSOR_LAP_DATA_LAPTIME].format("%.2f");    	
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);

		// Lap number    	
		drw = self.findDrawableById("data2");
        var lap = vv_sensors.aerosensor.lap_data.offsetLapNo();
    	text = lap.format("%.0f");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);

		// Average speed
		drw = self.findDrawableById("data3");
        var dist = vv_sensors.aerosensor.lap_data.vals[vv_sensors.aerosensor.lap_data.AEROSENSOR_LAP_DATA_DISTANCE];
        var time = vv_sensors.aerosensor.lap_data.vals[vv_sensors.aerosensor.lap_data.AEROSENSOR_LAP_DATA_LAPTIME];
        var circ = vv_sensors.aerosensor.settings.vals[vv_sensors.aerosensor.settings.SETTING_WHEEL_CIRC];
        if (dist == null)
        {
            value = 0;
        }
        else if (time == null)
        {
            value = 0;
        }
        else if (time == 0)
        {
            value = 0;
        }
        else if (circ == null)
        {
            value = 0;
        }
        else
        {
            value = dist * circ / 1e6 / time * 3600;
        }
    	text = value.format("%.1f");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);

		// 10s power
		drw = self.findDrawableById("data4");
    	text = vv_sensors.bpwr.getAvgPower().format("%.0f");
    	drw.setValueColor(as_colors.foreground);
    	drw.setValueText(text);

		// head
		drw = self.findDrawableById("data5");
        drw.setValueColor(as_colors.foreground);
    	value = vv_sensors.aerobody.getHeadDeltaPos();
    	if (value == null)
    	{
    		drw.setValueText("---");
    	}
    	else
    	{
    		// Convert to cm.
    		value = value / 10.0f;
    		drw.setValueText(value.format("%.1f"));
    	}
    	drw.setBackgroundColor(vv_sensors.aerobody.getColour(vv_sensors.aerobody.HEAD_SENSOR));

		// chest
		drw = self.findDrawableById("data6");
        drw.setValueColor(as_colors.foreground);
    	value = vv_sensors.aerobody.getChestDeltaPos();
    	if (value == null)
    	{
    		drw.setValueText("---");
    	}
    	else
    	{
    		// Convert to cm.
    		value = value / 10.0f;
    		drw.setValueText(value.format("%.1f"));
    	}
    	drw.setBackgroundColor(vv_sensors.aerobody.getColour(vv_sensors.aerobody.CHEST_SENSOR));

    }
}
