using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Timer;
using Toybox.Activity as Activity;

class as_lap_table extends Ui.View 
{	
	
    function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.DataTable(dc));
    }
    
    // Update the view 
    function onUpdate(dc) 
    {
        View.onUpdate(dc);
		reDrawAll(dc);
    }

    // Update the view 
    function reDrawAll(dc) 
    {
		var font  = Gfx.FONT_SMALL;
		var textHeight = dc.getFontHeight(font);
		var w=dc.getWidth();
		var h=dc.getHeight();
		var ih = vv_icons.height;
		var w1 = dc.getTextWidthInPixels("X", font)*1.5;
		var w2 = (w-w1)/5;
        // Draw headers
		dc.setColor(as_colors.foreground, Gfx.COLOR_TRANSPARENT);

        dc.drawText(w1+w2/2,ih,font,as_session_data.sessionData[as_lap_data_array.LAP_NO],Gfx.TEXT_JUSTIFY_CENTER);

        var text = (as_session_data.sessionData[as_lap_data_array.LAP_CDA]*100).format("%.2f");
        dc.drawText(w1+w2*3/2,ih,font,text,Gfx.TEXT_JUSTIFY_CENTER);

        text = as_session_data.sessionData[as_lap_data_array.LAP_TIME].format("%.1f");
        dc.drawText(w1+w2*5/2,ih,font,text,Gfx.TEXT_JUSTIFY_CENTER);
        
        text = as_session_data.sessionData[as_lap_data_array.LAP_HEAD].format("%.1f");
        dc.drawText(w1+w2*7/2,ih,font,text,Gfx.TEXT_JUSTIFY_CENTER);

        text = as_session_data.sessionData[as_lap_data_array.LAP_CHEST].format("%.1f");
        dc.drawText(w1+w2*9/2,ih,font,text,Gfx.TEXT_JUSTIFY_CENTER);

        var headerHeight = ih + textHeight;
        dc.drawText(w1/2,headerHeight,font,"X",Gfx.TEXT_JUSTIFY_CENTER);
        dc.drawText(w1+w2/2,headerHeight,font,"Lap",Gfx.TEXT_JUSTIFY_CENTER);
        dc.drawText(w1+w2*3/2,headerHeight,font,"CdA",Gfx.TEXT_JUSTIFY_CENTER);
        dc.drawText(w1+w2*5/2,headerHeight,font,"Time",Gfx.TEXT_JUSTIFY_CENTER);
        dc.drawText(w1+w2*7/2,headerHeight,font,"Head",Gfx.TEXT_JUSTIFY_CENTER);
        dc.drawText(w1+w2*9/2,headerHeight,font,"Chest",Gfx.TEXT_JUSTIFY_CENTER);

        // Draw data
        var count = as_session_data.sessionLapData.size();
        var index = 0;
        for (var i=count-1;i>=0;i--)
        {
            var lap = as_session_data.sessionLapData[i];
            index++;
            var rowh = headerHeight+ index*(textHeight);
            if ((rowh + textHeight) > h) {break;}
            if (lap[as_lap_data_array.LAP_VALID])
            {
                dc.drawText(w1/2,rowh,font,"X",Gfx.TEXT_JUSTIFY_CENTER);
            }
            dc.drawText(w1+w2/2,rowh,font,lap[as_lap_data_array.LAP_NO],Gfx.TEXT_JUSTIFY_CENTER);

            text = (lap[as_lap_data_array.LAP_CDA]*100).format("%.2f");
            dc.drawText(w1+w2*3/2,rowh,font,text,Gfx.TEXT_JUSTIFY_CENTER);

            text = lap[as_lap_data_array.LAP_TIME].format("%.1f");
            dc.drawText(w1+w2*5/2,rowh,font,text,Gfx.TEXT_JUSTIFY_CENTER);
            
            text = lap[as_lap_data_array.LAP_HEAD].format("%.1f");
            dc.drawText(w1+w2*7/2,rowh,font,text,Gfx.TEXT_JUSTIFY_CENTER);

            text = lap[as_lap_data_array.LAP_CHEST].format("%.1f");
            dc.drawText(w1+w2*9/2,rowh,font,text,Gfx.TEXT_JUSTIFY_CENTER);
        }

    }
}
