using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Timer;

class vv_page3 extends Ui.View 
{
	const YSTART = 10;
	
    function initialize()  
    {
        Ui.View.initialize();	// Initialise UI
    }

    // Load your resources here
    function onLayout(dc) 
    {
        setLayout(Rez.Layouts.DataDial(dc));
        // Set headings etc:
        var drw;
        drw = self.findDrawableById("data");
        drw.setHeadingText("CdA");
        drw.setUnitText("");
        
        drw = self.findDrawableById("dial");
        drw.setMin(0);
        drw.setMax(100);
        drw.setTitle("% Aero");
        drw.setUnits("%");
    	
    }
    
    // Update the view 
    function onUpdate(dc) 
    {
		reDrawAll(dc);
        View.onUpdate(dc);
    }
    
    // Update the view 
    function reDrawAll(dc) 
    {
    	
    	var status = vv_sensors.getStatus();
    	
    	// Cda-----------------------------------------------------------------------------------------
    	var drw = self.findDrawableById("data");
    	drw.setMsgColor(status.color);
    	drw.setMsgText(status.text);
    	
    	var text = vv_sensors.aerosensor.data.cda.format("%.3f");
    	drw.setValueColor(status.color);
    	drw.setValueText(text);
    	
    	var pctDenom = vv_sensors.aerosensor.data.userPwrPct;
    	if (vv_sensors.aerosensor.data.gradePwrPct > 0)
    	{
    		pctDenom += vv_sensors.aerosensor.data.gradePwrPct;
    	}
    	if (vv_sensors.aerosensor.data.kineticPwrPct > 0)
    	{
    		pctDenom += vv_sensors.aerosensor.data.kineticPwrPct;
    	}
    	if (pctDenom == 0)
    	{
    		pctDenom = 1.0f;
    	}
    	var pctaero = -vv_sensors.aerosensor.data.windPwrPct * 1.0f / pctDenom * 100.0f;
    	if (pctaero > 100)
    	{
    		pctaero = 100;
    	}
    	else if (pctaero < 0)
    	{
    		pctaero = 0;
    	}
    	drw = self.findDrawableById("dial");
    	drw.setValue(pctaero);
    }
}
