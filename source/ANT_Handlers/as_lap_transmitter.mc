using Toybox.Ant as Ant;
using Toybox.System as Sys;
using Toybox.Time as Time;
using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Math;
using Toybox.Application as App;
using Toybox.Timer;
using Toybox.Lang;
		

class as_lap_transmitter extends Ant.GenericChannel
{
	const DEVICE_TYPE = 113;
	const PERIOD = 7590;
	hidden var m_channel;	
	var deviceCfg;
	var m_payload;
	
	function setData(lap, valid, cda, cpcor, laptime)
	{
		// Arrange data in 8-byte package.
		// Note I am calkling this page 237, although page nubmer doesn't appear.
		// Intent is it is the only page sent from this device..
		m_payload[0] = ((valid && 0x01)<<7) | (lap & 0x7F);
		var cda_int = (cda * 10000).toNumber();
		m_payload[1] = cda_int & 0xFF;
		m_payload[2] = (cda_int >> 8) & 0xFF;
		var cpcor_int = (cpcor * 10000).toNumber();
		m_payload[3] = cpcor_int & 0xFF;
		m_payload[4] = (cpcor_int >> 8) & 0xFF;
		var lt_int = (laptime * 1000).toNumber();
		Sys.println("Laptime = " + lt_int);
		m_payload[5] = lt_int & 0xFF;
		m_payload[6] = (lt_int >> 8) & 0xFF;
		m_payload[7] = (lt_int >> 16) & 0xFF;
		Sys.println("Sending message, first byte = " + m_payload[0]);
		var outgoing_msg = new Ant.Message();
		outgoing_msg.setPayload(m_payload);
		GenericChannel.sendBroadcast(outgoing_msg);

	}

	function initialize()
	{
		m_channel = new Ant.ChannelAssignment
		(
			Ant.CHANNEL_TYPE_TX_NOT_RX,
			Ant.NETWORK_PUBLIC
		);

		GenericChannel.initialize(method(:onMessage), m_channel);

		var deviceID = System.getDeviceSettings().uniqueIdentifier;
		var deviceNumber = 0;
		var charArray = deviceID.toCharArray();
		for (var i=0;i<deviceID.length();i++)
		{
			deviceNumber += charArray[i].toNumber();
		}

		System.println("Lap transmitter device number = " + deviceNumber);
		deviceCfg = new Ant.DeviceConfig
		({
			:deviceNumber => deviceNumber,
			:deviceType => DEVICE_TYPE,
			:messagePeriod => PERIOD,
			:transmissionType =>5,
			:radioFrequency => 67, 
			:searchTimeoutLowPriority => 10,    // Timeout in 25s
			:searchThreshold => 0               // Pair to all transmitting sensors
		});
		GenericChannel.setDeviceConfig(deviceCfg);	
		m_payload = new[8];
		for (var i=0;i<8;i++)
		{
			m_payload[i]=0;
		}
	}

	function open()
	{
		// Prime the pumps:
		var msg = new Ant.Message();
		msg.setPayload(m_payload);
		GenericChannel.open();
		GenericChannel.sendBroadcast(msg);
	}	
	
	function closeSensor()
	{
		GenericChannel.close();
	}
	
	function onMessage(msg)
	{
		var payload = msg.getPayload();
		
		//Sys.println("AT Message ID = 0x" + msg.messageId.format("%x") + ", payload 0 = 0x" + payload[0].format("%x") + ", payload 1 = 0x" + payload[1].format("%x") + ", payload 1 = 0x" + payload[2].format("%x"));

		// var payload2 = [0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08];
		// var msg2 = new Ant.Message();
		// msg2.setPayload(payload2);
		// GenericChannel.sendBroadcast(msg2);
		if (Ant.MSG_ID_BROADCAST_DATA == msg.messageId)
		{
		}

	}

}