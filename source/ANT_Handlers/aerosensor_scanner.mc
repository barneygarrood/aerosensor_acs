using Toybox.Ant as Ant;
using Toybox.System as Sys;


module aerosensor_module
{
	class aerosensor_scanner extends Ant.GenericChannel
	{
		const DEVICE_TYPE = 46;
		const PERIOD = 3995;
		hidden const ANT_FREQUENCY  = 67;
		
		var devices = new [16];		// Allow max of 16 devices to be found.
		var deviceCount = 0;
		var defaultDevice = 0;
		hidden var m_deviceCfg;
		hidden var m_chanAssign;
		
		hidden var m_callback;
		
		// ################################################################################
		// # ANT functions
		// ################################################################################	
		function initialize(callback,defaultDevice)
		{
			m_callback = callback;
			m_chanAssign = new Ant.ChannelAssignment
			(
				Ant.CHANNEL_TYPE_RX_ONLY,
				Ant.NETWORK_PUBLIC
			);
			// Set as background scan.
			m_chanAssign.setBackgroundScan(true);
			GenericChannel.initialize(method(:onMessage), m_chanAssign);
			m_chanAssign.isBackgroundScanEnabled();
			
			
			m_deviceCfg = new Ant.DeviceConfig
			({
				:deviceNumber => 0,		// Wildcard on device number.
				:deviceType => DEVICE_TYPE,
	            :messagePeriod => PERIOD,
				:transmissionType =>5,
				:radioFrequency => ANT_FREQUENCY,
				:searchTimeoutLowPriority => 10,
				:searchThreshold => 0, 
			});
			GenericChannel.setDeviceConfig(m_deviceCfg);
			defaultDevice = defaultDevice;
			startSearch();
		}
		
		function startSearch()
		{
			GenericChannel.open();
			devices = new[16];
			deviceCount = 0;
		}
				
		function stopSearch()
		{
			GenericChannel.close();
		}
		
		function release()
		{
			GenericChannel.release();
		}
		
		function reset()
		{
			devices = new [16];
			deviceCount = 0;
		}
		
		function onMessage(msg)
		{
			// Max of 16 devices:
			if (deviceCount == 16) 
			{
				return;
			}
			// Only perform on broadcast pages:
			if (Ant.MSG_ID_BROADCAST_DATA != msg.messageId)
			{
				return;
			}
			
			var exists = false;
			if (msg.deviceNumber == defaultDevice)
			{
				return;
			}
			for (var i=0; i<deviceCount; i++)
			{
				if (devices[i] == msg.deviceNumber) 
				{
					exists = true;
					m_callback.invoke(devices,i,msg.rssi,false);
					break;
				}
			}
			if (!exists)
			{
				devices[deviceCount] = msg.deviceNumber;
				deviceCount+=1;
				m_callback.invoke(devices, deviceCount, msg.rssi,true);
			}
		}
		
			
	}		
}