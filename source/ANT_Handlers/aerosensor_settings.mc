using Toybox.System as Sys;

module aerosensor_module
{		
	class aerosensor_settings
	{	
		const SETTING_WHEEL_CIRC = 0;
        const SETTING_TOTAL_MASS = 2;
        const SETTING_AERO_CAL = 7;
        const SETTING_PM_SCALING = 3;
        const SETTING_CRR = 11;
        
        var editableList = [7,2,0,3,9,10,11,18];
        var editableCount = 8;
        var originalVals = new[editableCount];
        var isValDirty = new[editableCount];
        var loaded;
        var status;
        
		var id_to_index_dic  =     {    18=>0,
                                19=>1,
                                144=>2,
                                145=>3,
                                146=>4,
                                216=>5,
                                217=>6,
                                218=>7,
                                220=>8,
                                221=>9,
                                222=>10,
                                223=>11,
                                147=>12,
                                149=>13,
                                150=>14,
                                151=>15,
                                152=>16,
                                153=>17,
                                154=>18,
                                155=>19
                                };
        var ids = [18,
                                19,
                                144,
                                145,
                                146,
                                216,
                                217,
                                218,
                                220,
                                221,
                                222,
                                223,
                                147,
                                149,
                                150,
                                151,
                                152,
                                153,
                                154,
                                155];
        
        var defs = [    "Wheel circumference",
                                "Elevation",
                                "Total mass",
                                "Power meter scaling",
                                "Short av period",
                                "Wheel moment of inertia",
                                "Reference CdA",
                                "Aero device calibration",
                                "Latency",
                                "CTF calibration offset",
                                "Time av period",
                                "Reference Crr",
                                "Baro averaging period",
                                "Demo mode pDyn",
                                "Demo mode yaw",
                                "Demo mode baro",
                                "Demo mode temp",
                                "Valid brake decceleration",
                                "Valid speed min",
                                "Valid speed hysteresis"
                                ];
                                
        var scaling= [    1,
                                0.3,
                                0.01,
                                0.000030517578125,
                                0.03125,
                                0.001,
                                0.0001,
                                0.000030517578125,
                                0.03125,
                                1,
                                0.03125,
                                0.00005,
                                0.03125,
                                0.01,
                                0.05,
                                -1,
                                0.01,
                                0.01,
                                0.01,
                                0.01];
                                                                
        var offset= [    0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                -90,
                                10800,
                                -30,
                                0,
                                0,
                                0];
                                                        
        var units = [    "mm",
                                "m",
                                "kg",
                                "",
                                "s",
                                "kgm2",
                                "",
                                "",
                                "s",
                                "Hz",
                                "s",
                                "",
                                "s",
                                "Pa",
                                "°",
                                "Pa",
                                "°C",
                                "m/s",
                                "m/s",
                                "m/s"];    
                                                        
        var format = [    "%u",
                                "%u",
                                "%u",
                                "%.3f",
                                "%.1f",
                                "%.3f",
                                "%.3f",
                                "%.3f",
                                "%.1f",
                                "%u",
                                "%.1f",
                                "%.3f",
                                "%.1f",
                                "%.1f",
                                "%.1f",
                                "%u",
                                "%.1f",
                                "%.1f",
                                "%.1f",
                                "%.1f"];    
                                                        
        var major =    [    10,
                                100,
                                5,
                                0.1,
                                1,
                                0.01,
                                0.01,
                                0.05,
                                1,
                                10,
                                1,
                                0.01,
                                1,
                                10,
                                1,
                                100,
                                1,
                                1,
                                1,
                                1];    
                                                        
        var minor =    [    1,
                                1,
                                1,
                                0.01,
                                0.1,
                                0.001,
                                0.001,
                                0.002,
                                0.1,
                                1,
                                0.1,
                                0.001,
                                0.1,
                                1,
                                0.1,
                                10,
                                0.1,
                                0.1,
                                0.1,
                                0.1];     
                                
		var min = [				10,
								0,
								0,
								0.1,
								0.25,
								0,
								0,
								0.1,
								0,
								0,
								1,
								0,
								0.25,
								10,
								-90,
								10800,
								-30,
								0.1,
								0.1,
								0.1];
								
		var max = [				9999,
								19660.5,
								655.35,
								2,
								2047.96875,
								65.535,
								6.5535,
								2,
								2047.96875,
								65535,
								2047.96875,
								3.27675,
								2047.96875,
								655.35,
								90,
								9000,
								100,
								10,
								100,
								10];
		
		                   
                                                                                
        var count = 20;
        var vals = new [count];
		
		function initialize()
		{
			loaded = false;
			status = new aerosensor_status();
		}
		
		function getFromBurst(data)
		{
	        // Put data into settings arrays:
	        for (var i=0;i<count;i++)
	        {
	        	var index = id_to_index_dic[data[i*3]];	// Get index from ID dictionary
	        	if (index != null)
	        	{
		        	// Get binary value from the two bytes:
		        	vals[index] = data[i*3+1] + data[i*3+2] << 8;
		        	// Scale to engineering units.
		        	vals[index] *= scaling[index];
		        	// Apply offset:
		        	vals[index] += offset[index];
		        }
	        }
	        // Make copy of editable vals:
	        for (var i=0;i<editableCount;i++)
	        {
	        	originalVals[i] = vals[editableList[i]];
	        }
	        status.isDirty = false;
	        loaded = true;
			
		}
		
	}
}