using Toybox.AntPlus;
using Toybox.System as Sys;

class aero_bsc_listener extends AntPlus.BikeSpeedCadenceListener
{
	hidden var m_spd;

	function initialize()
	{
		BikeSpeedCadenceListener.initialize();
		m_spd = null;
	}

	function onBikeSpeedCadenceUpdate(info)
	{
		if (info != null)
		{
			if (info.speed > 0)
			{
    			m_spd = info.speed*3.6;
				if (velo_view_fit_file.isLogging())
				{
					velo_view_fit_file.fields[ROAD_SPEED_FIELD].setData(m_spd);
				}
			}
		}
	}
	function getSpeed()
	{
		return m_spd;
	}
}


class antplus_bsc
{
	hidden var m_bsc;
	hidden var m_listener;
	
	function initialize()
	{
		// Initialise BSC device:
		m_listener = new aero_bsc_listener();
		m_bsc = new AntPlus.BikeSpeedCadence(m_listener);
	}
	
	function getSpeed()
	{
		return m_listener.getSpeed();
	}
	
	function getDeviceNumber()
	{
		return m_bsc.getDeviceState().deviceNumber;
	}
	
	// Returns device state.
	function getDeviceStateText()
	{
		var state = m_bsc.getDeviceState().state;
		var text = "";
		switch (state)
		{
			case AntPlus.DEVICE_STATE_DEAD:
				text = "Dead";
				break;
			case AntPlus.DEVICE_STATE_CLOSED:
				text = "Closed";
				break;
			case AntPlus.DEVICE_STATE_SEARCHING:
				text = "Searching";
				break;
			case AntPlus.DEVICE_STATE_TRACKING:
				text = "Tracking";
				break;
			case AntPlus.DEVICE_STATE_CNT:
				text = "Cnt (not sure..)";
				break;
		}
		return text;
	}
	
	
	
	function isTracking()
	{
		if (m_bsc.getDeviceState().state == AntPlus.DEVICE_STATE_TRACKING)
		{return true;} else {return false;}
	}
	
}