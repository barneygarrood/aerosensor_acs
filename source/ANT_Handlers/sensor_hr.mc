using Toybox.Sensor;
using Toybox.System as Sys;
using Toybox.Attention as Attention;

class sensor_hr
{
    hidden var m_connected = false;
    hidden var m_hr;
    
	function initialize()
	{
		Sensor.setEnabledSensors([Sensor.SENSOR_HEARTRATE,Sensor.SENSOR_BIKEPOWER]);
		Sensor.enableSensorEvents(method(:onSensor));
		m_connected = false;
		m_hr = 0;
	}
                       
	function onSensor(sensorInfo)
	{
        if (sensorInfo.heartRate != null) 
        {
        	if (!m_connected)
        	{
            	m_connected = true;
            }
        	m_hr = sensorInfo.heartRate;
        }
	}
	
	function getHeartRate()
	{
		return m_hr;
	}
}