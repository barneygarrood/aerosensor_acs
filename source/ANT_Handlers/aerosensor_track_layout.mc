using Toybox.System as Sys;

module aerosensor_module
{
	class aerosensor_track_layout
	{	
																					
		var count = 7;
		var vals = new [count];		
        var originalVals = new[count];
        var isValDirty = new[count];
        var status;
        
		var defs = [	"Corner 1 start",
						"Corner 1 end",
						"Corner 2 start",
						"Corner 2 end",
						"Track length",
						"Transition length",
						"Corner radius"
								];
								
		var scaling= [	0.1,0.1,0.1,0.1,0.1,0.1,0.01];
														
		var units = [	"m","m","m","m","m","m","m"];	
														
		var format = [			"%.1f",
								"%.1f",
								"%.1f",
								"%.1f",
								"%.1f",
								"%.1f",
								"%.2f"];	
														
		var major =	[	10,10,10,10,10,10,1];	
														
		var minor =	[	0.5,0.5,0.5,0.5,0.5,0.5,0.05];	
		
		var min = [0,0,0,0,0,0,10];
		var max = [10000,10000,10000,10000,10000,10000,10000];						
		
		function initialize()
		{
			status = new aerosensor_status();
		}
		
		function resync()
		{
	        for (var i=0;i<count;i++)
	        {
	        	originalVals[i] = vals[i];
	        }
	        status.isDirty = false;
		}
		
		function getFromBurst(data)
		{
	        // Put data into settings arrays:
	        for (var i=0;i<count;i++)
	        {
	        	// Get binary value from the two bytes:
	        	vals[i] = data[i*2] + data[i*2+1] << 8;
	        	// Scale to engineering units.
	        	vals[i] *= scaling[i];
	        }
	        // Make copy of editable vals:
	        for (var i=0;i<count;i++)
	        {
	        	originalVals[i] = vals[i];
	        }
	        status.isDirty = false;
		}
	}
}