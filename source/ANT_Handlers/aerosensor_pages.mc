using Toybox.AntPlus;
using Toybox.System as Sys;

module aerosensor_module
{
	class aerosensor_pages
	{
		// ################################################################################
		// # Page parsing functions
		// ################################################################################
		enum 
		{
			VALID_NONE = 0,
			VALID_SHORT = 1,
			VALID_LONG = 2,
			VALID_SECTION = 4,
			ACTIVE_SECTION = 8
		}
				
		function get_valid_flags(_data)
		{
			var valid_flags = VALID_NONE;
			valid_flags |= (_data.validCda * VALID_LONG);
			valid_flags |= (_data.validShort * VALID_SHORT);
			valid_flags |= (_data.validLap * VALID_SECTION);
			valid_flags |= (_data.activeLap * ACTIVE_SECTION);
			return valid_flags;
		}
		
		class page01
		{
			static const PAGE_NUMBER = 1;
			
			function parse(payload, _data, _page_data)
			{	
				var curEventCount = payload[1] & 0x1F;
				var paSign = (payload[1]>>7) & 0x01; 
				var curAccCda = ((payload[2]) | (payload[3]<<8));
				var curAccCdaYaw = (payload[4]) | (payload[5]<<8);
				_data.cdaError = payload[6] & 0x7F;
				_data.powerAdvantage = ((payload[6]>>7) & 0x01) | (payload[7]<<1);
				if (paSign) 
				{
					_data.powerAdvantage *= -1;
				}
				
				if (_page_data.isInit)
				{
					_data.validCda = (payload[1]>>5) & 0x01;
					_data.validCdaYaw = (payload[1]>>6) & 0x01;
					// Only update cda if valid, otherwise resets to zero.
					if (curEventCount - _page_data.prevEventCount)
					{				
						if (_data.validCda)
						{
							_data.cda = ((curAccCda - _page_data.prevAccCda) & 0xFFFF)/((curEventCount - _page_data.prevEventCount) & 0x1F) * 0.0001f;
						}
						_data.cdaYaw = ((curAccCdaYaw - _page_data.prevAccCdaYaw)& 0xFFFF) / ((curEventCount - _page_data.prevEventCount) & 0x1F) * 0.05f - 90.0f;
					}
				}
				else
				{
					_page_data.isInit = true;
					_data.cda = 0;
					_data.cdaYaw = 0;
					_data.validCda = false;
					_data.validCdaYaw = false;
				}
				_page_data.prevEventCount = curEventCount;
				_page_data.prevAccCda= curAccCda;
				_page_data.prevAccCdaYaw= curAccCdaYaw;
				// Update fields in FIT file:
				if (velo_view_fit_file.isLogging())
				{
					velo_view_fit_file.fields[CDA_FIELD].setData(_data.cda);
					//velo_view_fit_file.fields[CDA_ERROR_FIELD].setData(_data.cdaError);
				}
				//Ui.requestUpdate();
			}
		}
			
		class page18
		{
			static const PAGE_NUMBER = 18;
			function parse(payload, _data, _page_data)
			{	
				var curEventCount = payload[1] & 0x1F;
				var curAccPdyn = ((payload[3]) | (payload[4]<<8) | (payload[5]<<16));
				var curAccYaw = (payload[6]) | (payload[7]<<8);
				_data.density = payload[2] * 0.005f + 0.5f;
				if (_page_data.isInit)
				{
					if (curEventCount - _page_data.prevEventCount)
					{				
						_data.pdyn = ((curAccPdyn - _page_data.prevAccPdyn) & 0xFFFFFF)/((curEventCount - _page_data.prevEventCount) & 0x1F) * 0.001f;
						_data.yaw = ((curAccYaw - _page_data.prevAccYaw)& 0xFFFF) / ((curEventCount - _page_data.prevEventCount) & 0x1F) * 0.05f - 90.0f;
					}
					_data.validPdyn = (payload[1]>>6) & 0x01;
					_data.validYaw = (payload[1]>>7) & 0x01;
					if (_data.density > 0)
					{
						_data.windSpeed = Math.sqrt(2*_data.pdyn/_data.density)*3.6; // wind speed in m/sec
					}
				}
				else
				{
					_page_data.isInit = true;
					_data.pdyn = 0;
					_data.yaw = 0;
					_data.validPdyn = false;
					_data.validYaw = false;
				}
				_page_data.prevEventCount = curEventCount;
				_page_data.prevAccPdyn= curAccPdyn;
				_page_data.prevAccYaw= curAccYaw;
				// Update fields in FIT file:
				if (velo_view_fit_file.isLogging())
				{
					velo_view_fit_file.fields[WIND_YAW_FIELD].setData(_data.yaw);
					velo_view_fit_file.fields[AIR_DENSITY_FIELD].setData(_data.density);
					if (_data.windSpeed != null)
					{
						velo_view_fit_file.fields[WIND_SPEED_FIELD].setData(_data.windSpeed);
					}
				}
				//Ui.requestUpdate();
			}
		}
			
		class page19
		{
			static const PAGE_NUMBER = 19;
			function parse(payload, _data, _page_data)
			{	
				var curEventCount = payload[1] & 0x1F;
				_data.validPwr = (payload[1]>>6) & 0x01;
				_data.userPwrPct = payload[2];
				// Following is doing the twos compliment thing:
				_data.pwrDenom = (payload[6] | (payload[7] << 8)) * 0.05;
				// Put a minimum power denominator, to stop daft values coming through!
				if (_data.pwrDenom > 10)
				{
					_data.gradePwrPct = -1 * (payload[3] & 0x80) + (payload[3] & 0x7F);
					_data.fricPwrPct = -1 * (payload[4] & 0x80) + (payload[4] & 0x7F);
					_data.windPwrPct = -1 * (payload[5] & 0x80) + (payload[5] & 0x7F);
					_data.kineticPwrPct = -1*(_data.userPwrPct + _data.gradePwrPct + _data.fricPwrPct + _data.windPwrPct);
				}
				else
				{
					_data.gradePwrPct = 0;
					_data.fricPwrPct = 0;
					_data.windPwrPct = 0;
					_data.kineticPwrPct = -1*(_data.userPwrPct + _data.gradePwrPct + _data.fricPwrPct + _data.windPwrPct);
				}
				if (_page_data.isInit)
				{
				}
				else
				{
					_page_data.isInit = true;
				}
				_page_data.prevEventCount = curEventCount;
				
				// Update fields in FIT file:
				if (velo_view_fit_file.isLogging())
				{
					velo_view_fit_file.fields[RIDER_POWER_FIELD].setData(_data.pwrDenom * _data.userPwrPct / 100.0f);
					velo_view_fit_file.fields[GRAV_POWER_FIELD].setData(_data.pwrDenom * _data.gradePwrPct / 100.0f);
					velo_view_fit_file.fields[FRIC_POWER_FIELD].setData(_data.pwrDenom * _data.fricPwrPct / 100.0f);
					velo_view_fit_file.fields[AERO_POWER_FIELD].setData(_data.pwrDenom * _data.windPwrPct / 100.0f);
				}
				//Ui.requestUpdate();
			}
		}
			
		class page20
		{
			static const PAGE_NUMBER = 20;
			
			function parse(payload, _data, _page_data)
			{	
				var curAccTemp = (payload[5]) | (payload[6]<<8);
				var curAccBaro = (payload[2]) | (payload[3]<<8) | (payload[4]<<16);
				var curEventCount = payload[1] & 0x1F;
				if (_page_data.isInit)
				{
					// Check non-zero event count increment:
					if (curEventCount - _page_data.prevEventCount)
					{				
						_data.temperature = ((curAccTemp - _page_data.prevAccTemp) & 0xFFFF)/((curEventCount - _page_data.prevEventCount) & 0x1F)/100f - 30f;
						_data.baro = 108000 - ((curAccBaro - _page_data.prevAccBaro) & 0xFFFFFF)/((curEventCount - _page_data.prevEventCount) & 0x1F)*0.05;
					}
					
				}
				else
				{
					_data.temperature = 0;
					_data.baro = null;
					_page_data.isInit = true;
					_data.prev_baro = null;
				}
				_page_data.prevEventCount = curEventCount;
				_page_data.prevAccTemp = curAccTemp;
				_page_data.prevAccBaro= curAccBaro;
				// Update fields in FIT file:
				if (velo_view_fit_file.isLogging() & (_data.baro!=null))
				{
					velo_view_fit_file.fields[BAROMETRIC_PRESSURE_FIELD].setData(_data.baro);
				}
				//Ui.requestUpdate();
			}
		}
		
		class page81
		{
			static const PAGE_NUMBER = 81;
			
			function parse(payload, _data)
			{	
				_data.swVersionSet = true;
				_data.majSwVersion = payload[3];
				_data.minSwVersion = payload[2];
				// Convoluted way of getting unisnged 32 bit integer from byte array.
				// Every other way I try returns a signed integer.
				var ba = new [4]b;
				ba[0]=payload[4];
				ba[1]=payload[5];
				ba[2]=payload[6];
				ba[3]=payload[7];
				_data.serialNumber = ba.decodeNumber(Lang.NUMBER_FORMAT_UINT32, null);
			}
		}
		
		
		class page86
		{
			static const PAGE_NUMBER = 86;
			
			function parse(payload, _data)
			{
				var index = null;
				switch (payload[7])
				{
					case 11:				//bpwr
						index = 0;
						break;
					case 123:				//spd
						index = 1;
						break;
					case 121:				//bsc
						index = 2;
						break;
				}
				if (index == null) {return;}
				if ((payload[3]>>7) == 0)
				{
					_data.pairedDevices[index].channelState = AntPlus.DEVICE_STATE_DEAD;
				}
				else
				{
					switch ((payload[3] >> 3) & 15)
					{
						case 0:
							_data.pairedDevices[index].channelState = AntPlus.DEVICE_STATE_CLOSED;
							break;
						case 1:
							_data.pairedDevices[index].channelState = AntPlus.DEVICE_STATE_SEARCHING;
							break;
						case 2:
							_data.pairedDevices[index].channelState = AntPlus.DEVICE_STATE_TRACKING;
							break;
						default:
							_data.pairedDevices[index].channelState = AntPlus.DEVICE_STATE_CNT;
					}
				}
				_data.pairedDevices[index].deviceNumber = payload[4] + (payload[5]<<8);
				_data.pairedDevices[index].transmissionType = payload[6];
				_data.pairedDevices[index].deviceType = payload[7];
				_data.pairedDevices[index].lastUpdateTime = Sys.getTimer();
			}
		}
		
		class page224
		{
			static const PAGE_NUMBER = 224;
			function parse(payload,_data)
			{
				_data.calcMode = payload[1];
				_data.demoMode = payload[2];
				_data.batVoltage = payload[6]/256.0f + (payload[7] & 0x0F);
				_data.batLevel = (payload[7]>>4) & 0x07;
				logging.log( "_data.batVoltage = " + _data.batVoltage );
				logging.log("payload[6]|[7] = " + payload[6] + "|" + payload[7]);
			}
		}
				
		class page229
		{
			static const PAGE_NUMBER = 229;
			function parse(payload,_data)
			{
				var lapNumber = payload[1] & 0x0F;
				//Sys.println("lapNumber = " + lapNumber + ", deviceLapNumber = " + _data.deviceLapNumber);
				if (lapNumber != _data.deviceLapNumber)
				{
					if (((lapNumber - data.deviceLapNumber) & 0x0F) < 2)
					{
						_data.deviceLapNumber = lapNumber;
						_data.prevLapNumber = _data.lapNumber;
						_data.prevLapCda = _data.cdaLap;
						// Tell device to mark new lap, only if p229 has been initialised already.
						// Dont necessarily want to start new lap on connection.
						// This may need refining.
						if (!_data.p229Init)
						{
							_data.p229Init = true;
						}
					
						_data.lapNumber++;
					}
					else
					{	
						_data.deviceLapNumber = lapNumber;
						_data.prevLapNumber = _data.lapNumber;
						_data.prevLapCda = _data.cdaLap;
						_data.lapNumber = lapNumber;
					}
				}
				_data.validCda = (payload[1] >> 4) & 0x01;
				// Note device now sends last valid value in case currently invalid..
				_data.cda = ((payload[2]) | ((payload[3] & 0x0F)<<8)) * 0.0002;
				_data.cdaError = payload[4] & 0x7F;
				
				_data.validShort = (payload[1] >> 5) & 0x01;
				_data.cdaShort = ((payload[5]) | ((payload[6] & 0x0F)<<8)) * 0.0002;
				
				// Device either sends current lap CdA, or that of previous, so always correct.
				_data.validLap = (payload[1] >> 6) & 0x01;
				_data.activeLap = (payload[1] >> 7) & 0x01;
				_data.cdaLap = (((payload[6] >> 4) & 0x0F) | (payload[7]<<4)) * 0.0002;
				// Update fields in FIT file:
				if (velo_view_fit_file.isLogging())
				{
					velo_view_fit_file.fields[CDA_FIELD].setData(_data.cda);
					velo_view_fit_file.fields[LAP_FIELD].setData(_data.lapNumber - _data.offsetLapNumber);

					velo_view_fit_file.fields[CDA_LAP_RECORD_FIELD].setData(_data.cdaLap);
					velo_view_fit_file.fields[CDA_LAP_FIELD].setData(_data.cdaLap);
					// Pack bytes 1 and 6 into a single value.
					var status = payload[1] | (payload[4]<<8);
					velo_view_fit_file.fields[CDA_STATUS_FIELD].setData(status);
				}
			}
		}

		class page230
		{
			static const PAGE_NUMBER = 230;
			
			function parse(payload, _data, _page_data)
			{	
				var curAccTemp = (payload[5]) | (payload[6]<<8);
				var curAccBaro = (payload[2]) | (payload[3]<<8) | (payload[4]<<16);
				var curEventCount = payload[1] & 0x1F;
				_data.temperature = curAccTemp/100f - 30f;
				if (_page_data.isInit)
				{
					// Check non-zero event count increment:
					if (curEventCount - _page_data.prevEventCount)
					{				
						_data.baro = 108000 - ((curAccBaro - _page_data.prevAccBaro) & 0xFFFFFF)/((curEventCount - _page_data.prevEventCount) & 0x1F)*0.05;
					}
					if ((_data.prev_baro != 0) && (_data.density > 0))
					{
						_data.elevation = _data.elevation - (_data.baro - _data.prev_baro) / _data.density / 9.807;
					}
					_data.prev_baro = _data.baro;
				}
				else
				{
					_data.baro = 0.0f;
					_page_data.isInit = true;
					_data.elevation = 0.0f;
					_data.prev_baro = 0.0f;
				}
				_page_data.prevEventCount = curEventCount;
				_page_data.prevAccBaro= curAccBaro;
				// Update fields in FIT file:
				if (velo_view_fit_file.isLogging())
				{
					velo_view_fit_file.fields[BAROMETRIC_PRESSURE_FIELD].setData(_data.baro);
				}
			}
		}
//		Page 233 no longer used in CIQ.  This should only be used from lap trigger to aerosensor.
// 		Leave basic definition in here.
		class page233
		{
			static const PAGE_NUMBER = 233;
			// NOT USED
		}	
	}
}