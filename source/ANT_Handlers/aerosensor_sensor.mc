using Toybox.Ant as Ant;
using Toybox.System as Sys;
using Toybox.Time as Time;
using Toybox.WatchUi as Ui;
using Toybox.Math;
using Toybox.Application as App;
using Toybox.Timer;
using Toybox.Attention;

module aerosensor_module
{
	class aerosensor_sensor extends Ant.GenericChannel
	{
		// ################################################################################
		// # Class variable declarations
		// ################################################################################
		hidden const DEVICE_TYPE = 46;
		hidden const PERIOD = 3995;
		hidden const SETTINGS_FREQ = 3; //3;
		hidden const ANT_FREQUENCY  = 67;
		const MODE_DEFAULT = 0;
		const MODE_LEVELLING = 16;
		
		hidden var m_mode;
		
		hidden var chanAssign;
		
		var data;
		var p01_data;
		var p18_data;
		var p19_data;
		var p20_data;
		var p230_data;
		var p233_data;
		var searching;
		var deviceCfg;
		var counter;
		var timerOffset = 2000;
		var maxOffset = 0;
		var settings;
		var track;
		var lap_data;
		var prev_lap_data;
		
		hidden var burstListener;
		hidden var defaultDeviceNumber;
		var sendQueueIndex = 0;
		var sendQueueSize = 0;
		var sendQueueMaxSize = 8;
		var sendQueue = new[sendQueueMaxSize];
		
		const SW_VER_REQUEST_MAX = 16; // i.e. request page 81 a maximum of 16 times.
		const SW_VER_REQUEST_PERIOD = 16; // i.e. request every 16 packets.
		hidden var swVersionTryCount = 0;
		hidden var swVersionCounter = 0;
	
		hidden var m_timer;
		hidden var lapNumber = 0;
		
		// ################################################################################
		// # ANT functions
		// ################################################################################	
		function initialize()
		{
			chanAssign = new Ant.ChannelAssignment
			(
				Ant.CHANNEL_TYPE_RX_NOT_TX,
				Ant.NETWORK_PUBLIC
			);
			GenericChannel.initialize(method(:onMessage), chanAssign);
			
			// Get device number:
			defaultDeviceNumber = App.Storage.getValue("AerosensorDeviceNumber");
			if (defaultDeviceNumber == null){defaultDeviceNumber = 0;}
			deviceCfg = new Ant.DeviceConfig
			({
				:deviceNumber => defaultDeviceNumber,
				:deviceType => DEVICE_TYPE,
	            :messagePeriod => PERIOD,
				:transmissionType =>5,
				:radioFrequency => ANT_FREQUENCY,
				:searchTimeoutLowPriority => 10,
				:searchThreshold => 0
			});
			logging.log("Device number = " + defaultDeviceNumber);
							logging.log("Changed frequency to " + deviceCfg.radioFrequency);
			GenericChannel.setDeviceConfig(deviceCfg);
			
			burstListener = new TestBurstListener(self, method(:processBurst));
			GenericChannel.setBurstListener(burstListener);
			
			// Initialise data points:
			data = new aerosensor_data();
			p01_data = new aerosensor_p01_data();
			p18_data = new aerosensor_p18_data();
			p19_data = new aerosensor_p19_data();
			p20_data = new aerosensor_p20_data();
			p230_data = new aerosensor_p230_data();
			p233_data = new aerosensor_p233_data();
			
			// Initialise settings - before loaded from device there should be none:
			searching = true;
			settings = new aerosensor_settings();
			track = new aerosensor_track_layout();
			lap_data = new as_lap_data();
			prev_lap_data = new as_lap_data();
			// Initialise send queue.  Works as a circular buffer.
			for (var i=0;i<sendQueueSize;i++)
			{
				sendQueue[i] = new[8];
			}
			sendQueueIndex = 0;
			sendQueueSize = 0;
			
			// Start timer to request connection and settings data:
	        m_timer = new Timer.Timer();
	        m_timer.start(method(:timerCallback),1000,true);
	        m_mode = MODE_DEFAULT;
		}
		
		function timerCallback()
		{
			if (!searching)
			{
//				// Check if we need to get settings:
				if (!settings.loaded)
				{
					sendRequestSettingsCommand();
				}
				else
//				{
					// Check if need to request pass-pair data:
					// I don't think we do because device sends it if needed.
					// Just need to figure out how to know device is connected or not..
					if (data.pairedDevices[0].channelState != AntPlus.DEVICE_STATE_TRACKING)
					{
						// Only request if last request more than 5s ago.
						if ((Sys.getTimer() - data.pairedDevices[0].lastUpdateTime) > 5000)
						{
							sendRequestPairedDevicesPage(0);
						}
					}
					// Only need one of speed and speed/cadence sensor:
					else if (
							(data.pairedDevices[1].channelState != AntPlus.DEVICE_STATE_TRACKING)&
							(data.pairedDevices[2].channelState != AntPlus.DEVICE_STATE_TRACKING))
					{
						for (var i=1;i<3;i++)
						{
							if ((Sys.getTimer() - data.pairedDevices[i].lastUpdateTime) > 5000)
							{
								sendRequestPairedDevicesPage(i);
							}
						}
					}
				//}
			}
		}
		
		function settingsReset()
		{
			settings.status = new aerosensor_status();
		}
		
		function trackReset()
		{
			track.status = new aerosensor_status();
		}
		
		function putPageInSendQueue(page)
		{
			// Check connected:
			if (searching){return;}
			// check queue isn't overgrown:
			if (sendQueueSize == sendQueueMaxSize) {return;}
			var index = (sendQueueIndex + sendQueueSize) & 0x07;
			sendQueue[index] = page;
			sendQueueSize = sendQueueSize + 1;
		}
		
		function getPageFromSendQueue()
		{
			// Check there are pages to send:
			if (sendQueueSize == 0) {return 0;}
			var ret = sendQueue[sendQueueIndex];
			sendQueueSize--;
			sendQueueIndex = (sendQueueIndex + 1) & 0x07;
			return ret;
		}
		
		function changeDeviceID(newID)
		{
			logging.log("Change device ID");
			closeSensor();
			deviceCfg.deviceNumber = newID;
			GenericChannel.setDeviceConfig(deviceCfg);
			open();
		}
		
		function open()
		{
			logging.log("Open");
			searching = true;
			data = new aerosensor_data();
			GenericChannel.open();
		}	
		
		function closeSensor()
		{
			logging.log("Close");
			GenericChannel.close();
		}

		function setDemoMode(index)
		{
			// Send command 0x82 = 130 on page 33 with index of demo mode.
			// Note we should eventually do a timesync command..
			var payload = [33,00,130,index,00,00,00,00];
			// Add to queue:
			logging.log("Adding payload to queue: " + payload);
			putPageInSendQueue(payload);
			
		}
		function setCalcMode(index)
		{
			// Send command 0x85 = 133 on page 33 with index of calculation mode.
			// Note we should eventually do a timesync command..
			var payload = [33,00,133,index,00,00,00,00];
			// Add to queue:
			logging.log("Adding payload to queue: " + payload);
			putPageInSendQueue(payload);
			
		}
		function clearModes()
		{
			// Resets modes:
			data.calcMode = -1;
			data.demoMode = -1;
		}
		
		function calcModeText()
		{
			var text = "Undefined: " + data.calcMode;
			switch (data.calcMode)
			{
				case 0:
					text = "Road";
					break;
				case 1:
					text = "Velodrome trackless";
					break;
				case 3:
					text = "Velodrome tracked";
					break;
			}
			return text;
		}
		
		function demoModeText()
		{
			var text = "Undefined: " + data.demoMode;
			switch (data.demoMode)
			{
				case 0:
					text = "Default";
					break;
				case 1:
					text = "Constant all";
					break;
				case 2:
					text = "True baro";
					break;
				case 3:
					text = "Sine baro";
					break;
				case 4:
					text = "Match windspeed";
					break;
				case 5:
					text = "Match windspeed for calc";
					break;
			}
			return text;
		}
		
		function sendNewLapCommand()
		{
			// Send command "02" on page 33.
			// Note we should eventually do a timesync command..
			var payload = [33,00,02,00,00,00,00,00];
			// Add to queue:
			putPageInSendQueue(payload);
		}	// end sentNewLapCommand
		
		function resetLapNumber()
		{
			data.offsetLapNumber = data.lapNumber-1;	// Make sure start at 1!
			data.lapStartTime = null;
		}

		function offsetLapNo()
		{
			return data.lapNumber - data.offsetLapNumber;
		}

		function sendStartSessionCommand()
		{
			// Send command "03" on page 33.
			// Note we should eventually do a timesync command..
			var payload = [33,00,03,00,00,00,00,00];
			// Add to queue:
			putPageInSendQueue(payload);
		}	// end sentNewLapCommand
		
		function sendEndSessionCommand()
		{
			// Send command "04" on page 33.
			// Note we should eventually do a timesync command..
			var payload = [33,00,04,00,00,00,00,00];
			// Add to queue:
			putPageInSendQueue(payload);
		}	// end sentNewLapCommand
		
		function sendOpenChannelCommand(deviceType, deviceNumber)
		{
			var payload = [0x4A,0xFF,0xFF,0x00,0xFF,0xFF,0xFF,0xFF];
			switch (deviceType)
			{
				case 11:
					payload[4] = 11;
					payload[5] = 57;
					payload[6] = 8182 & 0xFF;
					payload[7] = (8182 >>8) & 0xFF;
					break;
				case 121:
					payload[4] = 121;
					payload[5] = 57;
					payload[6] = 8086 & 0xFF;
					payload[7] = (8086 >>8) & 0xFF;
					break;
				case 123:
					payload[4] = 123;
					payload[5] = 57;
					payload[6] = 8118 & 0xFF;
					payload[7] = (8118 >>8) & 0xFF;
					break;
				default:
					return;
			}
			payload[1] = deviceNumber & 0xFF;
			payload[2] = (deviceNumber >> 8) & 0xFF;
			logging.log("Open channel command: " + payload);
			putPageInSendQueue(payload);
		}
		
		function sendRequestPairedDevicesPage(deviceIndex)
		{
			var payload = [0x46, 0xFF, 0xFF, deviceIndex, 0xFF, 0x01, 0x56, 0x01];		// Send paired devices page for deviceIndex.  Send page only once.
			putPageInSendQueue(payload);
		}
		
		function sendRequestSettingsCommand()
		{
			if (data.pendingAction != AEROSENSOR_PENDING_NONE) 
			{
				return;
			}
			if (searching)
			{
				settings.status.settingsPull = SETTINGS_DISCONNECTED;
			}
			else
			{
				// Send packet:
				var packet = [0x46, SETTINGS_FREQ, 0xFF,0xFF,0xFF,0x80,0x1F,0x01];	// Request settings page 0x1F on frequency 3 (0x03), send until ack (0x80).
	
				putPageInSendQueue(packet);
				// Clear old settings, and set status flag:
				settings.status.settingsPull = SETTINGS_SENDING;
				data.pendingAction = AEROSENSOR_PENDING_SETTINGS;
			}
		}
		
		// Request given page.  If device not connected then return 1, otherwise return 0.
		function sendRequestPageCommand(page_requested)
		{
			// Dom't think this is necessary, but leave in just in case..
			if (searching)
			{
				return 1;
			}
			else
			{
				// Send packet:
				var packet = [0x46, 0xFF, 0xFF,0xFF,0xFF,0x80,page_requested,0x01];	// Request settings page 0x1F on frequency 3 (0x03), send until ack (0x80).
	
				putPageInSendQueue(packet);
				return 0;
			}
		}
		
		
		function sendRequestTrackCommand()
		{
			if (data.pendingAction != AEROSENSOR_PENDING_NONE) 
			{
				return;
			}
			if (searching)
			{
				track.status.settingsPull = SETTINGS_DISCONNECTED;
			}
			else
			{
				// Send packet:
				var packet = [0x46, SETTINGS_FREQ, 0xFF,0xFF,0xFF,0x80,0xEB,0x01];	// Request settings page 0x1F on frequency 3 (0x03), send until ack (0x80).
	
				putPageInSendQueue(packet);
				// Clear old settings, and set status flag:
				track.status.settingsPull = SETTINGS_SENDING;
				data.pendingAction = AEROSENSOR_PENDING_TRACK;
			}
		}
		
		function changeMode(mode)
		{
			var switchMode = false;
			m_mode = mode;
		}
				
		private function requestModeChange(mode)
		{
			var payload = [0x21, 0x00, 0x80, mode, 0x00, 0x00, 0x00, 0x00];		// Send paired devices page for deviceIndex.  Send page only once.
			putPageInSendQueue(payload);
		}
		
		private function sendLapReceipt()
		{
			var payload = [0xEA, p233_data.lapNumber, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF];
			putPageInSendQueue(payload);
		}
		
		function checkRequestSwVersion()
		{
			if (data.swVersionSet){return;}
			if (swVersionTryCount > SW_VER_REQUEST_MAX){return;}
			swVersionCounter++;
			if (swVersionCounter == SW_VER_REQUEST_PERIOD)
			{
				swVersionCounter = 0;
				requestSoftwareVersion();
				swVersionTryCount++;
			}
		}
		
		function onMessage(msg)
		{
			var payload = msg.getPayload();
			if (Ant.MSG_ID_BROADCAST_DATA == msg.messageId)
			{
				if (settings.status.settingsPull == SETTINGS_SUCCESS)
				{
					counter++;
				}
				// Deal with software version page:
				checkRequestSwVersion();
				if (searching)
				{
					searching = false;
					deviceCfg = GenericChannel.getDeviceConfig();
					// Store device number, if necessary:
					if (deviceCfg.deviceNumber != defaultDeviceNumber)
					{
						defaultDeviceNumber = deviceCfg.deviceNumber;
						App.Storage.setValue("AerosensorDeviceNumber",defaultDeviceNumber);
					}
					Ui.requestUpdate();
				}
				// Check timer offset:
				var now = Sys.getTimer();
				var offset = (now - (msg.timestamp *1000)/32768) % 2000;
				if (offset < timerOffset){timerOffset = offset;}
				// Decode depending on which page was received:
				switch (payload[0])
				{
					case aerosensor_pages.page01.PAGE_NUMBER:
						aerosensor_pages.page01.parse(payload, data, p01_data);
						if (m_mode != MODE_DEFAULT){requestModeChange(m_mode);}
						break;
					case aerosensor_pages.page18.PAGE_NUMBER:
						aerosensor_pages.page18.parse(payload, data, p18_data);
						if (m_mode != MODE_DEFAULT){requestModeChange(m_mode);}
						break;
					case aerosensor_pages.page19.PAGE_NUMBER:
						aerosensor_pages.page19.parse(payload, data, p19_data);
						if (m_mode != MODE_DEFAULT){requestModeChange(m_mode);}
						break;
					case aerosensor_pages.page20.PAGE_NUMBER:
						aerosensor_pages.page20.parse(payload, data, p20_data);
						if (data.baro != null)
						{	
							if ((data.prev_baro != null) && (data.density != null))
							{
								data.elevation = data.elevation - (data.baro - data.prev_baro) / data.density / 9.807;
							}
							else
							{
								data.elevation = 0;
							}
							data.prev_baro = data.baro;
						}
						if (m_mode != MODE_DEFAULT){requestModeChange(m_mode);}
						break;
					case aerosensor_pages.page81.PAGE_NUMBER:
						aerosensor_pages.page81.parse(payload, data);
						break;
					case aerosensor_pages.page86.PAGE_NUMBER:
						aerosensor_pages.page86.parse(payload, data);
						break;
					case aerosensor_pages.page224.PAGE_NUMBER:
						logging.log("Page 224"); 
						aerosensor_pages.page224.parse(payload, data);
						break;
					case aerosensor_pages.page229.PAGE_NUMBER:
						aerosensor_pages.page229.parse(payload, data);
						if (m_mode != MODE_DEFAULT){requestModeChange(m_mode);}
						break;
					case aerosensor_pages.page230.PAGE_NUMBER:
						aerosensor_pages.page230.parse(payload, data, p230_data);
						if (m_mode != MODE_DEFAULT){requestModeChange(m_mode);}
						break;
					case aerosensor_pages.page233.PAGE_NUMBER:
						// NO LONGER USED!!
						break;
				}
				// Send page from queue if one exists:
				if (sendQueueSize>0)
				{
					var page = getPageFromSendQueue();
					msg = new Ant.Message();
					var msgTxt = "";
					for (var i=0;i<8;i++)
					{
						msgTxt += "[" + page[i] + "]";
					}
					msg.setPayload(page);
					if (msg != false)
					{
						GenericChannel.sendAcknowledge(msg);
					}
				}
				data.lastMsgTime = msg.timestamp;
			} //  end broadcast data
			else if (Ant.MSG_ID_CHANNEL_RESPONSE_EVENT == msg.messageId)
			{
				if (Ant.MSG_ID_RF_EVENT == (payload[0] & 0xFF))
				{
					if (Ant.MSG_CODE_EVENT_CHANNEL_CLOSED == (payload[1] & 0xFF))
					{
	                    // Channel closed, re-open
						open();
					}
					else if (Ant.MSG_CODE_EVENT_RX_FAIL_GO_TO_SEARCH == (payload[1] & 0xFF))
					{
						if (!searching)
						{
							searching = true;
							Ui.requestUpdate();
						}
					}
					else if (Ant.MSG_CODE_EVENT_TRANSFER_TX_COMPLETED == (payload[1] & 0xFF))
					{
						if (data.pendingAction == AEROSENSOR_PENDING_SETTINGS)
						{
							// If we requested settings then need to act to change frequency, and setup burst receiver:
							if(settings.status.settingsPull == SETTINGS_SENDING)
							{
								settings.status.settingsPull = SETTINGS_SUCCESS;  // Means succesfully requested, not complete.
								counter = 0;	// Only leave open for 6 cycles max;
								var deviceCfg = GenericChannel.getDeviceConfig();
								deviceCfg.radioFrequency = SETTINGS_FREQ;
								GenericChannel.setDeviceConfig(deviceCfg);
								logging.log("Changed frequency to " + deviceCfg.radioFrequency);
							}
							if(settings.status.settingsPush == SETTINGS_SENDING)
							{
								settings.status.settingsPush = SETTINGS_COMPLETE;
								data.pendingAction = AEROSENSOR_PENDING_NONE;
							}
						}
						else if (data.pendingAction == AEROSENSOR_PENDING_TRACK)
						{
							// If we requested settings then need to act to change frequency, and setup burst receiver:
							if(track.status.settingsPull == SETTINGS_SENDING)
							{
								track.status.settingsPull = SETTINGS_SUCCESS;  // Means succesfully requested, not complete.
								counter = 0;	// Only leave open for 6 cycles max;
								var deviceCfg = GenericChannel.getDeviceConfig();
								deviceCfg.radioFrequency = SETTINGS_FREQ;
								GenericChannel.setDeviceConfig(deviceCfg);
								logging.log("Changed frequency to " + deviceCfg.radioFrequency);
							}
							if(track.status.settingsPush == SETTINGS_SENDING)
							{
								track.status.settingsPush = SETTINGS_COMPLETE;
								data.pendingAction = AEROSENSOR_PENDING_NONE;
								track.resync();
							}
						}
					}
					else if (Ant.MSG_CODE_EVENT_TRANSFER_TX_FAILED == (payload[1] & 0xFF))
					{
						if (data.pendingAction == AEROSENSOR_PENDING_SETTINGS)
						{
							if(settings.status.settingsPull == SETTINGS_SENDING)
							{
								settings.status.settingsPull = SETTINGS_FAILURE;
								data.pendingAction = AEROSENSOR_PENDING_NONE;
							}
							if(settings.status.settingsPush == SETTINGS_SENDING)
							{
								settings.status.settingsPush = SETTINGS_FAILURE;
								data.pendingAction = AEROSENSOR_PENDING_NONE;
							}
						}
						else if (data.pendingAction == AEROSENSOR_PENDING_TRACK)
						{
							if(track.status.settingsPull == SETTINGS_SENDING)
							{
								track.status.settingsPull = SETTINGS_FAILURE;
								data.pendingAction = AEROSENSOR_PENDING_NONE;
							}
							if(track.status.settingsPush == SETTINGS_SENDING)
							{
								track.status.settingsPush = SETTINGS_FAILURE;
								data.pendingAction = AEROSENSOR_PENDING_NONE;
							}
						}
						
					}
					else if (Ant.MSG_CODE_EVENT_RX_FAIL == (payload[1] & 0xFF))
					{
						if (data.pendingAction == AEROSENSOR_PENDING_SETTINGS)
						{
							if ((settings.status.settingsPull == SETTINGS_SUCCESS))
							{
								counter++;
							}
						}
						else if (data.pendingAction == AEROSENSOR_PENDING_TRACK)
						{
							if ((track.status.settingsPull == SETTINGS_SUCCESS))
							{
								counter++;
							}
						}
					}
				}
			} // end channel response event
			
			if (data.pendingAction == AEROSENSOR_PENDING_SETTINGS)
			{
				if ((settings.status.settingsPull == SETTINGS_SUCCESS) && (counter == 6))
				{
					settings.status.settingsPull = SETTINGS_FAILURE;
					data.pendingAction = AEROSENSOR_PENDING_NONE;
					resetFrequency();
				}
			}
			else if (data.pendingAction == AEROSENSOR_PENDING_TRACK)
			{
				if ((track.status.settingsPull == SETTINGS_SUCCESS) && (counter == 6))
				{
					track.status.settingsPull = SETTINGS_FAILURE;
					data.pendingAction = AEROSENSOR_PENDING_NONE;
					resetFrequency();
				}
			}
		} // end on message
		
		function resetFrequency()
		{
			var deviceCfg = GenericChannel.getDeviceConfig();
			deviceCfg.radioFrequency = ANT_FREQUENCY;
			GenericChannel.setDeviceConfig(deviceCfg);
			logging.log("Changed frequency to " + deviceCfg.radioFrequency);
		}
		
		// Request page 81 = software version.
		function requestSoftwareVersion()
		{
			var payload = [0x46, 0xFF, 0x00, 0x00, 0x00, 0x01, 0x51, 0x01];	
			
			putPageInSendQueue(payload);
		}
		
		// Used for both aerosensor parameters and track layout.
		function processBurst(burstPayload)
		{
	        var itr = new Ant.BurstPayloadIterator(burstPayload);
	        var payload = itr.next();
	        var type = 0;
	        var settings_type = false;
	        if (payload[0] == 0x1F)
	        {
	        	type = AEROSENSOR_SETTING_TYPE_PARAMETER;
	        	settings_type = true;
	        }
	        else if (payload[0] == 0xEB)
	        {
	        	type = AEROSENSOR_SETTING_TYPE_TRACK;
	        	settings_type = true;
	        }
	        else if (payload[0] == 0xEC)
	        {
	        	type = AEROSENSOR_SETTING_TYPE_LAP_TRIGGER;
	        }
			// Reject if page number isn't correct for settings:
			else
			{
				return;
			}
		    var burst_data;
			if (settings_type)
			{
				// Get number of settings:
				var count = payload[2];
				if (count > 0)
				{		
					// Put burst data into single array.  Easier to handle.
					burst_data = new[(burstPayload.getSize() - 1) *8];
		        	payload = itr.next();
		        	var index = 0;
			        while( null != payload )
			        {
			            for (var i=0;i<8;i++)
			            {
			            	burst_data[index]=payload[i];
			            	index++;
			            }
			            payload = itr.next();
			        }
		        }
		    }
		    else
		    {	
				// Put burst data into single array.  Easier to handle.
				burst_data = new[(burstPayload.getSize()) *8];
	        	var index = 0;
		        while( null != payload )
		        {
		            for (var i=0;i<8;i++)
		            {
		            	burst_data[index]=payload[i];
		            	index++;
		            }
		            payload = itr.next();
		        }
		    }
	        // Mark as complete:
	        if (type == AEROSENSOR_SETTING_TYPE_PARAMETER)
	        {
	        	settings.getFromBurst(burst_data);
		        settings.status.settingsPull = SETTINGS_COMPLETE;
		        settings.status.count = settings.count;
		        data.pendingAction = AEROSENSOR_PENDING_NONE;
	        	logging.log("Settings received.");
				resetFrequency();
		    }
	        else if (type == AEROSENSOR_SETTING_TYPE_TRACK)
	        {
	        	track.getFromBurst(burst_data);
		        track.status.settingsPull = SETTINGS_COMPLETE;
		        track.status.count = track.count; 
		        data.pendingAction = AEROSENSOR_PENDING_NONE;
	        	logging.log("Track layout received.");
				resetFrequency();
		    }
	        else if (type == AEROSENSOR_SETTING_TYPE_LAP_TRIGGER)
	        {
	        	// Make copy of lap data:
	        	// Only do something if lap is new:
	        	if (lap_data.isLapNew(burst_data))
	        	{
					Sys.println("New lap - prev valid = " + lap_data.valid);
					// Only copy to previous if valid:
					if (lap_data.valid == 1)
					{
	        			prev_lap_data = lap_data.copy();
						Sys.println("Copying to prev, lapNo = " + lap_data.lapNumber + " cda = " + lap_data.vals[1]);
					}
	        		lap_data.getFromBurst(burst_data);
					var new_lap = lap_data.copy();
					as_session_data.addLap(lap_data.getLapDataArray());
					// Calculate session average:
					as_session_data.calculateSessionData();
	        	}
		    }
	        Ui.requestUpdate();
		}
		
		function sendSettings()
		{
			if (searching)
			{
				settings.status.settingsPush = SETTINGS_DISCONNECTED;
			}
			else
			{
				var settings_raw = new[settings.count];
				var buffer_size = 8 + settings.count*3;
				if (buffer_size % 8)
				{
					buffer_size+= 8 - (buffer_size % 8);
				}
				var buffer = new[buffer_size];
				buffer[0] = 0x1F;
				buffer[1] = 0x01;
				buffer[2] = settings.count;
				for (var i=0;i<5;i++)
				{
					buffer[i+3] = 0xFF;
				}
				for (var i=0;i<settings.count;i++)
				{
					settings_raw[i] = (settings.vals[i] - settings.offset[i]) / settings.scaling[i];
					var integer_val = settings_raw[i].toNumber();
					buffer[8 + 3 * i] = settings.ids[i];
					buffer[8 + 3 * i + 1] = integer_val & 0xFF;
					buffer[8 + 3 * i + 2] = (integer_val>>8) & 0xFF;	
				}
				for (var i=8+3*settings.count;i<buffer_size;i++)
				{
					buffer[i]=0x00;
				}
				// Send buffer:
				var payload = new Ant.BurstPayload();
				var msg = new[8];
				for (var i=0;i<buffer_size/8;i++)
				{
					for (var j=0;j<8;j++)
					{
						msg[j]=buffer[i*8+j];
					}
					payload.add(msg);
				}
				settings.status.settingsPush = SETTINGS_SENDING;
				data.pendingAction = AEROSENSOR_PENDING_SETTINGS;
				GenericChannel.sendBurst(payload);
			}
		}
		
		function sendTrack()
		{
			if (searching)
			{
				track.status.settingsPush = SETTINGS_DISCONNECTED;
			}
			else
			{
				var track_raw = new[track.count];
				var buffer_size = 8 + track.count*2;
				if (buffer_size % 8)
				{
					buffer_size+= 8 - (buffer_size % 8);
				}
				var buffer = new[buffer_size];
				buffer[0] = 0xEB;
				buffer[1] = 0x01;
				buffer[2] = track.count;
				for (var i=0;i<5;i++)
				{
					buffer[i+3] = 0xFF;
				}
				for (var i=0;i<track.count;i++)
				{
					track_raw[i] = track.vals[i] / track.scaling[i];
					var integer_val = track_raw[i].toNumber();
					buffer[8 + 2 * i] = integer_val & 0xFF;
					buffer[8 + 2 * i + 1] = (integer_val>>8) & 0xFF;	
				}
				for (var i = 8 + 2 * track.count;i<buffer_size;i++)
				{
					buffer[i]=0x00;
				}
				// Send buffer:
				var payload = new Ant.BurstPayload();
				var msg = new[8];
				for (var i=0;i<buffer_size/8;i++)
				{
					for (var j=0;j<8;j++)
					{
						msg[j]=buffer[i*8+j];
					}
					payload.add(msg);
				}
				track.status.settingsPush = SETTINGS_SENDING;
				data.pendingAction = AEROSENSOR_PENDING_TRACK;
				GenericChannel.sendBurst(payload);
			}
		}
		
		function getPullStatusText(status)
		{
			var ret = "";
			switch (status.settingsPull)
			{
				case SETTINGS_NO_ACTION:
					ret = "Get parameters from Aerosensor.";
					break;
				case SETTINGS_SENDING:
					ret = "Requsting parameters.";
					break;
				case SETTINGS_FAILURE:
					ret = "Request parameters failed.";
					break;
				case SETTINGS_SUCCESS:
					ret = "Paremeters requested..";
					break;
				case SETTINGS_COMPLETE:
					ret = status.count + " parameters received.";
					break;
				case SETTINGS_DISCONNECTED:
					ret = "Aerosensor disconnected.";
					break;
			}
			return ret;
		}
		
		function getPushStatusText(status)
		{
			var ret = "";
			switch (status.settingsPush)
			{
				case SETTINGS_NO_ACTION:
					if (status.isDirty)
					{
						ret = "Push to Aerosensor.";
					}
					else
					{
						ret = "No changes.";
					}
					break;
				case SETTINGS_SENDING:
					ret = "Sending...";
					break;
				case SETTINGS_FAILURE:
					ret = "Sending failed.";
					break;
				case SETTINGS_SUCCESS:
					ret = "Sent, waiting response.";
					break;
				case SETTINGS_COMPLETE:
					ret = "Sent successfully.";
					break;
				case SETTINGS_DISCONNECTED:
					ret = "Aerosensor disconnected.";
					break;
			}
			return ret;
		}
	}
	
	//! An extension of BurstListener that handles burst related events
	class TestBurstListener extends Ant.BurstListener {
	    var burstStatistics;
		var recCb;
		var m_sensor;
	    //! Constructor.
	    function initialize(sensor, receiveCallback) {
	        burstStatistics = new BurstStatistics();
	        BurstListener.initialize();
	        recCb = receiveCallback;
	        m_sensor = sensor;
	    }
	
	    //! Callback when a burst transmission completes successfully
	    function onTransmitComplete() {
	        burstStatistics.txSuccessCount++;
	        System.println("onTransmitComplete");
	    }
	
	    //! Callback when a burst transmission fails over the air
	    //! @param [Number] errorCode The type of burst failure that occurred, see Ant.BURST_ERROR_XXX
	    function onTransmitFail(errorCode) {
	        burstStatistics.txFailCount++;
	        System.println("onTransmitFail-" + errorCode);
	    }
	
	    //! Callback when a burst reception fails over the air
	    //! @param [Number] errorCode The type of burst failure that occurred, see Ant.BURST_ERROR_XXX
	    function onReceiveFail(errorCode) {
	        burstStatistics.rxFailCount++;
	        System.println("onReceiveFail-" + errorCode);
	    }
	
	    //! Callback when a burst reception completes successfully
	    //! @param [BurstPayload] burstPayload The burst data received across the channel
	    function onReceiveComplete(burstPayload) {
	        System.println("onReceiveComplete start");
	        burstStatistics.rxSuccessCount++;
	        recCb.invoke(burstPayload);
	       	Ui.requestUpdate();
	    }
	
	    //! Iterates over a burst paylaod to print each packet
	    //! @param [BurstPayload] burstPayload The burst data to display
	    hidden function printPayload(burstPayload)
	    {
	        var itr = new Ant.BurstPayloadIterator(burstPayload);
	        var payload = itr.next();
	        while( null != payload )
	        {
	            System.println("payload " + payload);
	            payload = itr.next();
	        }
	    }
	}
	
	class BurstStatistics {
	    var rxFailCount;
	    var rxSuccessCount;
	    var txFailCount;
	    var txSuccessCount;
	
	    //! Constructor.
	    function initialize() {
	        rxFailCount = 0;
	        rxSuccessCount = 0;
	        txFailCount = 0;
	        txSuccessCount = 0;
	    }
	}
}
	