using Toybox.AntPlus;
using Toybox.System as Sys;

class antplus_speed
{
	function getSpeed(bsc, bspd)
	{
		var data = bspd.getSpeed();
		if (data!= null)
		{
			return data;
		}
		else
		{
			data = bsc.getSpeed();
			if (data != null)
			{
				return data;
			}
			else
			{
				return 0;
			}
		}
		return  0;
	}
}