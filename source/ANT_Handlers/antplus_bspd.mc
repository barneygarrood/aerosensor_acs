using Toybox.AntPlus;
using Toybox.System as Sys;


class aero_bspd_listener extends AntPlus.BikeSpeedListener
{
	hidden var m_spd;

	function initialize()
	{
		BikeSpeedListener.initialize();
		m_spd = null;
	}

	function onBikeSpeedUpdate(info)
	{
		if (info != null)
		{
			if (info.speed > 0)
			{
    			m_spd = info.speed*3.6;
				Sys.println("BSPD speed = " + m_spd);
				if (velo_view_fit_file.isLogging())
				{
					velo_view_fit_file.fields[ROAD_SPEED_FIELD].setData(m_spd);
				}
			}
		}
	}

	function getSpeed()
	{
		return m_spd;
	}
}


class antplus_bspd
{
	hidden var m_bspd;
	hidden var m_listener;
	hidden var m_dist;
	function initialize()
	{
		// Initialise BSPD device:
		m_listener = new aero_bspd_listener();
		m_bspd = new AntPlus.BikeSpeed(m_listener);
	}

	function getSpeed()
	{
		return m_listener.getSpeed();
	}
	
	function getDeviceNumber()
	{
		return m_bspd.getDeviceState().deviceNumber;
	}
	
	// Returns device state.
	function getDeviceStateText()
	{
		var state = m_bspd.getDeviceState().state;
		var text = "";
		switch (state)
		{
			case AntPlus.DEVICE_STATE_DEAD:
				text = "Dead";
				break;
			case AntPlus.DEVICE_STATE_CLOSED:
				text = "Closed";
				break;
			case AntPlus.DEVICE_STATE_SEARCHING:
				text = "Searching";
				break;
			case AntPlus.DEVICE_STATE_TRACKING:
				text = "Tracking";
				break;
			case AntPlus.DEVICE_STATE_CNT:
				text = "Cnt (not sure..)";
				break;
		}
		return text;
	}
	function isTracking()
	{
		if (m_bspd.getDeviceState().state == AntPlus.DEVICE_STATE_TRACKING)
		{return true;} else {return false;}
	}	
}