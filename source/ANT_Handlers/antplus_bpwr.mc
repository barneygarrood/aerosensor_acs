using Toybox.AntPlus;
using Toybox.System as Sys;
using Toybox.Timer;

	
// BPWR Listener class.  
class antplus_bpwr_listener extends AntPlus.BikePowerListener
{
	
	function initialize()
	{
		BikePowerListener.initialize();
	}
	
	
	function onMessage(msg)
	{
		// Only act on broadcast message id (0x4E):
		// if (msg.messageId == 0x4E)
		// {
		// 	antplus_bpwr.updateAverage();
		// }
	}
}

class antplus_bpwr
{
	hidden var m_listener;	
	hidden var m_bpwr;
	hidden var _sample_count;
	hidden var _samples;
	hidden var _head;
	hidden var _max_samples_count = 20; //number of samples, at 2Hz - runs from 2hz timer in velo_view_app
	hidden var _timer;
	
	function initialize()
	{
		_sample_count = 0;
		_head = 0;
		_samples = new [_max_samples_count];
		m_listener = new antplus_bpwr_listener();
		m_bpwr = new AntPlus.BikePower(m_listener);
		// Set reference to thiss object in the listener, which allows it to call functions in here.
    	// Set timer to add power to average once per second.
        //_timer= new Timer.Timer();
        //_timer.start(method(:updateAverage),250,false);
	}
	
	// Class to store data.  Currently just power but could be other stuff too.  
	// Keep like this for consistency.
	class bpwr_data
	{
		var power;
		function initialize()
		{
			power = -1;
		}
	}
	
	// Returns device number.
	function getDeviceNumber()
	{
		return m_bpwr.getDeviceState().deviceNumber;
	}
	// Returns device state.
	function getDeviceStateText()
	{
		var state = m_bpwr.getDeviceState().state;
		var text = "";
		switch (state)
		{
			case AntPlus.DEVICE_STATE_DEAD:
				text = "Dead";
				break;
			case AntPlus.DEVICE_STATE_CLOSED:
				text = "Closed";
				break;
			case AntPlus.DEVICE_STATE_SEARCHING:
				text = "Searching";
				break;
			case AntPlus.DEVICE_STATE_TRACKING:
				text = "Tracking";
				break;
			case AntPlus.DEVICE_STATE_CNT:
				text = "Cnt (not sure..)";
				break;
		}
		return text;
	}
	
	function isTracking()
	{
		if (m_bpwr.getDeviceState().state == AntPlus.DEVICE_STATE_TRACKING)
		{	
			return true;
		} 
		else 
		{
			return false;
		}
	}
	
	// Function to return bike power data object.
	function getData()
	{
		var ret = new bpwr_data();
    	var info = m_bpwr.getCalculatedPower();
    	if (info!=null)
    	{
    		if (info.power != null)
    		{
	    		if (info.power > 0)
	    		{
	    			ret.power = info.power;
	    		}
	    	}
    	}
    	return ret;
	}
	
	// Callback on power update.
	function updateAverage()
	{
		var info = m_bpwr.getCalculatedPower();
    	if (info!=null)
    	{
    		if (info.power != null)
    		{
	    		if (info.power >= 0)
	    		{
					_samples[_head] = info.power;
					if (_sample_count < _max_samples_count)
					{
						_sample_count++;
					}
					_head++;
					if (_head == _max_samples_count)
					{
						_head = 0;
					}
				}
			}
		}
	}
	
	// get average power.
	function getAvgPower()
	{
		var ret = 0.0f;
		if (_sample_count == 0) {return ret;}
		for (var i=0;i<_sample_count;i++)
		{
			ret+=_samples[i];
		}
		ret/=_sample_count;
		return ret;
	}
	
	
}