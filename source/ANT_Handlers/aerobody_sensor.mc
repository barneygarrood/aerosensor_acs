using Toybox.Ant as Ant;
using Toybox.System as Sys;
using Toybox.Time as Time;
using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Math;
using Toybox.Application as App;
using Toybox.Timer;
using Toybox.Lang;
using aerosensor_module as aerosensor;
		
enum {
	REF_STATUS_NONE,
	REF_STATUS_SENDING,
	REF_STATUS_SENT,
	REF_STATUS_SYNCED,
	REF_STATUS_FAILED
}

module aerobody_module
{
	class aerobody_sensor extends Ant.GenericChannel
	{
		const DEVICE_TYPE = 112;
		const PERIOD = 3895;
		const MIN_RANGE = 15;
		const MAX_RANGE = 1000;
		const MAX_AVG_SAMPLES = 16; // Ordinarily recieve data at 8Hz, so 16 samples = 2 second average.
		const DATUM_SET_TIME = 5000;	// Milliseconds between asking to set datum, and actually setting datum.
		const MAX_HEAD_OFFSET = 500;
		const HEAD_SENSOR = 2;
		const CHEST_SENSOR = 0;
		const BUFFER_SIZE = 32;
		const SENSOR_COUNT = 3;
		const AVG_TIME = 2000;	// Rolling average milliseconds.
		const SW_VER_REQUEST_MAX = 16; // i.e. request page 81 a maximum of 16 times.
		const SW_VER_REQUEST_PERIOD = 16; // i.e. request every 16 packets.
		
		hidden var m_channel;	
		
		hidden var m_start_time;
		
		var data;
		var searching;
		var deviceCfg;
		var setting_datum;
				
		hidden var defaultDeviceNumber;
		
		hidden var sendQueueIndex = 0;
		hidden var sendQueueSize = 0;
		hidden var sendQueueMaxSize = 8;
		hidden var sendQueue = new[sendQueueMaxSize];
		
		hidden var buffer = new[(SENSOR_COUNT+1)*32];	// Max 8Hz x 4 seconds, time + 3 sensors.
		hidden var range = new[SENSOR_COUNT];
		hidden var status = new[SENSOR_COUNT]; // 1 if valid, 0 if invalid.		
		hidden var buffer_head;
		
		var ref_status;
		
		hidden var swVersionTryCount = 0;
		hidden var swVersionCounter = 0;
		
		
		class aerobody_data
		{
			
			// page 224 stuff:
			var batVoltage;
			var batLevel;
			
			// page 81 stuff
			var majSwVersion;
			var minSwVersion;
			var serialNumber;
			var swVersionSet;
			
			// Initailizer:
			function initialize()
			{				
				batVoltage = 0;
				batLevel = 0;
				majSwVersion = 0;
				minSwVersion = 0;
				serialNumber = 0l;
				swVersionSet = false;
			}
		}
		
		class aerobody_sensor_page81
		{
			static const PAGE_NUMBER = 81;
			
			function parse(payload, _data)
			{	
				_data.swVersionSet = true;
				_data.majSwVersion = payload[3];
				_data.minSwVersion = payload[2];
				// Convoluted way of getting unisnged 32 bit integer from byte array.
				// Every other way I try returns a signed integer.
				var ba = new [4]b;
				ba[0]=payload[4];
				ba[1]=payload[5];
				ba[2]=payload[6];
				ba[3]=payload[7];
				_data.serialNumber = ba.decodeNumber(Lang.NUMBER_FORMAT_UINT32, null);
			}
		}
		
		class aerobody_sensor_page224
		{
			static const PAGE_NUMBER = 224;
			
			function parse(payload, _data)
			{	
				_data.batVoltage = payload[6]/256.0f + (payload[7] & 0x0F);
				_data.batLevel = (payload[7]>>4) & 0x07;
			}
		}
				
		class aerobody_sensor_page228
		{
			static const PAGE_NUMBER = 228;
			
			function parse(payload, _data)
			{	
				var now = Sys.getTimer();
				buffer[4*buffer_head] = now;
				for (var i=0;i<SENSOR_COUNT;i++)
				{
					buffer[4*buffer_head + 1 + i] = (payload[2+2*i] | (payload[3+2*i]<<8));
				}
				// Calculate rolling average:
				//var avg = new [SENSOR_COUNT];
				var count = new [SENSOR_COUNT];
				
				// initailise values:
				for (var i=0;i<SENSOR_COUNT;i++)
				{
					//avg[i]=0;
					count[i]=0;
				}
				//Loop through buffer averaging over a max of 2 seconds:
				//Changed  8/6/21 to just send most recent valid range value, up to a max of 2s ago.
				var index = buffer_head;
				for (var i=0;i<BUFFER_SIZE;i++)
				{
					if (buffer[index*4] == null){break;}
					if ((now - buffer[index*4])>AVG_TIME){break;}
					var check = true;
					
					for (var j=0;j<SENSOR_COUNT;j++)
					{
						if (count[j] == 0)
						{
							if ((buffer[index*4+1+j] >= MIN_RANGE) && (buffer[index*4+1+j]<=MAX_RANGE))
							{
								count[j]++;
								//avg[j] += buffer[index*4+1+j];
								range[j] = buffer[index*4+1+j];
							}
							else
							{
								check = false;
							}
						}
					}
					if (check){break;}
					index--;
					if (index<0){index+=BUFFER_SIZE;}
				}
				// Calculate averages:
				for (var i=0;i<SENSOR_COUNT;i++)
				{
					if (count[i]>0)
					{
						//range[i]=avg[i];///count[i];
						//status[i]=1;
					}
					else
					{
						range[i]=null;
						status[i]=0;
					}
				}
				// Increment head:
				buffer_head++;
				if (buffer_head >= BUFFER_SIZE){buffer_head = 0;}
				// Update fields in FIT file:
				var head = 0;
				var chest = 0;
				
				head = range[HEAD_SENSOR];
				if (head != null)
				{
					head/=10.0f;
				}
				chest = range[CHEST_SENSOR];
				if (chest != null)
				{
					chest/=10.0f;
				}		
				vv_sensors.aerosensor.lap_data.add_body_pos(head,chest);		
					
				if (velo_view_fit_file.isLogging())
				{
					if (head != null)
					{
						velo_view_fit_file.fields[AERO_POS_HEAD_FIELD].setData(head);
					} 
					else
					{
						velo_view_fit_file.fields[AERO_POS_HEAD_FIELD].setData(0);
					}
					
					if (chest != null)
					{
						velo_view_fit_file.fields[AERO_POS_CHEST_FIELD].setData(chest);
					}
					else
					{
						velo_view_fit_file.fields[AERO_POS_CHEST_FIELD].setData(0);
					}	
				}
			}
		}	
		
		class aerobody_sensor_page232
		{
			static const PAGE_NUMBER = 232;
			
			function parse(payload, _data)
			{	
				var value;
				var save = false;
				for (var i=0;i<SENSOR_COUNT;i++)
				{
					value = (payload[2+2*i] | (payload[3+2*i]<<8));
					if (value!=null)
					{
						if ((value <= MAX_RANGE) & (value>=MIN_RANGE))
						{
							if (i == HEAD_SENSOR)
							{
								if (vv_settings.values[vv_settings.HEADREF] != value)
								{
									vv_settings.values[vv_settings.HEADREF] = value;
									save = true;
								}
							}
							else if (i == CHEST_SENSOR)
							{
								if (vv_settings.values[vv_settings.CHESTREF] != value)
								{
									vv_settings.values[vv_settings.CHESTREF] = value;
									save = true;
								}
							}
						}
					}
					if (save)
					{
						vv_settings.save();
					}
					ref_status = REF_STATUS_SYNCED;
				}
			}
		}
	
		
		function initialize()
		{
			m_channel = new Ant.ChannelAssignment
			(
				Ant.CHANNEL_TYPE_RX_NOT_TX,
				Ant.NETWORK_PUBLIC
			);
			GenericChannel.initialize(method(:onMessage), m_channel);
			
			// Get device number:
			defaultDeviceNumber = vv_settings.values[vv_settings.AEROBODY_DEVICE_NUMBER];
			if (defaultDeviceNumber == null){defaultDeviceNumber = 0;}
			
			deviceCfg = new Ant.DeviceConfig
			({
				:deviceNumber => defaultDeviceNumber,
				:deviceType => DEVICE_TYPE,
	            :messagePeriod => PERIOD,
				:transmissionType =>5,
				:radioFrequency => 67,
				:searchTimeoutLowPriority => 10,
				:searchThreshold => 0
			});
			GenericChannel.setDeviceConfig(deviceCfg);			
			
			// Initialise data points:
			data = new aerobody_data();
			searching = true;
			
			for (var i=0;i<SENSOR_COUNT;i++)
			{
				range[i] = null;
			}
			for (var i=0;i<(1+SENSOR_COUNT)*BUFFER_SIZE;i++)
			{
				buffer[i] = null;
			}
			buffer_head = 0;
			
			// Initialise send queue.  Works as a circular buffer.
			for (var i=0;i<sendQueueSize;i++)
			{
				sendQueue[i] = new[8];
			}
			sendQueueIndex = 0;
			sendQueueSize = 0;
			ref_status = REF_STATUS_NONE;
		}
				
		function putPageInSendQueue(page)
		{
			// check queue isn't overgrown:
			if (sendQueueSize == sendQueueMaxSize) {return;}
			var index = (sendQueueIndex + sendQueueSize) & 0x07;
			sendQueue[index] = page;
			sendQueueSize = sendQueueSize + 1;
		}
		
		function getPageFromSendQueue()
		{
			// Check there are pages to send:
			if (sendQueueSize == 0) {return 0;}
			var ret = sendQueue[sendQueueIndex];
			sendQueueSize--;
			sendQueueIndex = (sendQueueIndex + 1) & 0x07;
			return ret;
		}
		
		function changeDeviceID(newID)
		{
			closeSensor();
			deviceCfg.deviceNumber = newID;
			GenericChannel.setDeviceConfig(deviceCfg);
			open();
		}
		
		function getHeadRef()
		{
			return vv_settings.values[vv_settings.HEADREF];
		}
		
		function getChestRef()
		{
			return vv_settings.values[vv_settings.CHESTREF];
		}
		
		function setHeadRef(headRef)
		{
			vv_settings.values[vv_settings.HEADREF] = headRef;
			//vv_settings.save();
		}
		
		function setChestRef(chestRef)
		{
			vv_settings.values[vv_settings.CHESTREF] = chestRef;
			//vv_settings.save();
		}
		
		function getHeadPos()
		{
			return range[HEAD_SENSOR];
		}
		
		function getChestPos()
		{
			return range[CHEST_SENSOR];
		}
		
		function getDeltaPos(index)
		{
			if (range[index] == null)
			{
				return null;
			}
			else
			{
				if (index == HEAD_SENSOR)
				{
					if (vv_settings.values[vv_settings.HEADREF] == null){return null;}
					return range[index] - vv_settings.values[vv_settings.HEADREF];
				}
				else if (index == CHEST_SENSOR)
				{	
					if (vv_settings.values[vv_settings.CHESTREF] == null){return null;}
					return range[index] - vv_settings.values[vv_settings.CHESTREF];
				}
			}
			return null;
		}
		
		function getHeadDeltaPos()
		{
			return getDeltaPos(HEAD_SENSOR);
		}
		
		function getChestDeltaPos()
		{
			return getDeltaPos(CHEST_SENSOR);
		}
		
		function open()
		{
			searching = true;
			data = new aerobody_data();
			GenericChannel.open();
		}	
		
		function closeSensor()
		{
			GenericChannel.close();
		}
		
		function checkRequestSwVersion()
		{
			if (data.swVersionSet){return;}
			if (swVersionTryCount > SW_VER_REQUEST_MAX){return;}
			swVersionCounter++;
			if (swVersionCounter == SW_VER_REQUEST_PERIOD)
			{
				swVersionCounter = 0;
				requestSoftwareVersion();
				swVersionTryCount++;
			}
		}
		
		function onMessage(msg)
		{
			var payload = msg.getPayload();
			if (Ant.MSG_ID_BROADCAST_DATA == msg.messageId)
			{
				// Deal with software version page:
				checkRequestSwVersion();
				if (searching)
				{
					searching = false;
					deviceCfg = GenericChannel.getDeviceConfig();
					// Store device number, if necessary:
					if (deviceCfg.deviceNumber != defaultDeviceNumber)
					{
						defaultDeviceNumber = deviceCfg.deviceNumber;					
						vv_settings.values[vv_settings.AEROBODY_DEVICE_NUMBER] = defaultDeviceNumber;
						vv_settings.save();
					}
				}
				var dp;
				// Decode depending on which page was received:
				switch (payload[0])
				{
					case aerobody_sensor_page81.PAGE_NUMBER:
						aerobody_sensor_page81.parse(payload, data);
						break;
					case aerobody_sensor_page224.PAGE_NUMBER:
						aerobody_sensor_page224.parse(payload, data);
						break;
					case aerobody_sensor_page228.PAGE_NUMBER:
						aerobody_sensor_page228.parse(payload, data);
						break;
					case aerobody_sensor_page232.PAGE_NUMBER:
						aerobody_sensor_page232.parse(payload, data);
						break;
				}
				// Send page from queue if one exists:
				if (sendQueueSize>0)
				{
					var page = getPageFromSendQueue();
					msg = new Ant.Message();
					msg.setPayload(page);
					if (msg != false)
					{
						GenericChannel.sendAcknowledge(msg);
					}
				}
			} //  end broadcast data
			else if (Ant.MSG_ID_CHANNEL_RESPONSE_EVENT == msg.messageId)
			{
				if (Ant.MSG_ID_RF_EVENT == (payload[0] & 0xFF))
				{
					if (Ant.MSG_CODE_EVENT_CHANNEL_CLOSED == (payload[1] & 0xFF))
					{
	                    // Channel closed, re-open
						open();
					}
					else if (Ant.MSG_CODE_EVENT_RX_FAIL_GO_TO_SEARCH == (payload[1] & 0xFF))
					{
						if (!searching)
						{
							searching = true;
						}
					}
					else if (Ant.MSG_CODE_EVENT_TRANSFER_TX_COMPLETED == (payload[1] & 0xFF))
					{
						if (ref_status == REF_STATUS_SENDING){ref_status = REF_STATUS_SENT;}
					}
					else if (Ant.MSG_CODE_EVENT_TRANSFER_TX_FAILED== (payload[1] & 0xFF))
					{
						if (ref_status == REF_STATUS_SENDING){ref_status = REF_STATUS_FAILED;}
					}
				}
			}	// end channel response
		} // end on message
		
		
		function store_datums()
		{
			vv_settings.save();
		}
		
		function requestSoftwareVersion()
		{
			var payload = [0x46, 0x00, 0x00, 0x00, 0x00, 0x00, 0x51, 0x01];		// Send paired devices page for deviceIndex.  Send page only once.
			
			putPageInSendQueue(payload);
		}
				
		function getColour(index)
		{
			var value = getDeltaPos(index);
			if (value == null)
			{
				return as_colors.background;
			}
			else if (value < - vv_settings.values[vv_settings.NULLZONE])
			{
				return Gfx.COLOR_BLUE;
			}
			else if (value > vv_settings.values[vv_settings.NULLZONE])
			{
				return Gfx.COLOR_RED;
			}
			else
			{
				return as_colors.background;
			}
		}
		
	}
}