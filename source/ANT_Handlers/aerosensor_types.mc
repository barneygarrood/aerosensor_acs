	
module aerosensor_module
{
	// ################################################################################
	// # Class types.  We could put these somewhere else.
	// ################################################################################
	
	// Class to hold settings get/set status, e.g. to show a sending or receiving failed.
	enum 
	{
		SETTINGS_NO_ACTION	= 0,
		SETTINGS_SENDING = 1,
		SETTINGS_FAILURE = 2,
		SETTINGS_SUCCESS = 4,	// For case where request message has been received, but not yet returned settings.
		SETTINGS_COMPLETE = 8, 
		SETTINGS_DISCONNECTED = 16
	}
		
	enum
	{
		AEROSENSOR_SETTING_TYPE_PARAMETER = 0,
		AEROSENSOR_SETTING_TYPE_TRACK = 1,
		AEROSENSOR_SETTING_TYPE_LAP_TRIGGER = 2
	}
	
	class aerosensor_status
	{
		var settingsPull;
		var settingsPush;
		var count;
		var isDirty;
		function initialize()
		{
			settingsPull = SETTINGS_NO_ACTION;
			settingsPush = SETTINGS_NO_ACTION;
			count = 0;
			isDirty = false;
		}
	}
	
	enum
	{
		AEROSENSOR_PENDING_NONE = 0,
		AEROSENSOR_PENDING_SETTINGS = 1,
		AEROSENSOR_PENDING_TRACK = 2
	}
		
	class aerosensor_p01_data
	{
		var prevEventCount;
		var prevAccCda;
		var prevAccCdaYaw;
		var isInit;
		function initialize()
		{
			prevEventCount = 0;
			prevAccCda = 0;
			prevAccCdaYaw = 0;
			isInit = false;
		}
	}
	
	class aerosensor_p18_data
	{
		var prevEventCount;
		var prevAccPdyn;
		var prevAccYaw;
		var isInit;
		function initialize()
		{
			prevEventCount = 0;
			prevAccPdyn = 0;
			prevAccYaw = 0;
			isInit = false;
		}
	}
	
	class aerosensor_p19_data
	{
		var prevEventCount;
		var isInit;
		function initialize()
		{
			prevEventCount = 0;
			isInit = false;
		}
	}
	
	class aerosensor_p20_data
	{
		var prevEventCount;
		var prevAccBaro;
		var prevAccTemp;
		var isInit;
		function initialize()
		{
			prevEventCount = 0;
			prevAccBaro = 0;
			prevAccTemp = 0;
			isInit = false;
		}
	}
	
	class aerosensor_p230_data
	{
		var prevEventCount;
		var prevAccBaro;
		var prevAccTemp;
		var isInit;
		function initialize()
		{
			prevEventCount = 0;
			prevAccBaro = 0;
			prevAccTemp = 0;
			isInit = false;
		}
	}
	
	class aerosensor_p233_data
	{
		var timeOffset;
		var rssi;
		var lapNumber;
		var commandType;
		function initialize()
		{
			timeOffset = 0;
			rssi = 0;
			lapNumber = 0;
			commandType = 0;
		}
	}
	
	class aerosensor_paired_device
	{
		var channelState;
		var deviceNumber;
		var transmissionType;
		var deviceType;
		var lastUpdateTime;
		var connected;
		function initialize()
		{
			channelState = 0;
			deviceNumber = 0;
			transmissionType = 0;
			deviceType = 0;
			lastUpdateTime = 0;
		}
	}
	
	class aerosensor_data
	{
		// Page 1 stuff:
		var cda;
		var cdaYaw;
		var cdaError;
		var validCda;
		var validCdaYaw;
		var powerAdvantage;
		
		// Page 18 stuff:
		var validPdyn;
		var validYaw;
		var density;
		var pdyn;
		var yaw;
		var pdyn_av;
		var yaw_av;
		var density_av;
		
		// Page 19 stuff:
		var userPwrPct;
		var gradePwrPct;
		var fricPwrPct;
		var windPwrPct;
		var kineticPwrPct;
		var pwrDenom;
		var validPwr;
		
		// Page 20 stuff:
		var temperature;
		var baro;
		var validTemp;
		var validBaro;
		var prev_baro;
		var elevation;
		var windSpeed;
		var baro_av;
		var prev_baro_av;
		var windSpeed_av;
			
		// page 81 stuff
		var majSwVersion;
		var minSwVersion;
		var serialNumber;
		var swVersionSet;
		
		// Page 224 stuff
		var calcMode;
		var demoMode;
		var batVoltage;
		var batLevel;
				
		// Page 229 stuff
		var lapNumber;
		var deviceLapNumber;
		var offsetLapNumber;
		// Share cdaError, cda and validCda with page 1..
		var validShort;
		var validLap;
		var activeLap;
		var cdaLong;
		var cdaShort;
		var cdaLap;		
		var p229Init;
		
		// Variables to store previous :
		var prevLapCda;
		var prevLapNumber;
		
		// Paired devices:
		var pairedDevices;
		
		// status:
		//var settingsStatus;
		//var trackStatus;
		var pendingAction;
		
		// Lap start time;
		var lapStartTime;
		// Last message time:
		var lastMsgTime;
		
		// Initailizer:
		function initialize()
		{	
			cda = 0;
			cdaYaw = 0;
			cdaError = 0;
			validCda = false;
			validCdaYaw = false;
			powerAdvantage = 0;
			
			validPdyn = false;
			validYaw = false;
			density = 0;
			pdyn = 0;
			yaw = 0;
			pdyn_av = null;
			yaw_av = null;
			density_av = null;
			
			userPwrPct = 0;
			gradePwrPct = 0;
			fricPwrPct = 0;
			windPwrPct = 0;
			kineticPwrPct = 0;
			pwrDenom = 0;
			validPwr = false;
			
			temperature = 0;
			baro = null;
			baro_av = null;
			validTemp = false;
			validBaro = false;
			
			prev_baro = null;
			prev_baro_av = null;
			elevation = null;
			windSpeed = null;
			
			calcMode = -1;
			demoMode = -1;
			batVoltage = 0;
			batLevel = 0;
			
			lapNumber = 0;
			deviceLapNumber=-1;
			offsetLapNumber = 0;
			validShort = 0;
			validLap = 0;
			activeLap = 0;
			cdaLong = 0;
			cdaShort = 0;
			cdaLap = 0;
			prevLapCda = 0;
			prevLapNumber = 0;
			p229Init = false;
			
			majSwVersion = 0;
			minSwVersion = 0;
			serialNumber = 0l;
			swVersionSet = false;
			
			var dev0 = new aerosensor_paired_device();
			var dev1 = new aerosensor_paired_device();
			var dev2 = new aerosensor_paired_device();
			pairedDevices = [dev0,dev1,dev2];
			
			pendingAction = AEROSENSOR_PENDING_NONE;
		}
	}
}