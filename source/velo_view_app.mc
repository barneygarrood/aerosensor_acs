using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Position as GPS;
using Toybox.Timer;
using Toybox.Lang;
//using Toybox.Sensor;
using aerosensor_module as aerosensor;
using aerobody_module as aerobody;

// Global variables
var myGlobalVar = 0;

class velo_view_app extends App.AppBase 
{
	function onPosition( info ) {
		
	}
	
    function initialize() 
    {
        AppBase.initialize();
    	vv_settings.load();
    	as_colors.init();
        var myTimer = new Timer.Timer();
        myTimer.start(method(:timerCallback),500,true);
        as_session_data.reset();
    }

	function initSensors()
	{
		try
    	{
			vv_sensors.as_lap_transmitter = new as_lap_transmitter();
			vv_sensors.as_lap_transmitter.open();
		}
    	catch (e instanceof Ant.UnableToAcquireChannelException)
    	{
    		Sys.println(e.getErrorMessage());
    		vv_sensors.as_lap_transmitter = null;
    	}

		try
    	{
		    vv_sensors.aerosensor = new aerosensor.aerosensor_sensor();
    		// Create sensor object and open it:
    		if (vv_settings.values[vv_settings.AEROSENSOR_ACTIVE])
    		{
		    	vv_sensors.aerosensor.open();
		    }
    	}
    	catch (e instanceof Ant.UnableToAcquireChannelException)
    	{
    		Sys.println(e.getErrorMessage());
    		vv_sensors.aerosensor = null;
    	}
    	// Open body position sensor channel:
    	try
    	{
    		// Create sensor object and open it:
	    	vv_sensors.aerobody = new aerobody.aerobody_sensor();
	    	vv_sensors.aerobody.open();
    	}
    	catch (e instanceof Ant.UnableToAcquireChannelException)
    	{
    		Sys.println(e.getErrorMessage());
    		vv_sensors.aerobody = null;
    	}
    	
    	// Enable sensors
		Sensor.setEnabledSensors([Sensor.SENSOR_BIKESPEED, Sensor.SENSOR_BIKEPOWER, Sensor.SENSOR_BIKECADENCE]);
		// Enable GPS:
		Position.enableLocationEvents(Position.LOCATION_CONTINUOUS, null);
    	
    	vv_sensors.bsc = new antplus_bsc();
    	vv_sensors.bspd = new antplus_bspd();
    	vv_sensors.bpwr = new antplus_bpwr();
    	vv_sensors.lapTime = 0;
	}

    // onStart() is called on application start up
    function onStart(state) 
    {
    	
    }

    // onStop() is called when your application is exiting
    function onStop(state) 
    {
    	vv_sensors.aerosensor.closeSensor();
    	vv_sensors.aerosensor.release();
    	
    	vv_sensors.aerobody.closeSensor();
    	vv_sensors.aerobody.release();
        //Position.enableLocationEvents(Position.LOCATION_DISABLE, method(:onPosition));
        velo_view_fit_file.stopLog();
    }

    // Return the initial view of your application here
    function getInitialView() 
    {
    	var del = new velo_view_delegate(0);
    	return del.showSplash();
    }

	function timerCallback()
	{
		Ui.requestUpdate();
		vv_sensors.bpwr.updateAverage();
	}
    
    function onSettingsChanged() {
    	vv_settings.load();
    }    
    
}
