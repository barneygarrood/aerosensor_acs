using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Timer;

class velo_view_delegate extends Ui.InputDelegate 
{
	hidden var m_aerosensor_and_aerobody_page_count = 10;
	hidden var m_aerobody_only_page_count = 1;
	hidden var m_page_count;
	static hidden var m_index = 0;
	hidden var m_Timer;
	
	static function getPageNumber()
	{
		return m_index;
	}
	
    function initialize(index) 
    {
        Ui.InputDelegate.initialize();
        m_index = index;
    }
    
    // Trying to write so that we show splash screen then start radios etc whilst that is showing.
    function showSplash()
    {
    	// Set timer 
        m_Timer= new Timer.Timer();
        // Set timer to start initialisation process after showing screen:
        m_Timer.start(method(:init),50,false);
        // Return initial view
        return [ new acs_splash(), self ];
    }
    
    function init()
    {
    	// Set timer to end spalsh screen.
        m_Timer.start(method(:endSplash),500,false);
        velo_view_app.initSensors();
    }
    
	function endSplash()
	{
		Ui.switchToView(getView(0), self, Ui.SLIDE_BLINK);
	}
	
	function updatePageCount()
	{
		if (vv_settings.values[vv_settings.AEROSENSOR_ACTIVE])
		{
			m_page_count = m_aerosensor_and_aerobody_page_count;
		}
		else
		{
			m_page_count = m_aerobody_only_page_count;
		}
	}

    function onNextPage() {
    	updatePageCount();
        m_index = (m_index + 1) % m_page_count;
        //Ui.switchToView(getView(m_index), getDelegate(m_index), Ui.SLIDE_LEFT);
        Ui.switchToView(getView(m_index), self, Ui.SLIDE_LEFT);
    }

    function onPreviousPage() {
    	updatePageCount();
        m_index = m_index - 1;
        if (m_index < 0) {
            m_index = m_page_count-1;
        }
        m_index = m_index % m_page_count;
        Ui.switchToView(getView(m_index), getDelegate(m_index), Ui.SLIDE_RIGHT);
    }

    function onSwipe(evt) {
    	var swipeDir = evt.getDirection();
    	switch (swipeDir)
    	{	
    		case Ui.SWIPE_LEFT:
    			onNextPage();
    			break;
    		case Ui.SWIPE_RIGHT:
    			onPreviousPage();
    			break;
    		case Ui.SWIPE_UP:
    			onMenu();
    			break;
    	}
    }

	
    function onKey(evt) {
        var key = evt.getKey();
        switch (key)
        {
        	case Ui.KEY_DOWN:
            	onNextPage();
        		break;
        	case Ui.KEY_UP:
            	onPreviousPage();
        		break;
        	case Ui.KEY_START:
            	velo_view_fit_file.startStopLog();
        		break;
        	case Ui.KEY_LAP:
        		vv_sensors.aerosensor.sendNewLapCommand();
        		break;
	    	case Ui.KEY_ESC:
	    		break;
        	case Ui.KEY_ENTER:
        	case Ui.KEY_MENU:
	    		// Depends on page:
	    		onMenu();
				break;
        }
    }

    function getView(m_index) {
        var view;
		switch (m_index)
		{
			case 0:
            	view = new vv_page0();
            	//view = new as_lap_table();
            	break;
			case 1:
            	view = new vv_page1();
            	break;
			case 2:
            	view = new vv_page2();
            	break;
			case 3:
            	view = new vv_page3();
            	break;
			case 4:
            	view = new vv_page4();
            	break;
			case 5:
            	view = new vv_page5();
            	break;
			case 6:
            	view = new vv_page6();
            	break;
			case 7:
            	view = new vv_page7();
            	break;
			case 8:
            	view = new as_lap_table();
            	break;
			case 9:
            	view = new as_session_table();
            	break;
        }
        return view;
    }

    function getDelegate(m_index) {
        var delegate = new velo_view_delegate(m_index);
        return delegate;
    }    
    
    function onMenu() {
    	var view = new vv_manual_menu();
		WatchUi.pushView(view, new vv_top_menu_delegate(view), WatchUi.SLIDE_IMMEDIATE);
		return true;
    }

}